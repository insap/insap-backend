<?php

namespace Plugins\ctd\Models;

use Illuminate\Database\Eloquent\Model;

class Ctd extends Model
{
    public $timestamps = false;

    public const TABLE = 'ctds';

    protected $table = self::TABLE;

    public const RECORD_ID_FIELD = 'record_id';

    public const RECORD_IMPORT_FIELD = 'record_import_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step_id',
        'latitude',
        'longitude',
        'station',

        'temperature',
        'turbidity',
        'chlorophyll_a',
        'depth',
        'salinity',
        'speed_of_sound',

        'ntu_1',
        'ntu_2',
        'ntu_average',

        'date',
        'record_id',
        'record_import_id',
    ];
}
