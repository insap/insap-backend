<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Plugins\ctd\Models\Ctd;

class CreateCtdsTable extends Migration
{
    public function up()
    {
        Schema::create(Ctd::TABLE, static function (Blueprint $table) {
            $table->id();

            $table->bigInteger('step_id')->index()->nullable();
            $table->integer('station')->index()->nullable();

            $table->double('latitude');
            $table->double('longitude');

            $table->float('temperature')->nullable();
            $table->float('turbidity')->nullable();
            $table->float('chlorophyll_a')->nullable();
            $table->float('depth')->nullable();
            $table->float('salinity')->nullable();
            $table->float('speed_of_sound')->nullable();

            $table->float('ntu_1')->nullable();
            $table->float('ntu_2')->nullable();
            $table->float('ntu_average')->nullable();

            $table->dateTime('date')->index();
            $table->integer(Ctd::RECORD_ID_FIELD)->index()->nullable();
            $table->integer(Ctd::RECORD_IMPORT_FIELD)->index()->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Ctd::TABLE);
    }
}
