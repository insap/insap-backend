<?php

namespace Plugins\adcp\Models;

use Illuminate\Database\Eloquent\Model;

class Adcp extends Model
{
    public $timestamps = false;

    public const TABLE = 'adcps';

    protected $table = self::TABLE;

    public const RECORD_ID_FIELD = 'record_id';

    public const RECORD_IMPORT_FIELD = 'record_import_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step_id',
        'latitude',
        'longitude',
        'distance',
        'speed',
        'max_depth',
        'list_depths',
        'depths',
        'date',
        'expedition_number',
        'record_id',
        'record_import_id',
    ];

    protected $casts = [
        'list_depths' => 'array',
        'depths' => 'array',
        'date' => 'datetime',
    ];
}
