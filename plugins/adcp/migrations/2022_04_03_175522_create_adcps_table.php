<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Plugins\adcp\Models\Adcp;

class CreateAdcpsTable extends Migration
{
    public function up()
    {
        Schema::create(Adcp::TABLE, static function (Blueprint $table) {
            $table->id();

            $table->bigInteger('step_id')->index()->nullable();
            $table->integer('expedition_number')->index();

            $table->double('latitude');
            $table->double('longitude');
            $table->double('distance')->nullable();
            $table->double('speed')->nullable();
            $table->double('max_depth')->nullable();

            $table->json('depths')->nullable();
            $table->json('list_depths')->nullable();

            $table->dateTime('date')->index();
            $table->integer(Adcp::RECORD_ID_FIELD)->index()->nullable();
            $table->integer(Adcp::RECORD_IMPORT_FIELD)->index()->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Adcp::TABLE);
    }
}
