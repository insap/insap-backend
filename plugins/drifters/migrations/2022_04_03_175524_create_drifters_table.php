<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Plugins\drifters\Models\Drifter;

class CreateDriftersTable extends Migration
{
    public function up()
    {
        Schema::create(Drifter::TABLE, static function (Blueprint $table) {
            $table->id();

            $table->bigInteger('step_id')->index()->nullable();

            $table->double('latitude');
            $table->double('longitude');

            $table->double('speed')->nullable();
            $table->double('distance')->nullable();
            $table->integer('drifter_number')->index()->nullable();
            $table->integer('utc_diff')->nullable();

            $table->dateTime('date_time')->index();
            $table->integer(Drifter::RECORD_ID_FIELD)->index()->nullable();
            $table->integer(Drifter::RECORD_IMPORT_FIELD)->index()->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists(Drifter::TABLE);
    }
}
