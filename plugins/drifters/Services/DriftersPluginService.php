<?php

declare(strict_types=1);

namespace Plugins\drifters\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Plugin\Services\PluginRecordServiceInterface;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Enums\ProcessOptionEnum;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordImport;
use Plugins\drifters\Models\Drifter;
use RuntimeException;

class DriftersPluginService implements PluginRecordServiceInterface
{
    public function getDataFromRecord(Record $record): Collection
    {
        return Drifter::where(Drifter::RECORD_ID_FIELD, $record->id)->orderBy('step_id')->get();
    }

    public function addDataToDatabase(RecordImport $processImport, ProcessParamsDto $paramsDto, Process $process): void
    {
        $record = $processImport->record;

        if ($process->getOptionsByKey(ProcessOptionEnum::OverwriteDataOnMultiplyImport->value)) {
            $this->deleteDataFromRecord($record);
        }

        $data = collect($paramsDto->getData());

        $data = $data->map(function ($item) use ($processImport, $record) {
            $item[Drifter::RECORD_ID_FIELD] = $record->id;
            $item[Drifter::RECORD_IMPORT_FIELD] = $processImport->id;

            return $item;
        });

        $this->validateData($data->first());

        foreach ($data->chunk(1000) as $chunk) {

            Drifter::insert($chunk->all());
        }
    }

    public function deleteDataFromRecord(Record $record): int
    {
        return Drifter::query()->whereRecordId($record->id)->delete();
    }

    public function deleteDataByImportFromRecord(Record $record, RecordImport $recordImport): int
    {
        return Drifter::query()->whereRecordId($record->id)->whereRecordImportId($recordImport->id)->delete();
    }

    /**
     * @throws \JsonException
     */
    private function validateData(array $data): void
    {
        if (
            ! isset($data['record_id'])
        ) {
            throw new RuntimeException('Failed to validate data...'.json_encode($data, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));
        }
    }

    public function getSingleColumnFromRecord(Record $record, string $column): \Illuminate\Support\Collection
    {
        return \DB::table(Drifter::TABLE)->where(Record::RECORD_ID_FIELD, $record->id)->pluck($column);
    }

    public function getStepIdField(): string
    {
        return Record::STEP_ID_FIELD_PLUGIN;
    }

    public function getRecordImportIdField(): string
    {
        return Record::RECORD_IMPORT_ID_FIELD_PLUGIN;
    }
}
