<?php

namespace Plugins\drifters\Models;

use Illuminate\Database\Eloquent\Model;

class Drifter extends Model
{
    public $timestamps = false;

    public const TABLE = 'drifters';

    protected $table = self::TABLE;

    public const RECORD_ID_FIELD = 'record_id';

    public const RECORD_IMPORT_FIELD = 'record_import_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step_id',
        'latitude',
        'longitude',

        'speed',
        'drifter_number',
        'distance',
        'utc_diff',

        'date_time',
        'record_id',
        'record_import_id',
    ];

    protected $casts = [
        'date_time' => 'datetime',
    ];
}
