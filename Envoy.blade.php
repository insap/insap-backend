@setup
    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();
@endsetup

@servers(['production' => [env('DEPLOY_USER') . '@' . env('DEPLOY_HOST')]])

@task('deploy', ['on' => 'production'])
cd {{ env('DEPLOY_PATH') }}
sudo git pull
sudo make command 'composer i && php artisan migrate --force && php artisan optimize'
@endtask

@task('deploy_docker', ['on' => 'production'])
cd {{ env('DEPLOY_PATH') }}
{{--make up_prod--}}
make restart_deploy
@endtask

@task('deploy_build', ['on' => 'production'])
cd {{ env('DEPLOY_PATH') }}
docker compose build
@endtask

