#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

SHELL = /bin/bash
DC_RUN_ARGS = --rm --user "$(shell id -u):$(shell id -g)"
DC_PRE_RUN_ARGS = -f docker/docker-compose.yml -f docker/docker-compose.override.yml

.PHONY : help build init php fresh test up down container restart restart_deploy ps lint command deploy logs
	gitlab_driftery
.DEFAULT_GOAL : help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

build: ## Build
	docker compose $(DC_PRE_RUN_ARGS) build

init: ## Make full application initialization
	# Install dependencies & app key
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) --no-deps app composer install
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) --no-deps app php ./artisan key:generate
	# Run migrations && seeders
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php ./artisan fresh:project
	# Admin Commands. Create default users and etc
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php ./artisan admin:install
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php ./artisan admin:import helpers
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php ./artisan admin:import log-viewer
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php ./artisan admin:import summernote
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php ./artisan admin:install
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php ./artisan admin:generate-menu
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php artisan vendor:publish --provider="Encore\Admin\AdminServiceProvider"
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php artisan telescope:install
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app php artisan horizon:install

php: ## Start shell into app container
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) app sh

fresh: ## Fresh project
	docker compose $(DC_PRE_RUN_ARGS) exec web php artisan fresh:project --clear-files
	docker compose $(DC_PRE_RUN_ARGS) exec web php artisan reimport:journals
	docker compose $(DC_PRE_RUN_ARGS) exec web php artisan reimport:users

test: ## Execute app tests
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) --no-deps app composer test

up: ## Create and start containers
	APP_UID=$(shell id -u) APP_GID=$(shell id -g) docker compose $(DC_PRE_RUN_ARGS) up --detach --remove-orphans web queue cron horizon

down: ## Stop containers
	docker compose $(DC_PRE_RUN_ARGS) down --remove-orphans

restart: down up ## Restart all containers

restart_deploy:
	docker compose $(DC_PRE_RUN_ARGS) restart web
	docker compose $(DC_PRE_RUN_ARGS) restart queue
	docker compose $(DC_PRE_RUN_ARGS) restart cron
	docker compose $(DC_PRE_RUN_ARGS) restart horizon


container: ## Up only one container
	APP_UID=$(shell id -u) APP_GID=$(shell id -g) docker compose $(DC_PRE_RUN_ARGS) up --detach --remove-orphans $(filter-out $@,$(MAKECMDGOALS))


ps: # PS
	docker compose $(DC_PRE_RUN_ARGS) ps -a

lint:
	docker compose $(DC_PRE_RUN_ARGS) run $(DC_RUN_ARGS) --no-deps app /bin/sh -c './vendor/bin/pint'

command:
	docker compose $(DC_PRE_RUN_ARGS) exec web /bin/sh -c '$(filter-out $@,$(MAKECMDGOALS))'

logs:
	docker compose $(DC_PRE_RUN_ARGS) logs $(filter-out $@,$(MAKECMDGOALS))

deploy: # Deploy to Insap Prod
	php vendor/bin/envoy run deploy  || php vendor/bin/envoy run deploy_docker

gitlab_driftery: # Download actual exporter version
	docker compose $(DC_PRE_RUN_ARGS) exec web php artisan gitlab:download-handler exporter-php-driftery-sts 8 --update-exist
