<?php

declare(strict_types=1);

namespace App\Enums;

enum InsapServiceEnum: string
{
    case Default = '';
    case Insap = 'insap';
}
