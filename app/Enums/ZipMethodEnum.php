<?php

declare(strict_types=1);

namespace App\Enums;

enum ZipMethodEnum: string
{
    case Php = 'php';
    case Linux = 'linux';

    case PowerShell = 'powershell';
}
