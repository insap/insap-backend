<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;
use Symfony\Component\VarDumper\Caster\ReflectionCaster;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;
use Symfony\Component\VarDumper\VarDumper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $driver = app()->environment('production') ? \Mail::getDefaultDriver() : 'log';
        //\Mail::setDefaultDriver($driver);

        Passport::enablePasswordGrant();

        VarDumper::setHandler(function ($var, ?string $label = null) {
            $cloner = new VarCloner();
            $cloner->addCasters(ReflectionCaster::UNSET_CLOSURE_FILE_INFO);
            $var = $cloner->cloneVar($var);
            if ($label !== null) {
                $var = $var->withContext(['label' => $label]);
            }
            $dumper = app()->runningInConsole() ? new CliDumper() : new HtmlDumper();
            $dumper->dump($var);
        });
    }
}
