<?php

namespace App\Providers;

use App\Services\ZipperService;
use App\Services\ZipperServiceInterface;
use Illuminate\Support\ServiceProvider;

class ServicesServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(ZipperServiceInterface::class, ZipperService::class);

    }

    public function boot(): void
    {
    }
}
