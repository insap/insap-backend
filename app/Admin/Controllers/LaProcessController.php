<?php

namespace App\Admin\Controllers;

use App\Enums\ActiveStatusEnum;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Illuminate\Support\MessageBag;
use Laravel\Telescope\Telescope;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessApp\ProcessAppServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\User\Entities\User;

/** @mixin User */
class LaProcessController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Обработчики';

    /**
     * Make a grid builder.
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Process());

        $grid->column('id', __('id'));
        $grid->column('name', __('Логин'));
        $grid->column('description', __('Описание'));
        $grid->column('interpreter', __('Язык'))->using(ProcessInterpreterEnum::labels());
        $grid->column('type', __('Тип'))->using(ProcessTypeEnum::toSelect());
        $grid->column('appliance.name', 'Прибор');
        $grid->column('plugin.name', 'Плагин');
        $grid->column('user.name', 'Создатель');
        $grid->column('appliance.name', 'Прибор');

        $grid->column('image', __('Image'))->display(function () {
            // @phpstan-ignore-next-line
            return $this->image;
        })->image('', 50, 50);

        $grid->column('is_active', 'Статус')->using(ActiveStatusEnum::toSelect())->label(ActiveStatusEnum::toLabels());

        return $grid;
    }

    /**
     * Make a form builder.
     */
    protected function form($id = null): Form
    {
        $applianceService = app(ApplianceServiceInterface::class);
        $pluginService = app(PluginServiceInterface::class);
        $processService = app(ProcessServiceInterface::class);
        $processAppService = app(ProcessAppServiceInterface::class);

        $form = new Form(new Process());

        $form->tab('Основные данные', function (Form $form) use ($pluginService, $applianceService) {
            $form->display('id', 'ID');
            $form->text('name', 'Имя')->required();
            $form->text('description', 'Описание')->required();

            $form->hidden('user_id');

            $form->select('interpreter', 'Язык программирования')->options(ProcessInterpreterEnum::labels())
                ->default(ProcessInterpreterEnum::Php->value)->required();

            $form->select('type', 'Тип обработчика')->options(ProcessTypeEnum::toSelect())
                ->default(ProcessTypeEnum::Importer->value)->required();

            $form->select('is_active', 'Активность')->options(ActiveStatusEnum::toSelect())
                ->default(ActiveStatusEnum::Active->value)->required();

            $form->select('appliance_id', 'Прибор')->options($applianceService->getAllAppliances()->pluck('name', 'id'))->required();
            $form->select('plugin_id', 'Плагин')->options($pluginService->all()->pluck('name', 'id'));

            $form->image('imageFile.url', 'Изображение (Если необходимо');
            $form->file('archive_file', 'Файл с программой');

            $form->display('created_at', __('Дата создания'));
        })->tab('Данные из импортера', function (Form $form) use ($processService, $id) {
            if ($id) {
                $form->embeds('options', 'Параметры из импортера', function (Form\EmbeddedForm $embeddedForm) use ($id, $processService) {
                    $embeddedForm->display('name', 'Название импортера');
                    $embeddedForm->display('description', 'Описание импортера');
                    $embeddedForm->display('version', 'Версия импортера');
                    $embeddedForm->display('date', 'Дата обновления');

                    $embeddedForm->divider();

                    $model = $processService->getById($id);

                    foreach ($model->option_fields as $field) {
                        $embeddedForm->html('<pre>'.json_encode($field, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE).'</pre>');
                    }
                });
            }
        });

        $form->saving(static function (Form $form) use ($pluginService, $processService, $processAppService) {
            if ($form->isCreating()) {
                $dto = new ProcessDto(
                    processType: ProcessTypeEnum::from($form->type),
                    processInterpreter: ProcessInterpreterEnum::from($form->interpreter),
                    activeStatus: ActiveStatusEnum::from($form->is_active),
                    name: $form->name,
                    description: $form->description,
                    userId: \Auth::user()->id,
                    pluginId: $form->plugin_id,
                    applianceId: $form->appliance_id
                );

                try {
                    $process = $processService->createWithApp($dto, $form->archive_file);
                } catch (\Throwable $throwable) {
                    $error = new MessageBag([
                        'title' => 'Exception',
                        'message' => $throwable->getMessage(),
                    ]);

                    Telescope::catch($throwable);

                    return back()->with(compact('error'));
                }

                return redirect('adminx/processes/'.$process->id.'/edit');
            }

            if ($form->isEditing()) {
                try {
                    if ($form->archive_file) {
                        $process = $processService->getById($form->id);
                        $processAppService->updateApp($process, $form->archive_file);
                    }

                    $form->ignore('archieve_file');

                    if ($form->plugin_id || $form->plugin_id === null) {
                        $process = $processService->getById($form->id);
                        $plugin = $pluginService->findById($form->plugin_id);

                        $processService->updatePlugin($process, $plugin);
                    }

                    $form->ignore('plugin_id');
                } catch (\Throwable $throwable) {
                    $error = new MessageBag([
                        'title' => 'Exception',
                        'message' => $throwable->getMessage().PHP_EOL.$throwable->getTraceAsString(),
                    ]);

                    Telescope::catch($throwable);

                    return back()->with(compact('error'));
                }
            }
        });

        return $form;
    }
}
