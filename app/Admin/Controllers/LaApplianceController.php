<?php

namespace App\Admin\Controllers;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Modules\Appliance\Models\Appliance;
use Modules\User\Entities\User;

/** @mixin User */
class LaApplianceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * Make a grid builder.
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Appliance());

        $grid->column('id', __('id'));
        $grid->column('name', __('Логин'));
        $grid->column('description', __('Описание'));
        $grid->column('is_active', 'Статус')->using(ActiveStatusEnum::toSelect())->label(ActiveStatusEnum::toLabels());

        $grid->column('user.name', __('Создал'));

        $grid->column('created_at', __('Created at'))->display(fn ($date) => Carbon::parse($date)->format('d-m-Y H:i:s'));
        $grid->column('updated_at', __('Updated at'))->display(fn ($date) => Carbon::parse($date)->format('d-m-Y H:i:s'));

        return $grid;
    }

    /**
     * Make a form builder.
     */
    protected function form(): Form
    {
        $form = new Form(new Appliance());

        $form->display('id', 'ID');
        $form->text('name', 'Имя');
        $form->text('description', 'Описание');

        $form->select('is_active', 'Активность')->options(ActiveStatusEnum::toSelect())
            ->default(ActiveStatusEnum::Active->value);

        $form->display('user.name', 'Создал');
        $form->hidden('user_id');

        $form->display('created_at', __('Created at'));
        $form->display('updated_at', __('Updated at'));

        $form->saving(static function (Form $form) {
            // @phpstan-ignore-next-line
            if ($form->isCreating()) {
                $form->user_id = \Auth::user()->id;
            }

            dump(request()::getHttpMethodParameterOverride());
            \rr\dump(request()::getHttpMethodParameterOverride());
        });

        return $form;
    }
}
