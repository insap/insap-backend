<?php

namespace App\Admin\Controllers;

use App\Enums\ActiveStatusEnum;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Modules\Journal\Models\Journal;
use Modules\User\Entities\User;

/** @mixin User */
class LaJournalController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Журналы';

    /**
     * Make a grid builder.
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Journal());

        $grid->column('id', __('id'));
        $grid->column('name', __('Название'));
        $grid->column('description', __('Описание'));
        $grid->column('order', __('Порядок отображения'));
        $grid->column('date_start', __('Начало экспедиции'));
        $grid->column('date_end', __('Окончание экспедиции'));
        $grid->column('user.name', 'Создатель');

        $grid->column('image', __('Изображение'))->display(function () {
            // @phpstan-ignore-next-line
            return $this->image;
        })->image('', 50, 50);

        $grid->column('is_active', 'Статус')->using(ActiveStatusEnum::toSelect())->label(ActiveStatusEnum::toLabels());

        return $grid;
    }

    /**
     * Make a form builder.
     */
    protected function form($id = null): Form
    {
        $form = new Form(new Journal());

        $form->display('id', 'ID');
        $form->text('name', 'Имя');
        $form->text('description', 'Описание');
        $form->number('order', 'Порядок отображения');

        $form->datetime('date_start', 'Начало экспедиции');
        $form->datetime('date_end', 'Окончание экспедиции');

        $form->image('imageFile.url', 'Изображение');

        $form->display('user.name', 'Создатель');

        $form->select('is_active', 'Активность')->options(ActiveStatusEnum::toSelect())
            ->default(ActiveStatusEnum::Active->value);

        $form->display('created_at', __('Created at'));

        return $form;
    }
}
