<?php

namespace App\Admin\Controllers;

use App\Enums\ActiveStatusEnum;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Modules\File\Entities\File;
use Modules\User\Entities\User;

/** @mixin User */
class LaFileController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Файлы';

    /**
     * Make a grid builder.
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new File());

        $grid->column('id', __('id'));
        $grid->column('name', __('Название файла'));
        $grid->column('path', __('Путь к файлу'));
        $grid->column('url', __('URL'));
        $grid->column('mime', __('Mime'));
        $grid->column('user.name', 'Создатель');

        $grid->column('is_active', 'Статус')->using(ActiveStatusEnum::toSelect())->label(ActiveStatusEnum::toLabels());

        return $grid;
    }

    /**
     * Make a form builder.
     */
    protected function form($id = null): Form
    {
        $form = new Form(new File());

        $form->display('id', 'ID');
        $form->display('name', 'Название файла');
        $form->display('path', 'Путь к файлу');
        $form->display('url', 'URL');
        $form->display('mime', 'URL');
        $form->display('user.name', 'Создатель');

        $form->select('is_active', 'Активность')->options(ActiveStatusEnum::toSelect())
            ->default(ActiveStatusEnum::Active->value);

        return $form;
    }
}
