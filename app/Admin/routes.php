<?php

use App\Admin\Controllers\HomeController;
use App\Admin\Controllers\LaApplianceController;
use App\Admin\Controllers\LaFileController;
use App\Admin\Controllers\LaJournalController;
use App\Admin\Controllers\LaProcessController;
use App\Admin\Controllers\LaUserController;
use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix' => config('admin.route.prefix'),
    'middleware' => config('admin.route.middleware'),
    'as' => config('admin.route.prefix').'.',
], function (Router $router) {
    $router->get('/', [HomeController::class, 'index'])->name('home');

    $router->resource('users', LaUserController::class);
    $router->resource('appliances', LaApplianceController::class);
    $router->resource('processes', LaProcessController::class);
    $router->resource('files', LaFileController::class);
    $router->resource('journals', LaJournalController::class);
});
