<?php

declare(strict_types=1);

namespace App\Admin\Selectable;

use Encore\Admin\Grid\Selectable;

class Appliance extends Selectable
{
    public $model = \Modules\Appliance\Models\Appliance::class;

    public function make()
    {
        $this->column('id', '#');
        $this->column('name', 'Название прибора');
    }
}
