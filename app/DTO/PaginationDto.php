<?php

declare(strict_types=1);

namespace App\DTO;

class PaginationDto
{
    public function __construct(
        public int $total,
        public int $page,
        public int $lastPage,
        public int $perPage
    ) {
    }
}
