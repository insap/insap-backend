<?php

declare(strict_types=1);

namespace App\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class CollectionWithPaginationDto implements Arrayable
{
    public function __construct(public Collection|JsonResource $collection, public ?PaginationDto $paginationDto = null)
    {
    }

    public function toArray(): array
    {
        return [
            'data' => $this->collection,
            'pagination' => $this->paginationDto,
        ];
    }
}
