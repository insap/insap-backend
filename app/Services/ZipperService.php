<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Collection;
use Modules\Process\DTO\ProcessFileDto;
use STS\ZipStream\ZipStream;

class ZipperService implements ZipperServiceInterface
{
    public function createAndDownloadZip(string $destinationFile, Collection $files): ZipStream
    {
        return \Zip::create($destinationFile, $files->map(fn (ProcessFileDto $dto) => $dto->getUploadedFile()->getRealPath())->toArray());
    }
}
