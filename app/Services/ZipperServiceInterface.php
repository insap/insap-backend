<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Collection;
use STS\ZipStream\ZipStream;

interface ZipperServiceInterface
{
    public function createAndDownloadZip(string $destinationFile, Collection $files): ZipStream;
}
