<?php

declare(strict_types=1);

namespace App\Services\StaticLogger;

class StaticLogger
{
    private string $log;

    private ?float $time;

    public function __construct()
    {
        $this->log = '';
    }

    public function reset(): self
    {
        $this->log = '';

        return $this;
    }

    public function add(string $logMessage, bool $trackTime = false): self
    {
        $this->log .= ' '.$logMessage;
        $this->time = $trackTime ? microtime(true) : null;

        return $this;
    }

    public function addSuccess(bool $trackTime = false): self
    {
        $message = '... OK';

        if ($trackTime) {
            $message .= $this->getTrackTime();
        }

        $message .= PHP_EOL;
        $this->log .= $message;

        return $this;
    }

    public function addError(string $error, bool $trackTime = false): self
    {
        $message = '... Error ['.$error.']';

        if ($trackTime) {
            $message .= $this->getTrackTime();
        }

        $message .= PHP_EOL;
        $this->log .= $message;

        return $this;
    }

    private function getTrackTime(): string
    {
        return ' ['.($this->time ?
            number_format((microtime(true) - $this->time), 2, '.', '') : 'null').'s]';
    }

    public function getLog(): string
    {
        return $this->log;
    }
}
