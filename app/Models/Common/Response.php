<?php

namespace App\Models\Common;

use App\Enums\ResponseStatusEnum;
use App\Helpers\BytesForHuman;
use App\Traits\Makeable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Laravel\Octane\Exceptions\DdException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Throwable;

class Response implements Arrayable
{
    use Makeable;

    protected ResponseStatusEnum $status;

    protected mixed $data = [];

    protected bool $hasErrors = false;

    protected array $errors = [];

    protected array $validationErrors = [];

    protected float $executionTime = 0;

    protected int $memoryUsageStart;

    protected string $memoryPeakUsage;

    protected Carbon $date;

    /**
     * Response constructor.
     */
    public function __construct()
    {
        // Sets memory usage
        $this->memoryUsageStart = memory_get_usage();
        $this->memoryPeakUsage = BytesForHuman::format($this->memoryUsageStart);
        $this->executionTime = microtime(true);

        $this->status = ResponseStatusEnum::Unknown;
        $this->date = Carbon::now();
    }

    /**
     *  Return response with given error
     *
     * @return $this
     */
    public function withError(int $errorCode, string $message): self
    {
        $this->hasErrors = true;
        $this->status = ResponseStatusEnum::Error;

        $this->errors[] = [
            'code' => $errorCode,
            'message' => $message,
        ];

        return $this;
    }

    /**
     * @return $this
     */
    public function catch(Throwable $throwable): Response
    {
        throw_if($throwable instanceof DdException, $throwable);

        report($throwable);
        $code = $throwable->getCode() ?: SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR;

        return $this->withError($code, $throwable->getMessage());
    }

    public function file(string $fileName): BinaryFileResponse
    {
        return response()->download($fileName)->deleteFileAfterSend();
    }

    public function withData(mixed $data): Response
    {
        $this->data = $data;
        $this->status = ResponseStatusEnum::Success;

        $this->memoryPeakUsage = BytesForHuman::format(memory_get_usage() - $this->memoryUsageStart);

        return $this;
    }

    public function withStatus(int $status): Response
    {
        $this->status = ResponseStatusEnum::from($status);

        return $this;
    }

    /**
     * @return $this
     */
    private function withMessage(string $message = ''): Response
    {
        if (! $message) {
            return $this;
        }
        $this->data['message'] = $message;

        return $this;
    }

    public function success(string $message = ''): Response
    {
        return $this->withStatus(ResponseStatusEnum::Success->value)->withMessage($message);
    }

    public function validation(Validator $validator): Response
    {
        if ($validator->fails()) {
            $this->withError(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY, 'Validation Errors');
            $this->withStatus(ResponseStatusEnum::Error->value);

            foreach ($validator->errors()->getMessages() as $field => $error) {
                $this->validationErrors[$field] = $error;
            }
        }

        return $this;
    }

    public function toArray(): array
    {
        return [
            'data' => $this->data instanceof Arrayable ? $this->data->toArray() : $this->data,
            'status' => $this->status->value,
            'hasErrors' => $this->hasErrors,
            'errors' => $this->errors,
            'validationErrors' => $this->validationErrors,
            'execution_time' => microtime(true) - $this->executionTime,
            'memory_start_usage' => BytesForHuman::format($this->memoryUsageStart),
            'memory_peak_usage' => $this->memoryPeakUsage,
            'memory_end_usage' => BytesForHuman::format(abs(memory_get_usage() - $this->memoryUsageStart)),
            'date' => $this->date->format('Y-m-d H:i:s'),
        ];
    }

    public function response(int $statusCode = SymfonyResponse::HTTP_OK, array $headers = []): JsonResponse
    {
        return response()->json($this->toArray(), $statusCode, $headers);
    }
}
