<?php

namespace App\Helpers;

class LaravelHelper
{
    public static function getEloquentSqlWithBindings(mixed $query): string
    {
        return vsprintf(
            format: str_replace('?', '%s', $query->toSql()),
            values: collect($query->getBindings())
                ->map(fn (string|int $binding) => is_numeric($binding) ? $binding : "'{$binding}'")->toArray());
    }
}
