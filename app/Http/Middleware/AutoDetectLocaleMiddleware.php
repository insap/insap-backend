<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AutoDetectLocaleMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $locale = substr($request->header('Accept-Language', 'ru'), 0, 2);

        if (in_array($locale, ['ru', 'en'], true) && $locale !== app()->currentLocale()) {
            app()->setLocale($locale);
        }

        return $next($request);
    }
}
