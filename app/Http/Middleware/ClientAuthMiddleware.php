<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Laravel\Passport\Passport;
use Laravel\Passport\Token;
use League\OAuth2\Server\CryptKey;
use Modules\User\Entities\User;

class ClientAuthMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $user = $this->getUser();

        auth()->login($user);

        return $next($request);
    }

    private function getUser(): User
    {
        /** TODO: переработать */
        $accessToken = request()->bearerToken();

        if (! $accessToken) {
            abort(401);
        }

        try {
            $cryptKey = new CryptKey('file://'.Passport::keyPath('oauth-public.key'), null, false);
            $key = new Key($cryptKey->getKeyContents(), 'RS256');
            $decoded = JWT::decode($accessToken, $key);

            $token = Token::where('id', $decoded->jti)->firstOrFail();
        } catch (\Throwable $throwable) {
            report($throwable);
            abort(403);
        }

        return User::findOrFail($token->user_id);
    }
}
