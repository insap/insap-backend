<?php

namespace App\Console\Commands;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Illuminate\Console\Command;
use JsonMachine\Items;
use Modules\Journal\DTO\JournalCreateDto;
use Modules\Journal\Models\Journal;
use Modules\Journal\Services\JournalServiceInterface;
use Modules\User\Services\UserServiceInterface;

class ReImportJournalsCommand extends Command
{
    protected $signature = 'reimport:journals';

    protected $description = 'Command description';

    public function handle(JournalServiceInterface $journalService, UserServiceInterface $userService): void
    {
        $storage = \Storage::disk('local');
        $expeditionsFilePath = storage_path('app/old/expeditions.json');

        if (! \File::exists($expeditionsFilePath)) {
            $this->warn('Файла с экспедициями нету');

            return;
        }

        $user = $userService->getUserByEmail(config('app.admin_default_email'));
        foreach (Items::fromFile($expeditionsFilePath) as $item) {
            $item = (array) $item;
            $exist = Journal::whereSlug($item['id_exp'])->first();

            if ($exist) {
                $exist->update([
                    'date_start' => Carbon::parse($item['start_date']),
                    'date_end' => Carbon::parse($item['end_date']),
                ]);

                continue;
            }

            $journal = $journalService->create(new JournalCreateDto(
                name: $item['name'],
                activeStatusEnum: ActiveStatusEnum::Active,
                dateStart: Carbon::parse($item['start_date']),
                dateEnd: Carbon::parse($item['end_date']),
                creatorUserId: $user->id
            ));

            $journal->slug = $item['id_exp'];

            $tags = $journal->tags();
            $tags->create([
                'name' => $item['sea'],
                'is_filter' => false,
            ]);

            foreach ($item['appliances'] ?? [] as $appliance) {
                $tags->create([
                    'name' => $appliance->id_app,
                    'is_filter' => false,
                ]);
            }

            $journal->save();
        }
    }
}
