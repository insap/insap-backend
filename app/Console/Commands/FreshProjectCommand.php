<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Modules\Plugin\Models\Plugin;

class FreshProjectCommand extends Command
{
    protected $signature = 'fresh:project {--clear-files}';

    protected $description = 'Command description';

    public function handle(): void
    {
        if ($this->option('clear-files')) {
            $this->clearFileDir();
        }

        $this->call('migrate:fresh', ['--seed' => true, '--force' => true]);

        foreach (Plugin::all() as $plugin) {
            $this->call(MigratePluginCommand::class, ['slug' => $plugin->slug]);
        }

    }

    private function clearFileDir(): void
    {
        $storage = Storage::disk('fileStore');
        $storage->delete($storage->allFiles());

        $storage = Storage::disk('files');
        $storage->delete($storage->allFiles());

        $storage = Storage::disk('process');
        foreach ($storage->allDirectories() as $directory) {
            $storage->deleteDirectory($directory);
        }

    }
}
