<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Appliance\Models\Appliance;
use Modules\Journal\Models\Journal;
use Modules\Record\Models\Record;
use Plugins\ctd\Models\Ctd;

class ReportOuCommand extends Command
{
    protected $signature = 'report:ou';

    protected $description = 'Command description';

    public function handle(): void
    {
        app()->setLocale('ru');
        Carbon::setLocale('ru');
        setlocale(LC_TIME, 'ru_RU.UTF-8');

        $appliances = Appliance::query()->get();

        foreach (Journal::query()->cursor() as $journal) {
            $fullResult = '';
            $records = $journal->records;

            $dates = $journal->date_start->translatedFormat('F');
            if ($journal->date_start->month !== $journal->date_end->month) {
                $dates .= ' - ' . $journal->date_end->translatedFormat('F');
            }

            $fullResult .= $journal->name . PHP_EOL;
            $fullResult .='Даты: ' . $dates . PHP_EOL;

            foreach ($appliances as $appliance) {
                $appRecords = $records->filter(fn(Record $record) => $record->appliance_id === $appliance->id);
                $result = '';

                if ($appliance->name === 'ADCP' && $appRecords->count() > 0) {
                    $result = $appliance->name . ' - ' . $appRecords->count() . ' разреза(ов)';
                } elseif ($appliance->name === 'CTD' && $appRecords->count() > 0) {
                    $stations = 0;
                    foreach ($appRecords as $appRecord) {
                        $stations += Ctd::whereRecordId($appRecord->id)->get()->pluck('station')->unique()->count();
                    }

                    $result = $appliance->name . ' - ' . $appRecords->count() . ' выходов. ' . $stations . ' танций';
                } elseif ($appliance->slug === 'Дрифтеры' && $appRecords->count() > 0) {
                    $result = $appliance->name . ' - ' . $appRecords->count() . ' дрифтера(ов)';
                }

                if ($result) {
                    $fullResult .= $result . PHP_EOL;
                }

            }

            echo $fullResult;
            #$this->components->info($fullResult);
        }
    }
}
