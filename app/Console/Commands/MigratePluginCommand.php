<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MigratePluginCommand extends Command
{
    protected $signature = 'migrate:plugin {slug}';

    protected $description = 'Exec migrations for plugin';

    public function handle(): void
    {
        $slug = $this->argument('slug');
        //$database = $this->argument('database');

        $path = 'plugins'.DIRECTORY_SEPARATOR.$slug.DIRECTORY_SEPARATOR.'migrations';
        $migrationDir = base_path('plugins').DIRECTORY_SEPARATOR.$slug.DIRECTORY_SEPARATOR.'migrations';

        if (\File::isDirectory($migrationDir) === false) {
            throw new \RuntimeException('Invalid migration folder name: '.$migrationDir);
        }

        Artisan::call('migrate', ['--path' => $path, '--force' => true]);
        $this->info(Artisan::output());
    }
}
