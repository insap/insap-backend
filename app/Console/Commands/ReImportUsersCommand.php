<?php

namespace App\Console\Commands;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Illuminate\Console\Command;
use JsonMachine\Items;
use Modules\User\Entities\User;

class ReImportUsersCommand extends Command
{
    protected $signature = 'reimport:users';

    protected $description = 'Command description';

    public function handle(): void
    {
        $file = storage_path('app/old/users.json');

        if (! \File::exists($file)) {
            $this->warn('Файла с экспедициями нету');

            return;
        }

        foreach (Items::fromFile($file) as $userArray) {
            $userArray = (array) $userArray;

            if (! User::whereEmail($userArray['email'])->exists()) {
                User::create([
                    'name' => $userArray['name'],
                    'email' => $userArray['email'],
                    'password' => \Hash::make($userArray['password']),
                    'email_verified_at' => Carbon::now(),
                    'is_active' => ActiveStatusEnum::Active,
                ]);

                $this->info('Создан пользователь '.$userArray['name'].' ('.$userArray['email'].')');
            }
        }
    }
}
