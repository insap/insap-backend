<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Modules\Process\Services\ProcessApp\ProcessAppServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use PhpZip\ZipFile;
use Str;
use ZipArchive;

class GitlabDownloadHandler extends Command
{
    protected $signature = 'gitlab:download-handler {handler-project-name} {process-id} {--update-exist}';

    protected $description = 'Command description';

    public function handle(ProcessAppServiceInterface $processAppService, ProcessServiceInterface $processService): void
    {
        $storage = Storage::disk('examples');
        $handler = $this->argument('handler-project-name');
        $processId = (int) $this->argument('process-id');

        $handlers = [$handler];
        foreach ($handlers as $handler) {
            $path = $handler.'-main.zip';
            $url = "https://gitlab.com/insap/handlers/$handler/-/archive/main/$path";

            //https://gitlab.com/insap/handlers/importer-php-ctd/-/archive/master/importer-php-ctd-master.zip
            $content = \Http::withHeader('PRIVATE-TOKEN', config('app.gitlab_token'))->get($url);

            if ($content->status() === 404) {
                $path = $handler.'-master.zip';
                $url = "https://gitlab.com/insap/handlers/$handler/-/archive/master/$path";

                $content = \Http::withHeader('PRIVATE-TOKEN', config('app.gitlab_token'))->get($url);

                if ($content->status() === 404) {
                    dd($content);
                }
            }


            $storage->put($path, $content);

            // Извлекаем файлы на один уровень выше, чтобы был корректный формат
            $this->processZip($path);

            if ($this->option('update-exist')) {
                $process = $processService->getById($processId);

                $uploadedFile = UploadedFile::fake()
                    ->createWithContent(
                        name: $path,
                        content: file_get_contents($storage->path($path))
                    );

                $processAppService->updateApp(
                    process: $process,
                    archive: $uploadedFile
                );

                $this->info('Process '.$process->name.' updated successfully');
            }
        }
    }

    private function processZip(string $sourceArchive)
    {
        $zip = new ZipFile();
        $tmpDir = 'tmp_extract_dir';

        $storage = Storage::disk('examples');
        $storage->makeDirectory($tmpDir);

        $sourceArchivePath = $storage->path($sourceArchive);

        $zip->openFile($sourceArchivePath)
            ->extractTo($storage->path($tmpDir))
            ->close();

        // Удаляем все файлы и папки из старого архива
        $storage->delete($sourceArchive);

        // Получаем список файлов и папок извлеченной директории
        $files = $storage->allFiles($tmpDir);

        // Создаем новый архив с обновленным содержимым
        $zip = (new ZipArchive());
        $zip->open($sourceArchivePath, ZipArchive::CREATE);

        foreach ($files as $file) {
            $entryName = Str::after(Str::after($file, 'tmp_extract_dir/'), '/');
            $zip->addFile($storage->path($file), $entryName);

            $this->info($entryName);

            $storage->delete($storage->path($file));
        }

        $zip->close();
        // Удаляем временную директорию
        $storage->deleteDirectory($tmpDir);
    }
}
