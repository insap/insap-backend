<?php

namespace App\Console\Commands;

use App\Enums\ActiveStatusEnum;
use App\Helpers\BytesForHuman;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Modules\Appliance\Models\Appliance;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\Journal\DTO\JournalFilterDto;
use Modules\Journal\Models\Journal;
use Modules\Journal\Services\JournalServiceInterface;
use Modules\Process\DTO\ProcessFileDto;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Models\Process;
use Modules\Process\Services\ImporterExecuteService;
use Modules\Record\DTO\RecordCreateDto;
use Modules\Record\DTO\RecordImportDto;
use Modules\Record\Enums\RecordImportStatusEnum;
use Modules\Record\Models\Record;
use Modules\Record\Services\RecordImportServiceInterface;
use Modules\Record\Services\RecordServiceInterface;
use Modules\User\Entities\User;
use Modules\User\Services\UserServiceInterface;

class ReImportApplianceDataCommand extends Command
{
    protected $signature = 'reimport:appliance {appliance}';

    protected $description = 'Command description';

    public function handle(
        JournalServiceInterface $journalService,
        UserServiceInterface $userService,
        ApplianceServiceInterface $applianceService
    ): void {
        ini_set('memory_limit', '3G');

        $storage = \Storage::disk('local');
        $slug = $this->argument('appliance');

        $appliance = $applianceService->getBySlug($slug);

        $journals = $journalService->getAllByFilters(new JournalFilterDto())->keyBy('slug');
        $user = $userService->getUserByEmail(config('app.admin_default_email'));

        $this->clearAllApplianceData($appliance);
        $this->call(ReImportJournalsCommand::class);

        switch ($slug) {
            case 'adcp': $this->importAdcpData(
                storage: $storage,
                journals: $journals,
                user: $user
            );
                break;
            case 'ctd':
                $this->importCtdData(
                    storage: $storage,
                    journals: $journals,
                    user: $user
                );
                $this->importCtdDagestanData(
                    storage: $storage,
                    journals: $journals,
                    user: $user
                );
                break;

            case 'driftery': $this->importDriftersData(
                storage: $storage,
                journals: $journals,
                user: $user
            );
                break;
            case 'meteo': $this->importMeteoData(
                storage: $storage,
                journals: $journals,
                user: $user
            );
                break;
        }
    }

    private function clearAllApplianceData(Appliance $appliance): void
    {
        $journalService = app(JournalServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);

        $records = Record::whereApplianceId($appliance->id)->get();

        foreach ($records as $record) {
            $recordService->delete($record, force: true);
        }
    }

    private function importMeteoData(Filesystem $storage, Collection $journals, User $user): void
    {
        $applianceService = app(ApplianceServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);
        $recordImportService = app(RecordImportServiceInterface::class);
        $importerExecuteService = app(ImporterExecuteService::class);

        $slug = 'meteo';
        $appliance = $applianceService->getBySlug($slug);
        $process = Process::whereApplianceId($appliance->id)->whereType(ProcessTypeEnum::Importer->value)->first();

        foreach ($storage->directories('old'.DIRECTORY_SEPARATOR.$slug) as $directory) {

            $expedition = Str::afterLast($directory, DIRECTORY_SEPARATOR);

            $journal = $journals->first(fn (Journal $journal, string $key) => $key === $expedition);

            foreach ($storage->directories($directory) as $dateDirectory) {

                $date = Carbon::createFromFormat('y.m.d', Str::afterLast($dateDirectory, DIRECTORY_SEPARATOR))->startOfDay();

                $record = $recordService->create(new RecordCreateDto(
                    name: 'Данные '.$appliance->name,
                    activeStatus: ActiveStatusEnum::Active,
                    journalId: $journal->id,
                    userId: $user->id,
                    applianceId: $appliance->id,
                    date: $date
                ));

                $files = collect();
                foreach ($storage->files($dateDirectory) as $filePath) {
                    $fileName = Str::afterLast($filePath, DIRECTORY_SEPARATOR);
                    $files->put($fileName, $filePath);
                }

                foreach ($files as $fileName => $filePath) {
                    $recordImport = $recordImportService->create(new RecordImportDto(
                        order: $recordImportService->getNextOrder($record),
                        importStatus: RecordImportStatusEnum::Initial,
                        processId: $process->id,
                        recordId: $record->id
                    ));

                    $params = [
                        'date_time' => $date->format('Y-m-d H:i:s'),
                    ];

                    $dataUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                        $storage->path($filePath),
                        $fileName,
                        $storage->mimeType($filePath),
                        null
                    );

                    $dataUploadedFile = UploadedFile::createFromBase($dataUploadedFile);

                    $dataFile = new ProcessFileDto($dataUploadedFile, 'log_file');

                    $this->info('Process '.$appliance->name
                        .' expedition:'.$expedition
                        .' date:'.$date->format('d.m.Y H:i:s')
                        .' log_file:'.$dataUploadedFile->getClientOriginalName()
                        .' memory: '.BytesForHuman::format(memory_get_usage())
                    );

                    $importerExecuteService->executeProcess($recordImport, $params, [$dataFile]);
                }
            }

            gc_collect_cycles();
        }
    }

    private function importDriftersDataOld(Filesystem $storage, Collection $journals, User $user): void
    {
        $applianceService = app(ApplianceServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);
        $recordImportService = app(RecordImportServiceInterface::class);
        $importerExecuteService = app(ImporterExecuteService::class);

        $slug = 'driftery';
        $appliance = $applianceService->getBySlug($slug);
        $process = Process::whereApplianceId($appliance->id)->whereType(ProcessTypeEnum::Importer->value)->first();

        foreach ($storage->directories('old'.DIRECTORY_SEPARATOR.$slug) as $directory) {

            $expedition = Str::afterLast($directory, DIRECTORY_SEPARATOR);

            $journal = $journals->first(fn (Journal $journal, string $key) => $key === $expedition);

            if ($journal === null) {
                $this->error("Can't find journal for $expedition");

                return;
            }

            foreach ($storage->directories($directory) as $dateDirectory) {

                $date = Carbon::createFromFormat('y.m.d', Str::afterLast($dateDirectory, DIRECTORY_SEPARATOR))->startOfDay();

                $record = $recordService->create(new RecordCreateDto(
                    name: 'Данные '.$appliance->name,
                    activeStatus: ActiveStatusEnum::Active,
                    journalId: $journal->id,
                    userId: $user->id,
                    applianceId: $appliance->id,
                    date: $date
                ));

                $files = collect();
                foreach ($storage->files($dateDirectory) as $filePath) {
                    $fileName = Str::afterLast($filePath, DIRECTORY_SEPARATOR);
                    $files->put($fileName, $filePath);
                }

                foreach ($files as $fileName => $filePath) {
                    $recordImport = $recordImportService->create(new RecordImportDto(
                        order: $recordImportService->getNextOrder($record),
                        importStatus: RecordImportStatusEnum::Initial,
                        processId: $process->id,
                        recordId: $record->id
                    ));

                    $explode = explode('_', $fileName);

                    $drifterNumber = (int) $explode[0];

                    $explode[1] = str_replace('g', '+', $explode[1]);
                    $explode[1] = str_replace('l', '-', $explode[1]);

                    $utcDiff = (int) $explode[1];

                    $params = [
                        'drifter_number' => $drifterNumber,
                        'utc_diff' => $utcDiff,
                    ];

                    $dataUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                        $storage->path($filePath),
                        $fileName,
                        $storage->mimeType($filePath),
                        null
                    );

                    $dataUploadedFile = UploadedFile::createFromBase($dataUploadedFile);

                    $dataFile = new ProcessFileDto($dataUploadedFile, 'data');

                    $this->info('Process '.$appliance->name
                        .' expedition:'.$expedition
                        .' date:'.$date->format('d.m.Y H:i:s')
                        .' data:'.$dataUploadedFile->getClientOriginalName()
                        .' memory: '.BytesForHuman::format(memory_get_usage())
                    );

                    $importerExecuteService->executeProcess($recordImport, $params, [$dataFile]);

                }
            }

            gc_collect_cycles();
        }
    }

    private function importDriftersData(Filesystem $storage, Collection $journals, User $user): void
    {
        $applianceService = app(ApplianceServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);
        $recordImportService = app(RecordImportServiceInterface::class);
        $importerExecuteService = app(ImporterExecuteService::class);

        $slug = 'driftery';
        $appliance = $applianceService->getBySlug($slug);
        $process = Process::whereApplianceId($appliance->id)->whereType(ProcessTypeEnum::Importer->value)->first();

        foreach ($storage->directories('old'.DIRECTORY_SEPARATOR.'driftery_new') as $directory) {

            $expedition = Str::afterLast($directory, DIRECTORY_SEPARATOR);

            $journal = $journals->first(fn (Journal $journal, string $key) => $key === $expedition);

            if ($journal === null) {
                $this->error("Can't find journal for $expedition");

                return;
            }

            $files = collect();
            foreach ($storage->files($directory) as $filePath) {
                $fileName = Str::afterLast($filePath, DIRECTORY_SEPARATOR);
                $files->put($fileName, $filePath);
            }

            foreach ($files as $fileName => $filePath) {

                $explode = explode('_', $fileName);

                $explode[1] = Str::replace('g', '+', $explode[1]);
                $explode[1] = Str::replace('l', '-', $explode[1]);
                $explode[4] = Str::replace('.xlsx', '', $explode[4]);

                $drifter = [
                    'number' => (int) $explode[0],
                    'utc_diff' => (int) $explode[1],
                    'name' => $explode[2],
                    'date_start' => Carbon::createFromFormat('d-m-Y', $explode[3]),
                    'date_end' => Carbon::createFromFormat('d-m-Y', $explode[4]),
                ];

                $record = $recordService->create(new RecordCreateDto(
                    name: $drifter['name'],
                    activeStatus: ActiveStatusEnum::Active,
                    journalId: $journal->id,
                    userId: $user->id,
                    applianceId: $appliance->id,
                    date: $drifter['date_start'],
                    dateEnd: $drifter['date_end']
                ));

                $recordImport = $recordImportService->create(new RecordImportDto(
                    order: $recordImportService->getNextOrder($record),
                    importStatus: RecordImportStatusEnum::Initial,
                    processId: $process->id,
                    recordId: $record->id
                ));

                $params = [
                    'drifter_number' => $drifter['number'],
                    'utc_diff' => $drifter['utc_diff'],
                ];

                $dataUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                    $storage->path($filePath),
                    $fileName,
                    $storage->mimeType($filePath),
                    null
                );

                $dataUploadedFile = UploadedFile::createFromBase($dataUploadedFile);

                $dataFile = new ProcessFileDto($dataUploadedFile, 'data');

                $this->info('Process '.$appliance->name
                    .' expedition:'.$expedition
                    .' date:'.$drifter['date_start']->format('d.m.Y H:i:s')
                    .' data:'.$dataUploadedFile->getClientOriginalName()
                    .' memory: '.BytesForHuman::format(memory_get_usage())
                );

                $importerExecuteService->executeProcess($recordImport, $params, [$dataFile]);

            }

            gc_collect_cycles();
        }
    }

    private function importCtdData(Filesystem $storage, Collection $journals, User $user): void
    {
        $applianceService = app(ApplianceServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);
        $recordImportService = app(RecordImportServiceInterface::class);
        $importerExecuteService = app(ImporterExecuteService::class);

        $slug = 'ctd';
        $appliance = $applianceService->getBySlug($slug);
        $process = Process::whereApplianceId($appliance->id)->whereType(ProcessTypeEnum::Importer->value)->first();

        foreach ($storage->directories('old'.DIRECTORY_SEPARATOR.$slug) as $directory) {

            $expedition = Str::afterLast($directory, DIRECTORY_SEPARATOR);

            if (
                Str::contains($expedition, 'dagestan_2022') !== false ||
                Str::contains($expedition, 'sochi_2018') !== false
            ) {
                continue;
            }

            $journal = $journals->first(fn (Journal $journal, string $key) => $key === $expedition);

            foreach ($storage->directories($directory) as $dateDirectory) {

                $date = Carbon::createFromFormat('y.m.d', Str::afterLast($dateDirectory, DIRECTORY_SEPARATOR))->startOfDay();

                $record = $recordService->create(new RecordCreateDto(
                    name: 'Данные '.$appliance->name,
                    activeStatus: ActiveStatusEnum::Active,
                    journalId: $journal->id,
                    userId: $user->id,
                    applianceId: $appliance->id,
                    date: $date
                ));

                $files = collect();
                foreach ($storage->files($dateDirectory) as $filePath) {
                    $fileName = Str::afterLast($filePath, DIRECTORY_SEPARATOR);
                    $files->put($fileName, $filePath);
                }

                foreach ($files as $fileName => $filePath) {
                    if (Str::contains($fileName, 'Coordinates') === true) {
                        continue;
                    }

                    $recordImport = $recordImportService->create(new RecordImportDto(
                        order: $recordImportService->getNextOrder($record),
                        importStatus: RecordImportStatusEnum::Initial,
                        processId: $process->id,
                        recordId: $record->id
                    ));

                    $params = [
                        'date_time' => $date->format('Y-m-d H:i:s'),
                    ];

                    $dataUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                        $storage->path($filePath),
                        $fileName,
                        $storage->mimeType($filePath),
                        null
                    );

                    $dataUploadedFile = UploadedFile::createFromBase($dataUploadedFile);

                    $dataFile = new ProcessFileDto($dataUploadedFile, 'data');

                    $refFileName = Str::replace($fileName, 'Coordinates_'.$fileName, $fileName);
                    $refFilePath = Str::replace($fileName, 'Coordinates_'.$fileName, $filePath);

                    $refUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                        $storage->path($refFilePath),
                        $refFileName,
                        $storage->mimeType($refFilePath),
                        null
                    );

                    $refUploadedFile = UploadedFile::createFromBase($refUploadedFile);

                    $refFile = new ProcessFileDto($refUploadedFile, 'coordinates');

                    $this->info('Process '.$appliance->name
                        .' expedition:'.$expedition
                        .' date:'.$date->format('d.m.Y H:i:s')
                        .' ref:'.$refUploadedFile->getClientOriginalName()
                        .' data:'.$dataUploadedFile->getClientOriginalName()
                        .' memory: '.BytesForHuman::format(memory_get_usage())
                    );

                    $importerExecuteService->executeProcess($recordImport, $params, [$dataFile, $refFile]);
                    gc_collect_cycles();
                }
            }

            gc_collect_cycles();
        }
    }

    private function importCtdDagestanData(Filesystem $storage, Collection $journals, User $user): void
    {
        $applianceService = app(ApplianceServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);
        $recordImportService = app(RecordImportServiceInterface::class);
        $importerExecuteService = app(ImporterExecuteService::class);

        $slug = 'ctd';
        $appliance = $applianceService->getBySlug($slug);
        $process = Process::whereApplianceId($appliance->id)->whereType(ProcessTypeEnum::Importer->value)->whereName('CTD для обработанных')->first();

        foreach ($storage->directories('old'.DIRECTORY_SEPARATOR.$slug) as $directory) {

            $expedition = Str::afterLast($directory, DIRECTORY_SEPARATOR);

            if (
                Str::contains($expedition, 'dagestan_2022') === false &&
                Str::contains($expedition, 'sochi_2018') === false
            ) {
                continue;
            }

            $journal = $journals->first(fn (Journal $journal, string $key) => $key === $expedition);

            foreach ($storage->directories($directory) as $dateDirectory) {
                $date = Carbon::createFromFormat('y.m.d', Str::afterLast($dateDirectory, DIRECTORY_SEPARATOR))->startOfDay();

                $record = $recordService->create(new RecordCreateDto(
                    name: 'Данные '.$appliance->name,
                    activeStatus: ActiveStatusEnum::Active,
                    journalId: $journal->id,
                    userId: $user->id,
                    applianceId: $appliance->id,
                    date: $date
                ));

                $files = collect();
                foreach ($storage->files($dateDirectory) as $filePath) {
                    $fileName = Str::afterLast($filePath, DIRECTORY_SEPARATOR);
                    $files->put($fileName, $filePath);
                }

                foreach ($files as $fileName => $filePath) {
                    $recordImport = $recordImportService->create(new RecordImportDto(
                        order: $recordImportService->getNextOrder($record),
                        importStatus: RecordImportStatusEnum::Initial,
                        processId: $process->id,
                        recordId: $record->id
                    ));

                    $dataUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                        $storage->path($filePath),
                        $fileName,
                        $storage->mimeType($filePath),
                        null
                    );

                    $dataUploadedFile = UploadedFile::createFromBase($dataUploadedFile);

                    $dataFile = new ProcessFileDto($dataUploadedFile, 'data');

                    $this->info('Process '.$appliance->name
                        .' expedition:'.$expedition
                        .' ctd:'.$process->name
                        .' date:'.$date->format('d.m.Y H:i:s')
                        .' data:'.$dataUploadedFile->getClientOriginalName()
                        .' memory: '.BytesForHuman::format(memory_get_usage())
                    );

                    $importerExecuteService->executeProcess($recordImport, ['date_time' => $date->format('Y-m-d H:i:s')], [$dataFile]);
                    gc_collect_cycles();
                }
            }

            gc_collect_cycles();
        }
    }

    private function importAdcpData(Filesystem $storage, Collection $journals, User $user): void
    {
        $applianceService = app(ApplianceServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);
        $recordImportService = app(RecordImportServiceInterface::class);
        $importerExecuteService = app(ImporterExecuteService::class);

        $slug = 'adcp';
        $appliance = $applianceService->getBySlug($slug);
        $process = Process::whereApplianceId($appliance->id)->whereType(ProcessTypeEnum::Importer->value)->first();

        foreach ($storage->directories('old'.DIRECTORY_SEPARATOR.$slug) as $directory) {
            $expedition = Str::afterLast($directory, DIRECTORY_SEPARATOR);

            //                        if ($expedition !== 'kaliningrad_2021'/* && $expedition !== 'kaliningrad_2020'*/) {
            //                            continue;
            //                        }

            $journal = $journals->first(fn (Journal $journal, string $key) => $key === $expedition);

            if (! $journal) {
                $this->info('Aborting Journal with key '.$expedition.'not exists');
                $this->info('Acceptable keys: '.$journals->keys()->implode(','));
                break;
            }

            foreach ($storage->directories($directory) as $dateDirectory) {
                $time = microtime(true);

                $date = Carbon::createFromFormat('y.m.d', Str::afterLast($dateDirectory, DIRECTORY_SEPARATOR))->startOfDay();

                $record = $recordService->create(new RecordCreateDto(
                    name: 'Данные '.$appliance->name,
                    activeStatus: ActiveStatusEnum::Active,
                    journalId: $journal->id,
                    userId: $user->id,
                    applianceId: $appliance->id,
                    date: $date
                ));

                $files = collect();
                foreach ($storage->files($dateDirectory) as $filePath) {
                    $fileName = Str::afterLast($filePath, DIRECTORY_SEPARATOR);
                    $files->put($fileName, $filePath);
                }

                $expeditionNumber = 1;

                try {
                    foreach ($files as $fileName => $filePath) {
                        if (Str::contains($fileName, '_ref') === true) {
                            continue;
                        }

                        $recordImport = $recordImportService->create(new RecordImportDto(
                            order: $recordImportService->getNextOrder($record),
                            importStatus: RecordImportStatusEnum::Initial,
                            processId: $process->id,
                            recordId: $record->id
                        ));

                        $params = [
                            'date_time' => $date->format('Y-m-d H:i:s'),
                            'expedition_number' => $expeditionNumber,
                        ];

                        $dataUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                            $storage->path($filePath),
                            $fileName,
                            $storage->mimeType($filePath),
                            null
                        );

                        $dataUploadedFile = UploadedFile::createFromBase($dataUploadedFile);

                        $dataFile = new ProcessFileDto($dataUploadedFile, 'data');

                        $refFileName = Str::replace('_data', '_ref', $fileName);
                        $refFilePath = Str::replace('_data', '_ref', $filePath);

                        $refUploadedFile = new \Symfony\Component\HttpFoundation\File\UploadedFile(
                            $storage->path($refFilePath),
                            $refFileName,
                            $storage->mimeType($refFilePath),
                            null
                        );

                        $refUploadedFile = UploadedFile::createFromBase($refUploadedFile);

                        $refFile = new ProcessFileDto($refUploadedFile, 'ref');

                        $this->info('Process '.$appliance->name
                            .' expedition:'.$expedition
                            .' date:'.$date->format('d.m.Y H:i:s')
                            .' '.$refUploadedFile->getClientOriginalName()
                            .' expNum:'.$expeditionNumber
                            .' memory: '.BytesForHuman::format(memory_get_usage())
                            .' time: '.(microtime(true) - $time)
                        );

                        $importerExecuteService->executeProcess($recordImport, $params, [$dataFile, $refFile]);
                        $expeditionNumber++;

                        gc_collect_cycles();
                    }
                } catch (\Throwable $throwable) {

                    throw $throwable;
                }

            }

        }
    }
}
