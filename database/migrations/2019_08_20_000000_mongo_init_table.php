<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::connection('mongodb')->dropIfExists('fs.files');
        Schema::connection('mongodb')->dropIfExists('fs.chunks');
    }

    public function down(): void
    {
        Schema::dropIfExists('failed_jobs');
    }
};
