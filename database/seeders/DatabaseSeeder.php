<?php

namespace Database\Seeders;

use App\Enums\InsapServiceEnum;
use Illuminate\Database\Seeder;
use Modules\Appliance\Database\Seeders\ApplianceDatabaseSeeder;
use Modules\Journal\Database\Seeders\JournalDatabaseSeeder;
use Modules\Option\Database\Seeders\OptionDatabaseSeeder;
use Modules\Plugin\Database\Seeders\PluginDatabaseSeeder;
use Modules\Process\Database\Seeders\ProcessDatabaseSeeder;
use Modules\Record\Database\Seeders\RecordDatabaseSeeder;
use Modules\User\Database\Seeders\UserDatabaseSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UserDatabaseSeeder::class,
        ]);

        if (config('app.insap_service') === InsapServiceEnum::Insap->value) {
            $this->call([
                ApplianceDatabaseSeeder::class,
                PluginDatabaseSeeder::class,
            ]);

            if (! app()->runningUnitTests()) {
                $this->passportSeeder();

                $this->call([
                    JournalDatabaseSeeder::class,
                    ProcessDatabaseSeeder::class,
                    RecordDatabaseSeeder::class,
                    OptionDatabaseSeeder::class,
                ]);
            }
        }
    }

    private function passportSeeder(): void
    {
        \Artisan::call('passport:client --name="'.config('app.name').' Personal Access Client'.'" --personal --no-interaction');
        \Artisan::call('passport:client --name="'.config('app.name').' Password Grant Client'.'" --provider=users --no-interaction');
        \Artisan::call('passport:client --name=STS --redirect_uri=http://dev.ocean.smislab.ru/geocover_v4/sts.shtml,http://ocean.smislab.ru/geocover_v4/sts.shtml --no-interaction --public');

        foreach (\DB::table('oauth_clients')->get() as $client) {
            $query = \DB::table('oauth_clients')->where('name', $client->name);

            if ($client->name === 'insap Personal Access Client' && config('passport.personal_access_client.id')) {
                $query->update([
                    'id' => config('passport.personal_access_client.id'),
                    'secret' => config('passport.personal_access_client.secret'),
                ]);
            }

            if ($client->name === 'insap Password Grant Client' && config('passport.password_grand_client.id')) {
                $query->update([
                    'id' => config('passport.password_grand_client.id'),
                    'secret' => config('passport.password_grand_client.secret'),
                    'provider' => 'users',
                    'password_client' => true
                ]);
            }

            if ($client->name === 'STS' && config('passport.sts.id')) {
                $query->update([
                    'id' => config('passport.sts.id'),
                ]);
            }
        }
    }
}
