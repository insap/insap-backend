#!/usr/bin/env bash

# Generate random passwords and secrets
dbPassword=$(xxd -l 12 -p /dev/random)
mongoRootPassword=$(xxd -l 12 -p /dev/random)
mongoPassword=$(xxd -l 12 -p /dev/random)
redisPassword=$(xxd -l 12 -p /dev/random)
minioSecret=$(xxd -l 12 -p /dev/random)
adminDefaultPassword=$(xxd -l 12 -p /dev/random)
personalAccessClientSecret=$(xxd -l 16 -p /dev/random)
personalGrandClientSecret=$(xxd -l 16 -p /dev/random)

macOS=false
# Determine the sed in-place command based on OS
if [ "$(uname)" == "Darwin" ]; then
    sedCmd="sed -i ''"
    macOS=true
else
    sedCmd="sed -i"
fi


# Function to update environment variables in a file
update_env() {
    file=$1
    $sedCmd "s/^DB_PASSWORD=.*/DB_PASSWORD=${dbPassword}/g" "$file"
    $sedCmd "s/^MONGO_DB_ROOT_PASSWORD=.*/MONGO_DB_ROOT_PASSWORD=${mongoRootPassword}/g" "$file"
    $sedCmd "s/^MONGO_DB_PASSWORD=.*/MONGO_DB_PASSWORD=${mongoPassword}/g" "$file"
    $sedCmd "s/^REDIS_PASSWORD=.*/REDIS_PASSWORD=${redisPassword}/g" "$file"
    $sedCmd "s/^MINIO_SECRET=.*/MINIO_SECRET=${minioSecret}/g" "$file"
    if [ "$file" = ".env" ]; then
        $sedCmd "s/^PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET=.*/PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET=${personalAccessClientSecret}/g" "$file"
        $sedCmd "s/^PASSPORT_PASSWORD_GRAND_CLIENT_SECRET=.*/PASSPORT_PASSWORD_GRAND_CLIENT_SECRET=${personalGrandClientSecret}/g" "$file"
        $sedCmd "s/^ADMIN_DEFAULT_PASSWORD=.*/ADMIN_DEFAULT_PASSWORD=${adminDefaultPassword}/g" "$file"
    fi

    # Check for and remove any backup file created by sed on macOS
    if [ "$macOS" = true ]; then
        backupFile="${file}''"
        if [ -f "$backupFile" ]; then
            rm "$backupFile"
        fi
    fi
}

# Update Docker environment variables
cp docker/.env.example docker/.env
update_env "./docker/.env"

# Update Application environment variables
cp .env.example .env
update_env ".env"

# Docker compose override setup
cp docker/docker-compose.override.local.yml docker/docker-compose.override.yml

# Build and initialize the project
make build

# Init Mongo
source docker/.env

make down
docker volume rm $(docker volume ls -q | grep "$COMPOSE_PROJECT_NAME")

make container mongo

echo "Waiting for MongoDB to start..."
until make ps | grep insap-mongo | grep "Up"; do
    echo "MongoDB is not yet available. Checking again in 2 seconds..."
    sleep 2
done

until docker exec -i insap-mongo mongosh --eval "print('waited for connection')" &>/dev/null; do
    echo "Waiting for MongoDB connection..."
    sleep 2
done

cat <<EOF
    use admin
    db.auth("${MONGO_DB_ROOT_USERNAME}", "${MONGO_DB_ROOT_PASSWORD}")
    use db_mongodb_00000000_insap
    db.createUser({
        user: "${MONGO_DB_USERNAME}",
        pwd: "${MONGO_DB_PASSWORD}",
        roles:[
            { "role": "clusterMonitor", "db": "admin" },
            "readWrite",
            "dbAdmin"
        ]
    })
    exit
EOF

docker exec -i insap-mongo mongosh <<EOF
use admin
db.auth("${MONGO_DB_ROOT_USERNAME}", "${MONGO_DB_ROOT_PASSWORD}")
use db_mongodb_00000000_insap
db.createUser({
    user: "${MONGO_DB_USERNAME}",
    pwd: "${MONGO_DB_PASSWORD}",
    roles:[
        { "role": "clusterMonitor", "db": "admin" },
        "readWrite",
        "dbAdmin"
    ]
})
exit
EOF

make init
