# Insap - web-service for in-situ applications

## About Insap

Insap is a web application for working with data from marine expeditions and more. Then main goals of this project is to provide next features:

- Provide cloud storage for your various devices data.
- Automate the data import and export process.
- Provide you with the ability to write scripts(Python, PHP, GO) to process data.
- Provide a user interface for working with data in the browser. (required <span style="color:red">*</span> access to [lux-admin-pro](https://indielayer.gumroad.com/l/lux-admin) )
- Provide an API for external services.


### Installation guide located in [docs/install.md](docs/install/install.md)
