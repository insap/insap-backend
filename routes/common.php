<?php

use App\Http\Controllers\CommonController;

Route::get('/storage/{filename}', [CommonController::class, 'storage'])->where('filename', '^(?!(api)).*$');

Route::get('/insap', static fn () => view('sts'));

// For public application
Route::get('/', static fn () => redirect()->route('app', ['any' => 'home']));
/**
 * Ресурсы хранятся в public/app/*, поэтому без префикса app не будет работать
 */
Route::get('/{any}', static fn () => redirect()->route('app', ['any' => 'home']));
Route::get('/app/{any}', [CommonController::class, 'app'])->where('any', '^(?!api|web).*$')->name('app');
