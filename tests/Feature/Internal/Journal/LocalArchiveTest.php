<?php

declare(strict_types=1);

namespace Tests\Feature\Internal\Journal;

use Illuminate\Http\UploadedFile;
use Modules\Journal\Tests\Fixture\JournalFixture;
use Modules\User\Tests\Fixture\UserFixture;
use Tests\TestCase;

class LocalArchiveTest extends TestCase
{
    private UserFixture $userFixture;
    private JournalFixture $journalFixture;

    public function setUp(): void
    {
        parent::setUp();

        $this->userFixture = resolve(UserFixture::class);
        $this->journalFixture = resolve(JournalFixture::class);
    }

    public function testCanUploadBigFile(): void
    {
        $storage = \Storage::disk('examples');
        $journal = $this->journalFixture->create();
        $this->actingAs(($user = $this->userFixture->create()));

        $response = $this->postJson(route('journals.add-local-archive'), [
            'journal_id' => $journal->id,
            'archive' => UploadedFile::fake()->createWithContent('test_archive.zip', file_get_contents($storage->path('CTD_исходники,_промежуточная_обработка.zip')))
        ]);

        $response->assertStatus(200);
    }
}
