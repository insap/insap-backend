<?php

namespace Tests;

use App\Console\Commands\MigratePluginCommand;
use Artisan;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Modules\Plugin\Models\Plugin;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations, withFaker {
        runDatabaseMigrations as baseRunDatabaseMigrations;
    }

    public function runDatabaseMigrations(): void
    {
        $this->baseRunDatabaseMigrations();
        $this->artisan('db:seed');

        foreach (Plugin::all() as $plugin) {
            Artisan::call(MigratePluginCommand::class, ['slug' => $plugin->slug]);
        }
    }

    public function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->clearFileDir();
        Mail::fake();
        DB::beginTransaction();
    }

    protected function tearDown(): void
    {
        DB::rollBack();
        parent::tearDown();

    }

    private function clearFileDir()
    {
        $storage = Storage::disk('fileStore');
        $storage->delete($storage->allFiles());

        $storage = Storage::disk('process');
        foreach ($storage->allDirectories() as $directory) {
            $storage->deleteDirectory($directory);
        }
    }
}
