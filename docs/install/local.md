## Installation Guide

### Tested on Mac OS (Sonoma)

### Requirements
- WSL2 on Windows | Linux | Mac OS
- Docker 24.0+
- Make (`sudo apt install make` | `brew install make`)

### Host Configuration
#### Windows:
- Open `c:\windows\system32\drivers\etc\hosts`
#### Unix:
- Open `/etc/hosts`
- Add the following lines:
```
127.0.0.1 insap.test
127.0.0.1 app.insap.test
127.0.0.1 minio.insap.test
```

### NPM and Node
- We need to use npm package chokidar to determine file changes while using Octane
- Install Node using instruction from the official site https://nodejs.org/en/download/package-manager/all

#### Laravel Octane setting
- Swoole used by default
- If you want to change octane driver, change `OCTANE_CORE` in `docker/.env.example` . Available variants: roadrunner, swoole, frankenphp
- If you choose roadrunner|frankenphp you must do following steps after setup application:
```shell
make php
php artisan octane:install
# Choose your driver and download binary
```

### Setup Application
- `sh bin/setup.sh`
- `npm i`
- `make down && make up`


### Tests
- `make test`

### Minio
- Open 'http://minio.insap.test'
- LogIn using .env MINIO_KEY and MINIO_SECRET
- Create a bucket with name .env MINIO_BUCKET_FILES

### Admin Panel
- Open 'http://insap.test/adminx'
- LogIn using admin:admin

