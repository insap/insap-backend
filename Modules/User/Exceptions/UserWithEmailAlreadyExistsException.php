<?php

namespace Modules\User\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class UserWithEmailAlreadyExistsException extends HttpException
{
    public function __construct(string $message = 'authorization::authorization.emailAlreadyExist', ?\Throwable $previous = null, array $headers = [], int $code = 0)
    {
        parent::__construct(400, trans($message), $previous, $headers, $code);
    }
}
