<?php

namespace Modules\User\Entities\DTO;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

readonly class RegisterDto implements Arrayable
{
    public function __construct(
        public string $name,
        public ?string $email,
        public ?string $password = null,
        public ?string $phone = null,
        public ?ActiveStatusEnum $status = null,
        public ?bool $verified = false,
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password ? \Hash::make($this->password) : null,
            'phone' => $this->phone,
            'status' => $this->status,
            'email_verified_at' => $this->verified ? Carbon::now() : null,
        ];
    }
}
