<?php

declare(strict_types=1);

namespace Modules\User\Tests\Fixture;

use App\Enums\ActiveStatusEnum;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\User\Entities\DTO\RegisterDto;
use Modules\User\Entities\User;
use Modules\User\Services\UserServiceInterface;

class UserFixture
{
    use WithFaker;

    public function __construct(
        private readonly UserServiceInterface $userService
    ) {
        $this->setUpFaker();
    }

    public function create(): User
    {
        $dto = new RegisterDto(
            $this->faker->name(),
            $this->faker->unique()->safeEmail(),
            $this->faker->password(),
            $this->faker->phoneNumber(),
            ActiveStatusEnum::Active,
            false
        );

        return $this->userService->create($dto);
    }
}
