<?php

namespace Modules\User\Tests\Unit;

use App\Enums\ActiveStatusEnum;
use Illuminate\Http\UploadedFile;
use Modules\File\Services\FileServiceInterface;
use Modules\User\Entities\DTO\RegisterDto;
use Modules\User\Services\UserServiceInterface;
use Modules\User\Tests\Fixture\UserFixture;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    private UserServiceInterface $userService;

    private FileServiceInterface $fileService;

    private UserFixture $userFixture;

    public function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->app->make(UserServiceInterface::class);
        $this->fileService = $this->app->make(FileServiceInterface::class);

        $this->userFixture = $this->app->make(UserFixture::class);
    }

    public function testCreate(): void
    {
        $dto = new RegisterDto(
            $this->faker->name(),
            $this->faker->unique()->safeEmail(),
            $this->faker->password(),
            $this->faker->phoneNumber(),
            ActiveStatusEnum::Active,
            false
        );

        $user = $this->userService->create($dto);

        $this->assertSame($dto->name, $user->name);
        $this->assertSame($dto->email, $user->email);
        $this->assertTrue(\Hash::check((string) $dto->password, (string) $user->password));
        $this->assertSame($dto->phone, $user->phone);
        $this->assertSame($dto->status, $user->is_active);
        $this->assertNull($user->email_verified_at);
    }

    public function testGetUserByEmail(): void
    {
        $user = $this->userFixture->create();

        $null = $this->userService->findUserByEmail('test');

        $this->assertNull($null);

        $someUser = $this->userService->getUserByEmail($user->email);

        $this->assertSame($user->id, $someUser->id);
    }

    public function testChangePhone(): void
    {
        $user = $this->userFixture->create();
        $oldPhone = $user->phone;

        $this->userService->changePhone($user, '+79775428977');

        $this->assertNotSame($user->phone, $oldPhone);
    }

    public function testSamePhone(): void
    {
        $user = $this->userFixture->create();
        $oldPhone = $user->phone;

        $this->expectExceptionMessage(trans('user::user.samePhone'));
        $this->userService->changePhone($user, $oldPhone);

    }

    public function testLocalPhone(): void
    {
        $user = $this->userFixture->create();

        $this->expectExceptionMessage(trans('user::user.localPhone'));
        $this->userService->changePhone($user, '+319775428989');

    }

    public function testRepeatedNumber(): void
    {
        $userOne = $this->userFixture->create();
        $userOne->phone = '+79775555555';
        $userOne->save();

        $userTwo = $this->userFixture->create();

        $this->expectExceptionMessage(trans('user::user.repeatedNumber'));
        $this->userService->changePhone($userTwo, '+79775555555');

    }

    public function testChangeImage(): void
    {
        $user = $this->userFixture->create();
        $uploadedFile = UploadedFile::fake()->image('test_image.png', 100, 100);

        $this->userService->changeImage($user, $uploadedFile, $user);

        $this->assertIsNumeric($user->image_id);

        $this->assertTrue($this->fileService->exists($user->imageFile->path));

        $this->fileService->delete($user->imageFile);
    }

    public function testChangeName(): void
    {
        $user = $this->userFixture->create();
        $newName = $this->faker->name;

        $this->userService->changeName($user, $newName);

        $this->assertSame($newName, $user->name);
    }

    public function testChangePassword(): void
    {
        $user = $this->userFixture->create();
        $password = $this->faker->password;

        $this->userService->changePassword($user, $password);

        $this->assertTrue(\Hash::check($password, $user->password));
    }
}
