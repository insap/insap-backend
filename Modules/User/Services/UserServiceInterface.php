<?php

declare(strict_types=1);

namespace Modules\User\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\User\DTO\UserFiltersDto;
use Modules\User\Entities\DTO\RegisterDto;
use Modules\User\Entities\User;
use Modules\User\Exceptions\UserWithEmailAlreadyExistsException;

interface UserServiceInterface
{
    public function create(RegisterDto $dto): User;

    public function getUserById(int $userId): User;

    public function findUserById(int $userId): ?User;

    public function getUserByEmail(string $email): User;

    public function findUserByEmail(string $email): ?User;

    /**
     * @throws UserWithEmailAlreadyExistsException
     */
    public function changeEmail(User $user, string $email): ?bool;

    public function changeName(User $user, string $name): bool;

    public function changePassword(User $user, string $password): bool;

    /**
     * @throws \Throwable
     */
    public function changePhone(User $user, string $phone): bool;

    public function changeImage(User $user, UploadedFile $uploadedFile, User $userChange): bool;

    public function getAllByFilters(UserFiltersDto $dto): Collection|LengthAwarePaginator;
}
