<?php

declare(strict_types=1);

namespace Modules\User\Services;

use Illuminate\Support\Facades\Mail;
use Modules\Authorization\Mail\ActivationEmailMail;
use Modules\Authorization\Mail\ChangePasswordMail;
use Modules\Authorization\Mail\ResetPasswordMail;
use Modules\User\Entities\User;

class UserEmailService
{
    public function sendActivationEmail(User $user, string $token): void
    {
        Mail::send(new ActivationEmailMail($user, $token));
    }

    public function sendResetPasswordEmail(User $user, string $token): void
    {
        Mail::send(new ResetPasswordMail($user, $token));
    }

    public function sendNewPasswordMail(User $user): void
    {
        Mail::send(new ChangePasswordMail($user));
    }
}
