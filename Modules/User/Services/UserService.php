<?php

declare(strict_types=1);

namespace Modules\User\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;
use Modules\File\Services\FileServiceInterface;
use Modules\User\DTO\UserFiltersDto;
use Modules\User\Entities\DTO\RegisterDto;
use Modules\User\Entities\User;
use Modules\User\Exceptions\UserWithEmailAlreadyExistsException;
use RuntimeException;

class UserService implements UserServiceInterface
{
    public function __construct(
        private readonly FileServiceInterface $fileService
    ) {
    }

    public function create(RegisterDto $dto): User
    {
        return User::create($dto->toArray());
    }

    public function changePassword(User $user, string $password): bool
    {
        return $user->fill(['password' => Hash::make($password)])->save();
    }

    public function findUserByEmail(string $email): ?User
    {
        return User::whereEmail($email)->first();
    }

    public function changeEmail(User $user, string $email): ?bool
    {
        if ($user->email === $email) {
            return false;
        }

        throw_if($this->findUserByEmail($email), new UserWithEmailAlreadyExistsException());

        return $user->fill(['email' => $email, 'email_verified_at' => null])->save();
    }

    public function changeName(User $user, string $name): bool
    {
        if ($user->name === $name) {
            return false;
        }

        return $user->update(['name' => $name]);
    }

    public function changePhone(User $user, string $phone): bool
    {
        throw_if($user->phone === $phone, new RuntimeException(trans('user::user.samePhone')));
        throw_if(! phone($phone, ['RU', 'UA', 'KZ'])->isValid(), new RuntimeException(trans('user::user.localPhone')));
        throw_if(User::where('phone', '=', $phone)->exists(), new RuntimeException(trans('user::user.repeatedNumber')));

        return $user->update(['phone' => $phone]);
    }

    public function changeImage(User $user, UploadedFile $uploadedFile, User $userChange): bool
    {
        $this->deleteImage($user);

        $file = $this->fileService->createFromUploadedFile($uploadedFile, $userChange);

        return $user->update(['image_id' => $file->id]);
    }

    private function deleteImage(User $user): void
    {
        // If exists
        if ($user->image_id) {
            $this->fileService->delete($user->imageFile);
        }
    }

    public function getUserById(int $userId): User
    {
        return User::findOrFail($userId);
    }

    public function findUserById(int $userId): ?User
    {
        return User::find($userId);
    }

    public function getUserByEmail(string $email): User
    {
        return User::whereEmail($email)->firstOrFail();
    }

    public function getAllByFilters(UserFiltersDto $dto): Collection|LengthAwarePaginator
    {
        $data = User::active()
            ->when($dto->search && strlen($dto->search) > 1, function (Builder $builder) use ($dto) {
                $builder->where(fn (Builder $query) => $query
                    ->orWhere('name', 'ilike', '%'.$dto->search.'%')
                    ->orWhere('email', 'ilike', '%'.$dto->search.'%')
                    ->orWhere('phone', 'ilike', '%'.$dto->search.'%')
                );
            });

        return $dto->page ? $data->paginate(
            perPage: $dto->perPage,
            page: $dto->page
        ) : $data->get();
    }
}
