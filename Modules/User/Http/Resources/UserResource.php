<?php

namespace Modules\User\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\User\Entities\User;

/** @mixin User */
class UserResource extends JsonResource
{
    private string $_token = '';

    /**
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'created_at' => Carbon::parse($this->created_at)->format('d.m.Y H:i:s'),
            'updated_at' => Carbon::parse($this->updated_at)->format('d.m.Y H:i:s'),
            'token' => $this->when((bool) $this->_token, $this->_token),
            'image' => $this->image,
        ];
    }

    public function setToken(string $token): self
    {
        $this->_token = $token;

        return $this;
    }
}
