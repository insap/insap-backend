<?php

declare(strict_types=1);

namespace Modules\User\DTO;

use Illuminate\Http\Request;

readonly class UserFiltersDto
{
    public function __construct(
        public ?string $search = null,
        public ?int $page = null,
        public int $perPage = 15
    ) {
    }

    public static function fromRequest(Request $request): self
    {
        return new self(
            $request->input('search'),
            $request->input('page'),
        );
    }
}
