<?php

declare(strict_types=1);

namespace Modules\File\Entities\DTO;

use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Modules\Record\Models\Record;
use MongoDB\Model\BSONDocument;

class MongoFileDto implements Arrayable
{
    public string $oid;

    public Carbon $createdAt;

    public string $uuid;

    public int $downloads;

    public int $recordId;

    public int $recordImportId;

    /**
     * @var string Filename with extension
     */
    public string $fileNameWithExtension;

    /**
     * @var string Название файла в монге
     */
    public string $filenameMongo;

    public string $extension;

    public string $alias;

    public string $contentType;

    public static function createFromBSON(BSONDocument $document): self
    {
        return (new self())
            ->setOid((string) $document['_id'])
            ->setFilenameMongo($document['filename'])
            ->setContentType($document['contentType'])
            ->setCreatedAt(Carbon::createFromTimestampMs($document['metadata']['created_at']))
            ->setUuid($document['metadata']['uuid'])
            ->setDownloads($document['metadata']['downloads'])
            ->setRecordId($document['metadata'][Record::RECORD_ID_FIELD])
            ->setRecordImportId($document['metadata'][Record::RECORD_IMPORT_ID_FIELD])
            ->setFileNameWithExtension($document['metadata']['filename'])
            ->setExtension($document['metadata']['extension'])
            ->setAlias($document['metadata']['type']);
    }

    public function toArray(): array
    {
        return [
            'oid' => $this->getOid(),
            'type' => $this->getAlias(),
            'alias' => $this->getAlias(),
            'filename' => $this->getFileNameWithExtension(),
            'uuid' => $this->getUUID(),
            'content_type' => $this->getContentType(),
            'created_at' => $this->getCreatedAt(),
            'downloads' => $this->getDownloads(),
            Record::RECORD_ID_FIELD => $this->getRecordId(),
            Record::RECORD_IMPORT_ID_FIELD => $this->getRecordImportId(),

            'extension' => $this->getExtension(),
        ];
    }

    public function getOid(): string
    {
        return $this->oid;
    }

    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getDownloads(): int
    {
        return $this->downloads;
    }

    public function getRecordId(): int
    {
        return $this->recordId;
    }

    public function getFileNameWithExtension(): string
    {
        return $this->fileNameWithExtension;
    }

    public function getFilenameMongo(): string
    {
        return $this->filenameMongo;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }

    public function setOid(string $oid): MongoFileDto
    {
        $this->oid = $oid;

        return $this;
    }

    public function setCreatedAt(Carbon $createdAt): MongoFileDto
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setUuid(string $uuid): MongoFileDto
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function setDownloads(int $downloads): MongoFileDto
    {
        $this->downloads = $downloads;

        return $this;
    }

    public function setRecordId(int $recordId): MongoFileDto
    {
        $this->recordId = $recordId;

        return $this;
    }

    public function setFileNameWithExtension(string $fileNameWithExtension): MongoFileDto
    {
        $this->fileNameWithExtension = $fileNameWithExtension;

        return $this;
    }

    public function setFilenameMongo(string $filenameMongo): MongoFileDto
    {
        $this->filenameMongo = $filenameMongo;

        return $this;
    }

    public function setExtension(string $extension): MongoFileDto
    {
        $this->extension = $extension;

        return $this;
    }

    public function setAlias(string $alias): MongoFileDto
    {
        $this->alias = $alias;

        return $this;
    }

    public function setContentType(string $contentType): MongoFileDto
    {
        $this->contentType = $contentType;

        return $this;
    }

    public function setRecordImportId(int $recordImportId): MongoFileDto
    {
        $this->recordImportId = $recordImportId;

        return $this;
    }

    public function getRecordImportId(): int
    {
        return $this->recordImportId;
    }
}
