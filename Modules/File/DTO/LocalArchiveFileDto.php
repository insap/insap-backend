<?php

declare(strict_types=1);

namespace Modules\File\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Process\DTO\ProcessFileDto;

class LocalArchiveFileDto implements Arrayable
{
    public function __construct(
        public string $fullPath,
        public string $fileName,
        public string $alias
    ) {
    }

    public function toArray(): array
    {
        return [
            'fullPath' => $this->fullPath,
            'fileName' => $this->fileName,
            'alias' => $this->alias,
        ];
    }

    public function getUrl(): string
    {
        return \Storage::disk('minio')->url($this->fullPath);
    }

    public static function fromProcessFile(string $path, ProcessFileDto $file): self
    {
        return new self(
            fullPath: $path,
            fileName: $file->getUploadedFile()->getClientOriginalName(),
            alias: $file->getAlias(),
        );
    }

    public static function fromArray(array $array): self
    {
        return new self(
            fullPath: $array['fullPath'],
            fileName: $array['fileName'],
            alias: $array['alias'],
        );
    }
}
