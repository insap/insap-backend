<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table(\Modules\File\Entities\File::TABLE, function (Blueprint $table) {
            $table->text('path')->nullable()->change();
            $table->text('url')->nullable()->change();
            $table->text('name')->nullable()->change();
        });
    }

    public function down(): void
    {
        Schema::table(\Modules\File\Entities\File::TABLE, function (Blueprint $table) {
            $table->string('path')->nullable()->change();
            $table->string('url')->nullable()->change();
            $table->string('name')->nullable()->change();
        });
    }
};
