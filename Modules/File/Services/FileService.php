<?php

declare(strict_types=1);

namespace Modules\File\Services;

use App\Enums\ActiveStatusEnum;
use Illuminate\Http\UploadedFile;
use Modules\File\Entities\File;
use Modules\User\Entities\User;
use Ramsey\Uuid\Uuid;
use Storage;

class FileService implements FileServiceInterface
{
    public function delete(File $file): ?bool
    {
        if (! \Storage::disk('minio')->delete($file->path)) {
            throw new \RuntimeException("Can't delete file: ".$file->path);
        }

        return $file->delete();
    }

    public function createFromUploadedFile(UploadedFile $file, ?User $user = null): File
    {
        $storage = Storage::disk('minio');

        $name = $file->getClientOriginalName();
        $path = (string) Uuid::uuid4().DIRECTORY_SEPARATOR.$file->getClientOriginalName();
        $mime = $file->getMimeType();
        $hashName = $file->hashName();
        $size = $file->getSize();

        $storage->put($path, $file->getContent());

        return File::create([
            'name' => $name,
            'path' => $path,
            'size' => $size,
            'url' => route('files.download.s3-file', ['file' => $path]),
            'mime' => $mime,
            'is_active' => ActiveStatusEnum::Active->value,
            'user_id' => optional($user)->id,
        ]);
    }

    public function exists(string $path): bool
    {
        return Storage::disk('minio')->exists($path);
    }

    public function getById(int $id): File
    {
        return File::findOrFail($id);
    }
}
