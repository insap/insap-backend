<?php

declare(strict_types=1);

namespace Modules\File\Services\Minio;

use Modules\File\DTO\LocalArchiveFileDto;

class MinioService
{
    public static function delete(array $files): void
    {
        collect($files)
            ->filter(fn (array $array) => (bool) count($array))
            ->map(fn (array $array) => LocalArchiveFileDto::fromArray($array))
            ->each(fn (LocalArchiveFileDto $fileDto) => \Storage::disk('minio')->delete($fileDto->fullPath));
    }

    public static function toJson(array $files): array
    {
        return collect($files)
            ->filter()
            ->filter(fn (array $array) => (bool) count($array))
            ->map(fn (array $fileArray) => LocalArchiveFileDto::fromArray($fileArray))
            ->map(fn (LocalArchiveFileDto $file) => array_merge($file->toArray(), [
                'url' => route('files.download.s3-file', ['file' => $file->fullPath]),
            ]))->all();
    }
}
