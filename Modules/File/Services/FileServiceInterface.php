<?php

declare(strict_types=1);

namespace Modules\File\Services;

use Illuminate\Http\UploadedFile;
use Modules\File\Entities\File;
use Modules\User\Entities\User;

interface FileServiceInterface
{
    public function createFromUploadedFile(UploadedFile $file, ?User $user = null): File;

    public function getById(int $id): File;

    public function delete(File $file): ?bool;

    public function exists(string $path): bool;
}
