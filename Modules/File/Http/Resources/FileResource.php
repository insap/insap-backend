<?php

namespace Modules\File\Http\Resources;

use App\Helpers\BytesForHuman;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\User\Http\Resources\UserResource;

/** @mixin \Modules\File\Entities\File */
class FileResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'path' => $this->path,
            'url' => $this->url,
            'size' => $this->size,

            'human_size' => $this->size ? BytesForHuman::format($this->size, 2) : null,
            'name' => $this->name,
            'mime' => $this->mime,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'user_id' => $this->user_id,

            'user' => new UserResource($this->whenLoaded('user')),
        ];
    }
}
