<?php

namespace Modules\File\Http\Controllers;

use App\Http\Controllers\Controller;

class FileController extends Controller
{
    public function download(string $file)
    {
        return \Storage::disk('minio')->download($file);
    }
}
