<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\File\Http\Controllers\FileController;

Route::prefix('files')->middleware('auth:sanctum')->name('files.')->group(function () {
    Route::get('download/s3/{file}', [FileController::class, 'download'])
        ->where('file', '^(?!api|web).*$')->name('download.s3-file');
});
