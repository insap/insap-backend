<?php

declare(strict_types=1);

namespace Modules\Tags\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Modules\Tags\Enums\TagEntityEnum;
use Modules\Tags\Models\Tag;

interface TagServiceInterface
{
    public function sync(EloquentCollection $existingTags, array $tags, MorphToMany $relation): void;

    /**
     * @return EloquentCollection<int, Tag>
     */
    public function getTagsByEntity(TagEntityEnum $entity): Collection;
}
