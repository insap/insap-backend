<?php

declare(strict_types=1);

namespace Modules\Tags\Services;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Modules\Tags\Enums\TagEntityEnum;
use Modules\Tags\Models\Tag;

class TagService implements TagServiceInterface
{
    public function sync(EloquentCollection $existingTags, array $tags, MorphToMany $relation): void
    {
        $tagsToAdd = collect($tags)->diff($existingTags->pluck('name'));
        $tagsToRemove = $existingTags->pluck('name')->diff($tags);

        $tagsToAdd->each(fn (string $tag) => $relation->create([
            'name' => $tag,
            'is_filter' => false,
        ]));

        // @phpstan-ignore-next-line
        $existingTags->whereIn('name', $tagsToRemove)->each(fn (Tag $tag) => $tag->delete());
    }

    public function getTagsByEntity(TagEntityEnum $entity): EloquentCollection
    {
        $tags = Tag::query();

        return match ($entity) {
            TagEntityEnum::Process => $tags->whereHas('processes')->get(),
            TagEntityEnum::Journal => $tags->whereHas('journals')->get()
        };
    }
}
