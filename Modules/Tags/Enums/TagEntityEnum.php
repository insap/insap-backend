<?php

declare(strict_types=1);

namespace Modules\Tags\Enums;

use Modules\Journal\Models\Journal;
use Modules\Process\Models\Process;

enum TagEntityEnum: string
{
    case Process = Process::class;
    case Journal = Journal::class;

    public static function labels(): array
    {
        return [
            'process' => self::Process->value,
            'journal' => self::Journal->value,
        ];
    }
}
