<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Tags\Models\Tag;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Tag::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('is_filter')->default(false);

            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Tag::TABLE);
    }
};
