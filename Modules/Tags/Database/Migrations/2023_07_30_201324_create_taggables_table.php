<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Tags\Models\Tag;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Tag::TAGGABLE_TABLE, function (Blueprint $table) {
            $table->foreignId('tag_id')->index()->references('id')->on(Tag::TABLE)->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->morphs('taggable');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Tag::TAGGABLE_TABLE);
    }
};
