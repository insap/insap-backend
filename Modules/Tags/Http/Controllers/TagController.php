<?php

namespace Modules\Tags\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Tags\Enums\TagEntityEnum;
use Modules\Tags\Http\Requests\GetTagsRequest;
use Modules\Tags\Models\Tag;
use Modules\Tags\Services\TagServiceInterface;

class TagController extends Controller
{
    public function __construct(
        private readonly TagServiceInterface $tagService
    ) {
    }

    public function getTagsByEntity(GetTagsRequest $request): Response
    {
        $response = Response::make();

        $entity = TagEntityEnum::from(TagEntityEnum::labels()[$request->input('entity')]);
        $data = $this->tagService->getTagsByEntity($entity)->map(fn (Tag $tag) => [
            'text' => $tag->name,
            'value' => $tag->name,
        ])->unique('value');

        return $response->withData($data);
    }
}
