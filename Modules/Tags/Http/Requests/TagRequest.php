<?php

namespace Modules\Tags\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'is_filter' => ['required'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
