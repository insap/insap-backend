<?php

namespace Modules\Tags\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\In;
use Modules\Tags\Enums\TagEntityEnum;

class GetTagsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'entity' => ['required', new In(array_keys(TagEntityEnum::labels()))],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
