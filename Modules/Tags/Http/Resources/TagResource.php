<?php

namespace Modules\Tags\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \Modules\Tags\Models\Tag */
class TagResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'is_filter' => $this->is_filter,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
