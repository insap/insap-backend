<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Tags\Http\Controllers\TagController;

Route::prefix('tags')->middleware('auth:sanctum')->name('tags.')->group(function () {
    Route::post('get-by-entity', [TagController::class, 'getTagsByEntity'])->name('get-by-entity');
});
