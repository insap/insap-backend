<?php

namespace Modules\Tags\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Modules\Journal\Models\Journal;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Modules\Tags\Models\Tag
 *
 * @property int $id
 * @property string $name
 * @property bool $is_filter
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Journal> $journals
 * @property-read int|null $journals_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Record> $records
 * @property-read int|null $records_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereIsFilter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereUpdatedAt($value)
 *
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Process> $processes
 * @property-read int|null $processes_count
 *
 * @mixin \Eloquent
 */
class Tag extends Model
{
    use HasFactory, HasSlug;

    public const TABLE = 'tags';

    public const TAGGABLE_TABLE = 'taggables';

    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'is_filter',
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->slugsShouldBeNoLongerThan(config('app.max_slug_length'))
            ->doNotGenerateSlugsOnUpdate()
            ->generateSlugsFrom(['name'])
            ->saveSlugsTo('slug');
    }

    public function journals(): MorphToMany
    {
        return $this->morphedByMany(Journal::class, 'taggable');
    }

    public function processes(): MorphToMany
    {
        return $this->morphedByMany(Process::class, 'taggable');
    }

    public function records(): MorphToMany
    {
        return $this->morphedByMany(Record::class, 'taggable');
    }
}
