<?php

use Modules\Authorization\Http\Controllers\OAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('oauth')->name('oauth.')->group(function () {
    Route::middleware('web')->any('authorize', [OAuthController::class, 'login'])->name('login');
    Route::middleware('web')->get('logout', [OAuthController::class, 'logout'])->name('logout');

    Route::middleware('client_auth')->group(function () {
        Route::post('me', [OAuthController::class, 'me'])->name('me');

    });
});
