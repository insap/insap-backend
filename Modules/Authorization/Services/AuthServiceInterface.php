<?php

declare(strict_types=1);

namespace Modules\Authorization\Services;

use Laravel\Socialite\Two\User as SocialiteUser;
use Modules\User\Entities\User;
use Modules\User\Enums\AuthProviderEnum;

interface AuthServiceInterface
{
    public function auth(string $identifier, string $password): ?User;

    public function logout(): void;

    public function socialAuth(SocialiteUser $socialiteUser, AuthProviderEnum $provider): User;
}
