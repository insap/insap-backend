<?php

declare(strict_types=1);

namespace Modules\Authorization\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class AppLoginRequest extends CustomFormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string',
        ];
    }

    public function getCredentials(): array
    {
        return $this->only('email', 'password');
    }
}
