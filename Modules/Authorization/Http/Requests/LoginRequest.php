<?php

namespace Modules\Authorization\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class LoginRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|email|exists:users',
            'password' => 'required|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
