<?php

declare(strict_types=1);

namespace Modules\Authorization\Http\Requests;

use App\Http\Requests\GetFormRequest;

class LoginApiRequest extends GetFormRequest
{
    public function rules(): array
    {
        return [
            'client_id' => 'required|exists:oauth_clients,id',
            'redirect_uri' => 'required|string|url',
            'response_type' => 'required|string',

            'email' => 'nullable|email',
            'password' => 'required_with:email|string',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
