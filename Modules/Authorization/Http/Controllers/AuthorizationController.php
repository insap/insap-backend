<?php

namespace Modules\Authorization\Http\Controllers;

use App\Enums\ActiveStatusEnum;
use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Carbon\Carbon;
use Crypt;
use Exception;
use Modules\Authorization\Http\Requests\ChangeCredentialsRequest;
use Modules\Authorization\Http\Requests\ChangePasswordRequest;
use Modules\Authorization\Http\Requests\LoginRequest;
use Modules\Authorization\Http\Requests\RegisterRequest;
use Modules\Authorization\Http\Requests\ResetPasswordRequest;
use Modules\Authorization\Services\AuthServiceInterface;
use Modules\User\Entities\DTO\RegisterDto;
use Modules\User\Http\Resources\UserResource;
use Modules\User\Services\UserEmailService;
use Modules\User\Services\UserServiceInterface;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Throwable;

class AuthorizationController extends Controller
{
    public function __construct(
        private readonly UserServiceInterface $userService,
        private readonly UserEmailService $emailService,
        private readonly AuthServiceInterface $authService
    ) {
    }

    public function register(RegisterRequest $request): Response
    {
        $response = Response::make();

        try {
            $user = $this->userService->create(new RegisterDto(
                name: $request->input('name'),
                email: $request->input('email'),
                password: $request->input('password'),
                phone: $request->input('phone'),
            ));

            $this->emailService->sendActivationEmail($user, Crypt::encryptString($user->email));
        } catch (Exception $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function login(LoginRequest $request): Response
    {
        $response = Response::make();

        $user = $this->userService->getUserByEmail($request->input('email'));

        if ($user->is_active !== ActiveStatusEnum::Active) {
            return $response->withError(SymfonyResponse::HTTP_BAD_REQUEST, trans('authorization::authorization.active'));
        }

        if (! auth()->attempt($request->only('email', 'password'))) {
            return $response->withError(SymfonyResponse::HTTP_BAD_REQUEST, trans('authorization::authorization.failed'));
        }

        $user = $request->user();
        $token = $user->createToken('web');

        return $response->withData(UserResource::make($user)->setToken($token->accessToken));
    }

    public function logout(): Response
    {
        $response = Response::make();

        $this->authService->logout();

        return $response->success();
    }

    public function verifyAccount(string $token)
    {
        $email = Crypt::decryptString($token);
        $user = $this->userService->getUserByEmail($email);

        if ($user->email_verified_at) {
            abort(SymfonyResponse::HTTP_BAD_REQUEST);
        }

        $user->fill([
            'is_active' => ActiveStatusEnum::Active,
            'email_verified_at' => Carbon::now(),
        ])->save();

        return view('authorization::verify_acc');
    }

    public function me(): Response
    {
        return Response::make()->withData(request()->user() ? UserResource::make(request()->user()) : []);
    }

    public function changeCredentials(ChangeCredentialsRequest $request): Response
    {
        $response = Response::make();
        $user = $request->user();

        try {

            if ($this->userService->changeEmail($user, $request->input('email'))) {
                $this->emailService->sendActivationEmail($user, Crypt::encryptString($user->email));
            }

            $this->userService->changeName($user, $request->input('name'));
        } catch (Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function changePassword(ChangePasswordRequest $request): Response
    {
        $response = Response::make();
        $this->userService->changePassword($request->user(), $request->input('new_password'));

        return $response->success();
    }

    public function resetPassword(ResetPasswordRequest $request): Response
    {
        $response = Response::make();

        try {
            $this->emailService->sendResetPasswordEmail(
                user: $this->userService->getUserByEmail($request->input('email')),
                token: Crypt::encryptString($request->input('email')));
        } catch (Throwable $ex) {
            return $response->catch($ex);
        }

        return $response->success();
    }

    public function resetPasswordBladeView(string $token)
    {
        $email = Crypt::decryptString($token);
        $user = $this->userService->getUserByEmail($email);

        if (request()->post()) {
            $this->validate(request(), [
                'password' => get_password_rules(),
            ]);

            $this->userService->changePassword($user, request()->post('password'));
            $this->emailService->sendNewPasswordMail($user);

            return view('authorization::reset_password_success');
        }

        return view('authorization::reset_password', compact('token'));
    }

    public function reSendActivationEmail(ResetPasswordRequest $request): Response
    {
        $response = Response::make();
        $user = $this->userService->getUserByEmail($request->input('email'));

        if ($user->email_verified_at === null) {
            $this->emailService->sendActivationEmail($user, Crypt::encryptString($user->email));
        }

        return $response->success();
    }
}
