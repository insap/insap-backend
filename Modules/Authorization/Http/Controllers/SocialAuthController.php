<?php

namespace Modules\Authorization\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Illuminate\Http\RedirectResponse;
use Modules\Authorization\Services\AuthServiceInterface;
use Modules\User\Enums\AuthProviderEnum;
use Socialite;
use Str;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class SocialAuthController extends Controller
{
    public function __construct(
        private readonly AuthServiceInterface $authService
    ) {
    }

    public function providers(): Response
    {
        return Response::make()->withData(AuthProviderEnum::labels());
    }

    public function redirect(string $provider)
    {
        return \Socialite::driver(AuthProviderEnum::from($provider)->value)->redirect();
    }

    public function callback(string $provider): RedirectResponse
    {
        $authProvider = AuthProviderEnum::from($provider);

        /** @var \Laravel\Socialite\Two\User $authUser */
        $authUser = Socialite::driver($authProvider->value)->user();

        if (! ($authUser instanceof \Laravel\Socialite\Two\User)) {
            abort(SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR, trans('authorization::authorization.oauth_v2'));
        }

        if (Str::of($authUser->getEmail())->isEmpty()) {
            abort(SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY, trans('authorization::authorization.oauth_empty_email'));
        }

        $token = $this->authService->socialAuth($authUser, $authProvider)->createToken('web');

        return redirect()->to('app/auth/signin?'.http_build_query(['redirect' => '/home', 'social' => $token->accessToken]));
    }
}
