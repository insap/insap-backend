<?php

namespace Modules\Authorization\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Authorization\Http\Requests\LoginApiRequest;
use Modules\Authorization\Services\AuthServiceInterface;
use Modules\User\Http\Resources\ExternalUserResource;
use Str;

class OAuthController extends Controller
{
    public function __construct(
        private readonly AuthServiceInterface $authService
    ) {
    }

    public function login(LoginApiRequest $request)
    {
        if ($request->post()) {
            $credentials = $request->only(['email', 'password']);
            $this->authService->auth($credentials['email'], $credentials['password']);
        }

        if (! auth()->check()) {
            return view('authorization::login');
        }

        $request->session()->put('state', $state = Str::random(40));

        $params = [
            'client_id' => $request->input('client_id'),
            'redirect_uri' => $request->input('redirect_uri'),
            'response_type' => $request->input('response_type'),
            'scope' => '',
            'state' => $state,
            'prompt' => 'consent', // "none", "consent", or "login"
        ];

        return redirect()->route('passport.authorizations.authorize', $params);
    }

    public function me(): Response
    {
        $response = Response::make();

        $data = ExternalUserResource::make(request()->user());

        return $response->withData($data);
    }

    public function logout(): \Illuminate\Http\RedirectResponse
    {
        $this->authService->logout();

        return redirect()->back();
    }
}
