<?php

namespace Modules\Statistics\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Statistics\Services\StatisticsServiceInterface;

class StatisticsController extends Controller
{
    public function __construct(private readonly StatisticsServiceInterface $statisticsService)
    {
    }

    public function getDashboardStats(): Response
    {
        $response = Response::make();

        $data = $this->statisticsService->getTotalStats();

        return $response->withData($data);
    }
}
