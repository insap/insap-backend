<?php

namespace Modules\Statistics\Services;

use Illuminate\Support\Collection;

interface StatisticsServiceInterface
{
    public function getTotalStats(): Collection;
}
