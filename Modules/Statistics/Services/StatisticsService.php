<?php

namespace Modules\Statistics\Services;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Modules\Journal\DTO\JournalFilterDto;
use Modules\Journal\Services\JournalServiceInterface;
use Modules\Process\DTO\ProcessFiltersDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Enums\ProcessUseTypeEnum;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\Record\DTO\RecordFilterDto;
use Modules\Record\Services\RecordServiceInterface;
use Modules\User\DTO\UserFiltersDto;
use Modules\User\Http\Resources\UserResource;
use Modules\User\Services\UserServiceInterface;

readonly class StatisticsService implements StatisticsServiceInterface
{
    public function __construct(
        private UserServiceInterface $userService,
        private ProcessServiceInterface $processService,
        private JournalServiceInterface $journalService,
        private RecordServiceInterface $recordService,
    ) {
    }

    public function getTotalStats(): Collection
    {
        $from = Carbon::now()->startOfYear();
        $to = $from->copy()->endOfYear();

        $users = $this->userService->getAllByFilters(new UserFiltersDto());
        $processes = $this->processService->getAllByFilters(new ProcessFiltersDto());
        $journals = $this->journalService->getAllByFilters(new JournalFilterDto());
        $records = $this->recordService->getAllByFilters(new RecordFilterDto());

        return collect([
            'users' => UserResource::collection($users),
            'processes' => [
                'type' => collect(ProcessTypeEnum::cases())
                    ->mapWithKeys(fn (ProcessTypeEnum $typeEnum) => [
                        ProcessTypeEnum::labels()[$typeEnum->value] => $processes
                            ->where('type', $typeEnum)->count(),
                    ]),
                'interpreters' => collect(ProcessInterpreterEnum::cases())
                    ->mapWithKeys(fn (ProcessInterpreterEnum $interpreterEnum) => [
                        ProcessInterpreterEnum::labels()[$interpreterEnum->value] => $processes
                            ->where('interpreter', $interpreterEnum)->count(),
                    ]),
                'use_type' => collect(ProcessUseTypeEnum::cases())
                    ->mapWithKeys(fn (ProcessUseTypeEnum $useTypeEnum) => [
                        ProcessUseTypeEnum::labels()[$useTypeEnum->value] => $processes
                            ->where('use_type', $useTypeEnum)->count(),
                    ]),
            ],
            'journals' => [
                'total' => $journals->count(),
                'new_this_year' => $journals
                    ->where('date_start', '>=', $from)
                    ->where('date_start', '<=', $to)
                    ->count(),
            ],
            'records' => [
                'total' => $records->count(),
            ],
        ]);
    }
}
