<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Statistics\Http\Controllers\StatisticsController;

Route::prefix('statistics')->middleware('auth:sanctum')->name('statistics.')->group(function () {
    Route::post('dashboard-stats', [StatisticsController::class, 'getDashboardStats']);
});
