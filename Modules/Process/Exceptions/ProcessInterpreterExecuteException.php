<?php

declare(strict_types=1);

namespace Modules\Process\Exceptions;

use Illuminate\Contracts\Process\ProcessResult;
use Throwable;

class ProcessInterpreterExecuteException extends \Exception
{
    public ProcessResult $processResult;

    public function __construct(ProcessResult $processResult, string $message = '', int $code = 0, ?Throwable $previous = null)
    {
        $this->processResult = $processResult;
        $finalMessage = $message.' Output: '.$this->processResult->errorOutput().' - '.$this->processResult->output();

        parent::__construct($finalMessage, $code, $previous);
    }
}
