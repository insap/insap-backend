<?php

namespace Modules\Process\Http\Controllers;

use App\DTO\CollectionWithPaginationDto;
use App\DTO\PaginationDto;
use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\DTO\ProcessFiltersDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Enums\ProcessUseTypeEnum;
use Modules\Process\Http\Requests\ProcessAllRequest;
use Modules\Process\Http\Requests\ProcessCreateRequest;
use Modules\Process\Http\Requests\ProcessIDRequest;
use Modules\Process\Http\Requests\ProcessUpdateArchiveRequest;
use Modules\Process\Http\Requests\ProcessUpdateRequest;
use Modules\Process\Http\Resources\ProcessResource;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessApp\ProcessAppServiceInterface;
use Modules\Process\Services\ProcessCompatibleServiceInterface;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;

class ProcessController extends Controller
{
    public function __construct(
        private readonly ProcessServiceInterface $processService,
        private readonly PluginServiceInterface $pluginService,
        private readonly ProcessCompatibleServiceInterface $processCompatibleService,
        private readonly ProcessFieldServiceInterface $processFieldService,
        private readonly ProcessAppServiceInterface $processAppService
    ) {
    }

    public function getInterpretersList(): Response
    {
        $response = Response::make();

        return $response->withData(ProcessInterpreterEnum::labelsArray());
    }

    public function getUseTypesList(): Response
    {
        $response = Response::make();

        $data = collect(ProcessUseTypeEnum::labelsArray())->map(function (array $type) {
            return [
                'text' => trans('process::process.use_type.'.$type['text']),
                'value' => $type['value'],
            ];
        });

        return $response->withData($data);
    }

    public function getTypesList(): Response
    {
        $response = Response::make();

        $data = collect(ProcessTypeEnum::labelsArray())->map(function (array $type) {
            return [
                'text' => trans('process::process.'.$type['text']),
                'value' => $type['value'],
            ];
        });

        return $response->withData($data);
    }

    public function createProcess(ProcessCreateRequest $request): Response
    {
        $response = Response::make();

        try {
            $dto = ProcessDto::createFromRequest($request);
            $data = $this->processService->createWithApp($dto, $request->file('archive'));
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->withData($data);
    }

    public function all(): Response
    {
        $response = Response::make();

        $data = $this->processService->getAll();
        $data->load(['user', 'appliance', 'fields', 'plugin', 'archiveFile', 'tags', 'features']);

        return $response->withData(ProcessResource::collection($data));
    }

    public function getByFiltersPaginated(ProcessAllRequest $request): Response
    {
        $response = Response::make();

        $data = $this->processService->getAllByFilters(ProcessFiltersDto::fromRequest($request));
        $data->load(['user', 'appliance', 'fields', 'plugin', 'archiveFile', 'tags']);

        $result = new CollectionWithPaginationDto(ProcessResource::collection($data),
            $data instanceof LengthAwarePaginator ? new PaginationDto(
                total: $data->total(),
                page: $data->currentPage(),
                lastPage: $data->lastPage(),
                perPage: $data->perPage()
            ) : null
        );

        return $response->withData($result);
    }

    public function update(ProcessUpdateRequest $request): Response
    {
        $response = Response::make();

        $process = Process::find($request->input('process_id'));
        $plugin = $request->input('plugin_id') ? $this->pluginService->findById($request->input('plugin_id')) : null;

        try {
            if ($plugin) {
                $this->processService->updatePlugin($process, $plugin);
            }

            if ($request->input('name')) {
                $this->processService->updateName($process, $request->input('name'));
            }

            if ($request->input('description')) {
                $this->processService->updateDescription($process, $request->input('description'));
            }

            if ($request->input('compatible_processes')) {
                $this->processCompatibleService->sync($process, $request->input('compatible_processes'));
            }

            if ($request->input('use_type')) {
                $this->processService->updateUseType($process, ProcessUseTypeEnum::from($request->input('use_type')));
            }

            if ($request->input('tags')) {
                $this->processService->updateTags($process, $request->input('tags'));
            }

        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function updateApp(ProcessUpdateArchiveRequest $request): Response
    {
        $response = Response::make();

        $process = $this->processService->getById($request->input('process_id'));
        $archive = $request->file('archive');

        try {
            $this->processAppService->updateApp($process, $archive);
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function getFieldsByProcess(ProcessIDRequest $request): Response
    {
        $response = Response::make();

        $process = Process::find($request->input('process_id'));
        $data = $this->processFieldService->getByProcess($process);

        return $response->withData($data);
    }
}
