<?php

namespace Modules\Process\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Process\Http\Requests\ProcessFieldTypeRequest;
use Modules\Process\Http\Resources\ProcessFieldTypeResource;
use Modules\Process\Models\ProcessFieldType;

class ProcessFieldTypeController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', ProcessFieldType::class);

        return ProcessFieldTypeResource::collection(ProcessFieldType::all());
    }

    public function store(ProcessFieldTypeRequest $request)
    {
        $this->authorize('create', ProcessFieldType::class);

        return new ProcessFieldTypeResource(ProcessFieldType::create($request->validated()));
    }

    public function show(ProcessFieldType $processFieldType)
    {
        $this->authorize('view', $processFieldType);

        return new ProcessFieldTypeResource($processFieldType);
    }

    public function update(ProcessFieldTypeRequest $request, ProcessFieldType $processFieldType)
    {
        $this->authorize('update', $processFieldType);

        $processFieldType->update($request->validated());

        return new ProcessFieldTypeResource($processFieldType);
    }

    public function destroy(ProcessFieldType $processFieldType)
    {
        $this->authorize('delete', $processFieldType);

        $processFieldType->delete();

        return response()->json();
    }
}
