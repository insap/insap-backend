<?php

namespace Modules\Process\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Process\Http\Requests\ProcessIDRequest;
use Modules\Process\Http\Resources\ProcessResource;
use Modules\Process\Services\ProcessCompatibleServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;

class ProcessCompatibleController extends Controller
{
    public function __construct(
        private readonly ProcessCompatibleServiceInterface $processCompatibleService,
        private readonly ProcessServiceInterface $processService
    ) {
    }

    public function getAllCompatibleByExporter(ProcessIDRequest $request): Response
    {
        $response = Response::make();

        $process = $this->processService->getById($request->input('process_id'));
        $data = $this->processCompatibleService->getAllCompatibleByExporter($process);

        return $response->withData(ProcessResource::collection($data));
    }

    public function getAllExportersByImporter(ProcessIDRequest $request): Response
    {
        $response = Response::make();

        $process = $this->processService->getById($request->input('process_id'));
        $data = $this->processCompatibleService->getAllExportersByImporter($process);

        return $response->withData(ProcessResource::collection($data));
    }
}
