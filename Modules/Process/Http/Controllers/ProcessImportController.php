<?php

namespace Modules\Process\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Process\Http\Requests\ProcessImportRequest;
use Modules\Process\Http\Resources\ProcessImportResource;
use Modules\Record\Models\RecordImport;

class ProcessImportController extends Controller
{
    public function index()
    {
        return ProcessImportResource::collection(RecordImport::all());
    }

    public function store(ProcessImportRequest $request)
    {
        return new ProcessImportResource(RecordImport::create($request->validated()));
    }

    public function show(RecordImport $processImport)
    {
        return new ProcessImportResource($processImport);
    }

    public function update(ProcessImportRequest $request, RecordImport $processImport)
    {
        $processImport->update($request->validated());

        return new ProcessImportResource($processImport);
    }

    public function destroy(RecordImport $processImport)
    {
        $processImport->delete();

        return response()->json();
    }
}
