<?php

namespace Modules\Process\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \Modules\Process\Models\ProcessFieldType */
class ExternalFieldResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'field_type' => $this->field_type,
            'alias' => $this->alias,
            'title' => $this->title,
            'required' => $this->required,
            'description' => $this->description,
            'default_value' => $this->default_value,
            'select_options' => $this->select_options,
            'order' => $this->order,
        ];
    }
}
