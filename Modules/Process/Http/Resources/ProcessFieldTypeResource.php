<?php

namespace Modules\Process\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \Modules\Process\Models\ProcessFieldType */
class ProcessFieldTypeResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'field_type' => $this->field_type,
            'alias' => $this->alias,
            'title' => $this->title,
            'description' => $this->description,
            'default_value' => $this->default_value,
            'icon' => $this->icon,
            'order' => $this->order,
            'is_active' => $this->is_active,
            'process_id' => $this->process_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
