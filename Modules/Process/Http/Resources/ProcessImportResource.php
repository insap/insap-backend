<?php

namespace Modules\Process\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \Modules\Record\Models\RecordImport */
class ProcessImportResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'order' => $this->order,
            'files' => $this->files,
            'params' => $this->params,
            'status' => $this->status,
            'log' => $this->log,
            'user_id' => $this->user_id,
            'record_id' => $this->record_id,
            'process_id' => $this->process_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
