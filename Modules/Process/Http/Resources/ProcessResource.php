<?php

namespace Modules\Process\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Appliance\Http\Resources\ApplianceResource;
use Modules\Option\Http\Resources\OptionResource;
use Modules\Plugin\Http\Resources\PluginResource;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Tags\Http\Resources\TagResource;
use Modules\User\Http\Resources\UserResource;

/** @mixin \Modules\Process\Models\Process */
class ProcessResource extends JsonResource
{
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'image' => $this->image,

            'process_type' => ProcessTypeEnum::labels()[$this->type->value],
            'process_interpreter' => ProcessInterpreterEnum::labels()[$this->interpreter->value],

            'options' => $this->options,

            'tags' => TagResource::collection($this->whenLoaded('tags')),

            'plugin_id' => $this->plugin_id,
            'appliance_id' => $this->appliance_id,
            'process_interpreter_class' => $this->interpreter,
            'process_type_id' => $this->type,

            'appliance' => ApplianceResource::make($this->whenLoaded('appliance')),
            'user' => UserResource::make($this->whenLoaded('user')),
            'plugin' => PluginResource::make($this->whenLoaded('plugin')),
            'fields' => ProcessFieldTypeResource::collection($this->whenLoaded('fields')),
            'archive' => $this->whenLoaded('archiveFile'),

            'default_id' => $this->default_id,
            'compatible_processes' => $this->compatible_processes,

            'use_type' => $this->use_type,

            'features' => OptionResource::collection($this->whenLoaded('features')),

            'user_id' => $this->user_id,
            'image_id' => $this->image_id,
            'is_active' => $this->is_active,
            'archive_id' => $this->archive_id,

            'created_at' => Carbon::parse($this->created_at)->format('d.m.Y H:i:s'),
            'updated_at' => Carbon::parse($this->updated_at)->format('d.m.Y H:i:s'),
        ];
    }
}
