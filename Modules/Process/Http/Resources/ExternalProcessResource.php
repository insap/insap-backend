<?php

namespace Modules\Process\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Record\Models\Record;

/**
 * @property-read Record $extra_record
 *
 * @mixin \Modules\Process\Models\Process
 */
class ExternalProcessResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        $processFieldService = app(ProcessFieldServiceInterface::class);

        $calculated = $processFieldService->getCalculatedValues($this->extra_record, $this->resource);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'type' => $this->type,
            'image' => $this->image,

            'appliance' => $this->appliance->slug,
            'fields' => $calculated,
        ];
    }
}
