<?php

declare(strict_types=1);

namespace Modules\Process\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Illuminate\Validation\Rule;
use Modules\Process\Enums\ProcessUseTypeEnum;

class ProcessUpdateRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'process_id' => 'required|int|exists:processes,id',

            'tags' => 'nullable|array',

            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'plugin_id' => 'nullable|int',
            'compatible_processes' => 'nullable|array',
            'use_type' => [
                'sometimes',
                'string',
                Rule::in(collect(ProcessUseTypeEnum::cases())->map(fn (ProcessUseTypeEnum $enum) => $enum->value)),
            ],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
