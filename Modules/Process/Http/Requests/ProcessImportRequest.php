<?php

namespace Modules\Process\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class ProcessImportRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'order' => ['required', 'integer'],
            'files' => ['required', 'date'],
            'params' => ['required', 'date'],
            'status' => ['required', 'integer'],
            'log' => ['nullable'],
            'user_id' => ['nullable', 'integer'],
            'record_id' => ['required', 'integer'],
            'process_id' => ['required', 'integer'],
        ];
    }
}
