<?php

declare(strict_types=1);

namespace Modules\Process\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;

class ProcessRecordRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'process_id' => 'required|int|exists:'.Process::TABLE.',id',
            'record_id' => 'required|integer|exists:'.Record::TABLE.',id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
