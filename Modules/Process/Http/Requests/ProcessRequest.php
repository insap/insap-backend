<?php

namespace Modules\Process\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'description' => ['nullable'],
            'interpreter' => ['required'],
            'options' => ['nullable', 'date'],
            'appliance_id' => ['required', 'integer'],
            'plugin_id' => ['nullable', 'integer'],
            'user_id' => ['nullable', 'integer'],
            'image_id' => ['nullable', 'integer'],
            'is_active' => ['required', 'integer'],
            'acrhive_id' => ['nullable', 'integer'],
        ];
    }
}
