<?php

declare(strict_types=1);

namespace Modules\Process\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class ProcessUpdateArchiveRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'process_id' => 'required|int|exists:processes,id',
            'archive' => 'required|file',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
