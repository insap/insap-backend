<?php

declare(strict_types=1);

namespace Modules\Process\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Illuminate\Validation\Rule;
use Modules\Appliance\Models\Appliance;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;

class ProcessAllRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'appliances' => 'array|nullable',
            'appliances.*.id' => 'int|exists:'.Appliance::TABLE.', id',

            'tags' => 'array|nullable',

            'types' => 'array|nullable',
            'types.*.id' => ['int', Rule::in(collect(ProcessTypeEnum::cases())->map(fn (ProcessTypeEnum $enum) => $enum->value))],

            'langs' => 'array|nullable',
            'langs.*.id' => ['int', Rule::in(collect(ProcessInterpreterEnum::cases())->map(fn (ProcessInterpreterEnum $enum) => $enum->value))],

            'search' => 'string|nullable',

            'page' => 'int|nullable',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
