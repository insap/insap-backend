<?php

namespace Modules\Process\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class ProcessCreateRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'type' => 'required|int|min:0',
            'interpreter' => 'required|string',
            'archive' => 'required|file',

            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'options' => 'nullable|string',
            'appliance_id' => 'required|int|exists:appliances,id',
            'plugin_id' => 'nullable|int|',

        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
