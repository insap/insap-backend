<?php

namespace Modules\Process\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class ProcessFieldTypeRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'field_type' => ['required'],
            'alias' => ['required'],
            'title' => ['required'],
            'description' => ['nullable'],
            'default_value' => ['nullable'],
            'icon' => ['nullable'],
            'order' => ['required', 'integer'],
            'is_active' => ['required', 'integer'],
            'process_id' => ['required'],
        ];
    }
}
