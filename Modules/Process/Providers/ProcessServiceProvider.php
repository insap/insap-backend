<?php

namespace Modules\Process\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Process\Services\InterpreterService;
use Modules\Process\Services\InterpreterServiceInterface;
use Modules\Process\Services\ProcessApp\ProcessAppService;
use Modules\Process\Services\ProcessApp\ProcessAppServiceInterface;
use Modules\Process\Services\ProcessCompatibleService;
use Modules\Process\Services\ProcessCompatibleServiceInterface;
use Modules\Process\Services\ProcessExecuteService;
use Modules\Process\Services\ProcessExecuteServiceInterface;
use Modules\Process\Services\ProcessFieldService;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Process\Services\ProcessOptionService;
use Modules\Process\Services\ProcessOptionServiceInterface;
use Modules\Process\Services\ProcessService;
use Modules\Process\Services\ProcessServiceInterface;

class ProcessServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected $moduleName = 'Process';

    /**
     * @var string
     */
    protected $moduleNameLower = 'process';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->bind(InterpreterServiceInterface::class, InterpreterService::class);
        $this->app->bind(ProcessServiceInterface::class, ProcessService::class);
        $this->app->bind(ProcessOptionServiceInterface::class, ProcessOptionService::class);
        $this->app->bind(ProcessFieldServiceInterface::class, ProcessFieldService::class);
        $this->app->bind(ProcessExecuteServiceInterface::class, ProcessExecuteService::class);
        $this->app->bind(ProcessCompatibleServiceInterface::class, ProcessCompatibleService::class);
        $this->app->bind(ProcessAppServiceInterface::class, ProcessAppService::class);
    }

    /**
     * Register translations.
     */
    public function registerTranslations(): void
    {
        $langPath = resource_path('lang/modules/'.$this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    public function registerViews(): void
    {
        $viewPath = resource_path('views/modules/'.$this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath,
        ], ['views', $this->moduleNameLower.'-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Get the services provided by the provider.
     */
    public function provides(): array
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path.'/modules/'.$this->moduleNameLower)) {
                $paths[] = $path.'/modules/'.$this->moduleNameLower;
            }
        }

        return $paths;
    }
}
