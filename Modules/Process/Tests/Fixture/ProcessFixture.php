<?php

declare(strict_types=1);

namespace Modules\Process\Tests\Fixture;

use App\Enums\ActiveStatusEnum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\Models\Appliance;
use Modules\Plugin\Models\Plugin;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\DTO\ProcessFileDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessServiceInterface;
use Storage;

class ProcessFixture
{
    use WithFaker;

    public function __construct(
        private readonly ProcessServiceInterface $processService
    ) {
        $this->setUpFaker();
    }

    public function createWithInterpreter(Appliance $appliance, ProcessInterpreterEnum $interpreter, string $filename, ?Plugin $plugin = null): Process
    {
        $storage = Storage::disk('examples');
        $uploadedFile = UploadedFile::fake()->createWithContent($filename, file_get_contents($storage->path($filename)));

        $processDto = new ProcessDto(
            processType: ProcessTypeEnum::Importer,
            processInterpreter: $interpreter,
            activeStatus: ActiveStatusEnum::Active,
            name: $this->faker->word,
            pluginId: $plugin?->id,
            applianceId: $appliance->id
        );

        return $this->processService->createWithApp($processDto, $uploadedFile);
    }

    public function getProcessFileDto(string $alias, string $path, string $mime): ProcessFileDto
    {
        $uploadedFile = UploadedFile::fake()->createWithContent($alias, file_get_contents(
            $path
        ));
        $uploadedFile->mimeType($mime);

        return new ProcessFileDto($uploadedFile, $alias);
    }
}
