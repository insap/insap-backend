<?php

declare(strict_types=1);

namespace Modules\Process\Tests\Unit;

use App\Enums\ActiveStatusEnum;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\Tests\Fixture\ApplianceFixture;
use Modules\Plugin\Tests\Fixture\PluginFixture;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessApp\ProcessAppServiceInterface;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use Storage;
use Tests\TestCase;
use Webmozart\Assert\InvalidArgumentException;

class ProcessServiceTest extends TestCase
{
    private ?ProcessServiceInterface $processService;

    private ?ProcessAppServiceInterface $processAppService;

    private ?ProcessFieldServiceInterface $processFieldService;

    private ?ApplianceFixture $applianceFixture;

    private ?PluginFixture $pluginFixture;

    public function setUp(): void
    {
        parent::setUp();
        $this->processService = $this->app->make(ProcessServiceInterface::class);
        $this->processAppService = $this->app->make(ProcessAppServiceInterface::class);
        $this->processFieldService = $this->app->make(ProcessFieldServiceInterface::class);

        $this->applianceFixture = $this->app->make(ApplianceFixture::class);
        $this->pluginFixture = $this->app->make(PluginFixture::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->processService = null;
        $this->processAppService = null;
        $this->applianceFixture = null;
        $this->pluginFixture = null;
    }

    private function createWithInterpreter(ProcessInterpreterEnum $interpreter, string $filename): Process
    {
        $appliance = $this->applianceFixture->create();

        $storage = Storage::disk('examples');
        $uploadedFile = UploadedFile::fake()->createWithContent($filename, file_get_contents($storage->path($filename)));

        $processDto = new ProcessDto(
            processType: ProcessTypeEnum::Importer,
            processInterpreter: $interpreter,
            activeStatus: ActiveStatusEnum::Active,
            name: $this->faker->word,
            applianceId: $appliance->id
        );

        $process = $this->processService->createWithApp($processDto, $uploadedFile);

        $this->assertEquals($processDto->name, $process->name);
        $this->assertEquals($processDto->processType, $process->type);
        $this->assertEquals($processDto->processInterpreter, $process->interpreter);
        $this->assertEquals($processDto->activeStatus, ActiveStatusEnum::tryFrom($process->is_active));

        return $process;
    }

    public function testCreate(): void
    {
        $process = $this->createWithInterpreter(ProcessInterpreterEnum::Php, 'importer_php.zip');

        Storage::disk('process')->assertExists((string) $process->id);
        $this->processService->delete($process);

        $process = $this->createWithInterpreter(ProcessInterpreterEnum::Python, 'importer_python.zip');

        Storage::disk('process')->assertExists((string) $process->id);
        $this->processService->delete($process);

        //        $process = $this->createWithInterpreter(ProcessInterpreterEnum::Go, 'importer_go.zip');
        //        Storage::disk('process')->assertExists((string) $process->id)->deleteDirectory((string) $process->id);
    }

    public function testProcessDelete(): void
    {
        $process = $this->createWithInterpreter(ProcessInterpreterEnum::Php, 'importer_php.zip');

        $id = $process->id;
        $this->processService->delete($process);

        Storage::disk('process')->assertMissing((string) $id);

        $this->expectException(InvalidArgumentException::class);

        $this->processService->getById($process->id);
    }

    public function testUpdateApp(): void
    {
        $process = $this->createWithInterpreter(ProcessInterpreterEnum::Php, 'importer_php.zip');

        $uploadedFile = UploadedFile::fake()->createWithContent('importer_php.zip', file_get_contents(Storage::disk('examples')->path('importer_php.zip')));
        $this->processAppService->updateApp($process, $uploadedFile);

        $this->assertNotNull($this->processService->getById($process->id));

        Storage::disk('process')->assertExists((string) $process->id);
        $this->processService->delete($process);
    }

    public function testUpdateProcess(): void
    {
        $plugin = $this->pluginFixture->create();

        $process = $this->createWithInterpreter(ProcessInterpreterEnum::Php, 'importer_php.zip');

        $name = $this->faker->word;
        $this->processService->updateName($process, $name);

        $this->assertSame($name, $process->name);

        $description = $this->faker->word;
        $this->processService->updateDescription($process, $description);

        $this->assertSame($description, $process->description);

        $this->processService->updatePlugin($process, $plugin);
        $this->assertSame($plugin->id, $process->plugin_id);

        $this->processService->updatePlugin($process, null);
        $this->assertNull($process->plugin_id);

        Storage::disk('process')->assertExists((string) $process->id);
        $this->processService->delete($process);
    }

    public function testGetFieldsByProcess(): void
    {
        $process = $this->createWithInterpreter(ProcessInterpreterEnum::Python, 'importer-python-adcp.zip');

        $result = $this->processFieldService->getByProcess($process);

        $this->assertGreaterThan(0, $result->count());
        Storage::disk('process')->assertExists((string) $process->id);
        $this->processService->delete($process);
    }
}
