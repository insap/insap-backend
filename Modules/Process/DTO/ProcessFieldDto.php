<?php

declare(strict_types=1);

namespace Modules\Process\DTO;

use App\Enums\ActiveStatusEnum;
use Illuminate\Contracts\Support\Arrayable;
use Modules\Process\Enums\ProcessFieldEnum;
use Modules\Process\Models\Process;

class ProcessFieldDto implements Arrayable
{
    public function __construct(
        public int $processId,
        public ProcessFieldEnum $field,
        public string $alias,
        public string $title,
        public int $order,
        public bool $required,
        public ActiveStatusEnum $activeStatus,
        public mixed $defaultValue,
        public ?string $description = null,
        public ?string $icon = null,
        public ?array $selectOptions = null
    ) {
    }

    public function toArray(): array
    {
        return [
            'field_type' => $this->field->value,
            'required' => $this->required,
            'alias' => $this->alias,
            'title' => $this->title,
            'order' => $this->order,
            'is_active' => $this->activeStatus->value,
            'process_id' => $this->processId,
            'icon' => $this->icon,
            'default_value' => $this->defaultValue,
            'description' => $this->description,
            'select_options' => $this->selectOptions,
        ];
    }

    public static function createFromArray(array $fields, Process $process): self
    {
        return new self(
            processId: $process->id,
            field: ProcessFieldEnum::tryFrom($fields['field_type']),
            alias: $fields['alias'],
            title: $fields['title'],
            order: $fields['order'],
            required: $fields['required'] ?? false,
            activeStatus: ActiveStatusEnum::Active,
            defaultValue: $fields['default_value'],
            description: $fields['description'],
            icon: $fields['icon'],
            selectOptions: $fields['select'] ?? null
        );
    }
}
