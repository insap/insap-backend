<?php

declare(strict_types=1);

namespace Modules\Process\DTO;

use Modules\Process\Http\Requests\ProcessAllRequest;

readonly class ProcessFiltersDto
{
    public function __construct(
        public ?array $appliances = null,
        public ?array $types = null,
        public ?array $langs = null,
        public ?array $tags = null,
        public ?string $search = null,
        public ?int $page = null,
        public ?string $useType = null,
        public int $perPage = 15
    ) {
    }

    public static function fromRequest(ProcessAllRequest $request): self
    {
        return new self(
            $request->input('appliances'),
            $request->input('types'),
            $request->input('langs'),
            $request->input('tags'),
            $request->input('search'),
            $request->input('page'),
            $request->input('use_type')
        );
    }
}
