<?php

declare(strict_types=1);

namespace Modules\Process\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Record\Enums\RecordImportStatusEnum;

class ProcessImportCreateDto implements Arrayable
{
    public array $files;

    public array $params;

    public string $importLog;

    public RecordImportStatusEnum $importStatus;

    public int $userId;

    public int $recordId;

    public int $processId;

    public function __construct(array $files, array $params, string $importLog, RecordImportStatusEnum $importStatus, int $userId, int $recordId, int $processId)
    {
        $this->files = $files;
        $this->params = $params;
        $this->importLog = $importLog;
        $this->importStatus = $importStatus;
        $this->userId = $userId;
        $this->recordId = $recordId;
        $this->processId = $processId;
    }

    public function toArray(): array
    {
        return [
            'files' => $this->files,
            'params' => $this->params,
            'import_log' => $this->importLog,
            'import_status' => $this->importStatus->value,
            'user_id' => $this->userId,
            'record_id' => $this->recordId,
            'process_id' => $this->processId,
        ];
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function getImportLog(): string
    {
        return $this->importLog;
    }

    public function getImportStatus(): RecordImportStatusEnum
    {
        return $this->importStatus;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getRecordId(): int
    {
        return $this->recordId;
    }

    public function getProcessId(): int
    {
        return $this->processId;
    }
}
