<?php

declare(strict_types=1);

namespace Modules\Process\DTO\Interpreter;

class InterpreterArgDto
{
    public function __construct(
        public string $name,
        public string $value
    ) {
    }
}
