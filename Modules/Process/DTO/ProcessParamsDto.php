<?php

namespace Modules\Process\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class ProcessParamsDto implements Arrayable
{
    private Collection $params;

    /** @var ProcessFileDto[]|Collection Request files */
    private Collection $files;

    private Collection $data;

    public function __construct(array $params, array $data = [])
    {
        $this->data = collect($data);
        $this->params = collect($params);
        $this->files = collect();
    }

    public function addFileFromUploaded(UploadedFile $uploadedFile, string $type): void
    {
        $this->files->push(new ProcessFileDto($uploadedFile, $type));
    }

    public function setFilesFromUploaded(array $files): void
    {
        $this->files = collect($files);
    }

    public function getParams(): Collection
    {
        return $this->params;
    }

    /**
     * @return Collection|ProcessFileDto[]
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function getData(): Collection
    {
        return $this->data;
    }

    public function setData(array|Collection $data): void
    {
        $this->data = $data instanceof Collection ? $data : collect($data);
    }

    public function toArray(): array
    {
        return [
            'data' => $this->data,
            'params' => $this->params,
        ];
    }
}
