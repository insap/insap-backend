<?php

declare(strict_types=1);

namespace Modules\Process\DTO;

use Modules\Process\Enums\ProcessFieldEnum;

class ProcessRequestFieldDto
{
    public function __construct(
        public string $alias,
        public ProcessFieldEnum $fieldType,
        public mixed $value
    ) {
    }

    public function isNotEmpty(): bool
    {
        if ($this->getFieldType() === ProcessFieldEnum::CheckBox) {
            return true;
        }

        if ($this->getFieldType() === ProcessFieldEnum::Number && (int) $this->getValue() === 0) {
            return true;
        }

        return (bool) $this->getValue();
    }

    public static function makeFromArray(array $array): self
    {
        return new self($array['alias'], ProcessFieldEnum::tryFrom($array['field_type']), $array['value']);
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function getFieldType(): ProcessFieldEnum
    {
        return $this->fieldType;
    }

    public function getValue()
    {
        return $this->value;
    }
}
