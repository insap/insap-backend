<?php

declare(strict_types=1);

namespace Modules\Process\DTO\FieldRaw;

class FieldSingleColumnDto
{
    public function __construct(
        public bool $enabled,
        public string $column = '',
        public ?bool $json = null,
        public ?string $type = null,
        public ?bool $round = null
    ) {
    }

    public static function fromArray(array $options): self
    {
        $enabled = self::isEnabled($options);
        $columnName = self::getColumnName($options, $enabled);

        return new self(
            enabled: $enabled,
            column: $columnName,
            json: $options['single_column']['json'] ?? null,
            type: $options['single_column']['json_from'] ?? null,
            round: $options['single_column']['round'] ?? null
        );
    }

    private static function isEnabled(array $options): bool
    {
        return isset($options['single_column']['column']) && is_string($options['single_column']['column']);
    }

    private static function getColumnName(array $options, bool $enabled): string
    {
        return $enabled ? $options['single_column']['column'] : '';
    }

    public function toArray(): array
    {
        $result = [];

        if ($this->enabled) {
            $result['single_column'] = [];

            $result['single_column']['column'] = $this->column;

            if ($this->json !== null) {
                $result['single_column']['json'] = $this->json;

                if ($this->type !== null) {
                    $result['single_column']['json_from'] = $this->type;
                }
            }

            if ($this->round !== null) {
                $result['single_column']['round'] = $this->round;
            }

        }

        return $result;
    }
}
