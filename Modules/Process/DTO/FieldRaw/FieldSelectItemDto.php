<?php

declare(strict_types=1);

namespace Modules\Process\DTO\FieldRaw;

use Illuminate\Contracts\Support\Arrayable;

readonly class FieldSelectItemDto implements Arrayable
{
    public function __construct(
        public string $text,
        public string $value
    ) {
    }

    public function toArray(): array
    {
        return [
            'text' => $this->text,
            'value' => $this->value,
        ];
    }
}
