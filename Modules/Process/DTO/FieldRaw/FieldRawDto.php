<?php

declare(strict_types=1);

namespace Modules\Process\DTO\FieldRaw;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Process\Enums\ProcessFieldEnum;
use Modules\Process\Models\ProcessFieldType;

class FieldRawDto implements Arrayable
{
    public function __construct(
        public int $id,
        public ProcessFieldEnum $fieldType,
        public string $alias,
        public string $title,
        public int $order,
        public bool $required,
        public mixed $defaultValue,
        public ?string $description = null,
        public ?string $icon = null,
        public ?FieldSelectOptionsDto $selectOptions = null
    ) {
    }

    public static function fromModel(ProcessFieldType $processFieldType): self
    {
        return new self(
            id: $processFieldType->id,
            fieldType: ProcessFieldEnum::tryFrom($processFieldType->field_type),
            alias: $processFieldType->alias,
            title: $processFieldType->title,
            order: $processFieldType->order,
            required: $processFieldType->required,
            defaultValue: $processFieldType->default_value,
            description: $processFieldType->description,
            icon: $processFieldType->icon,
            selectOptions: $processFieldType->select_options ?
                FieldSelectOptionsDto::fromArray($processFieldType->select_options) : null
        );
    }

    public function toArray(): array
    {
        return [
            'field_type' => $this->fieldType->value,
            'required' => $this->required,
            'alias' => $this->alias,
            'title' => $this->title,
            'order' => $this->order,
            'icon' => $this->icon,
            'default_value' => $this->defaultValue,
            'description' => $this->description,
            'select_options' => $this->selectOptions?->toArray(),
        ];
    }
}
