<?php

declare(strict_types=1);

namespace Modules\Process\DTO\FieldRaw;

use Illuminate\Contracts\Support\Arrayable;

class FieldSelectOptionsDto implements Arrayable
{
    /**
     * @param  FieldSelectItemDto[]|null  $prepend
     * @param  FieldSelectItemDto[]|null  $options
     */
    public function __construct(
        public FieldSingleColumnDto $singleColumn,
        public ?array $prepend = null,
        public ?array $options = null
    ) {
    }

    public static function fromArray(array $options): self
    {
        $prepend = array_key_exists('prepend', $options) ? collect($options['prepend'])
            ->map(fn (array $item) => new FieldSelectItemDto($item['text'], $item['value']))->all() : null;
        $opt = array_key_exists('options', $options) ? collect($options['options'])
            ->map(fn (array $item) => new FieldSelectItemDto($item['text'], $item['value']))->all() : null;

        return new self(
            singleColumn: FieldSingleColumnDto::fromArray($options),
            prepend: $prepend,
            options: $opt
        );
    }

    public function toArray(): array
    {
        return array_merge([
            'prepend' => $this->prepend,
            'options' => $this->options,
        ], $this->singleColumn->toArray());
    }
}
