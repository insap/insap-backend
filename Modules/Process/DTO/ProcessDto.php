<?php

namespace Modules\Process\DTO;

use App\Enums\ActiveStatusEnum;
use Illuminate\Contracts\Support\Arrayable;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Http\Requests\ProcessCreateRequest;

class ProcessDto implements Arrayable
{
    public function __construct(
        public ProcessTypeEnum $processType,
        public ProcessInterpreterEnum $processInterpreter,
        public ActiveStatusEnum $activeStatus,
        public string $name,
        public ?string $description = null,
        public ?int $userId = null,
        public ?int $pluginId = null,
        public ?int $applianceId = null
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'type' => $this->processType->value,
            'interpreter' => $this->processInterpreter->value,
            'is_active' => $this->activeStatus->value,

            'appliance_id' => $this->applianceId,
            'plugin_id' => $this->pluginId,
            'user_id' => $this->userId,
        ];
    }

    public static function createFromRequest(ProcessCreateRequest $request): self
    {
        $processType = ProcessTypeEnum::tryFrom((int) $request->input('type'));
        $processInterpreter = ProcessInterpreterEnum::tryFrom((string) $request->input('interpreter'));

        return new self(
            processType: $processType,
            processInterpreter: $processInterpreter,
            activeStatus: ActiveStatusEnum::Active,
            name: $request->input('name'),
            description: $request->input('description'),
            userId: $request->user()->id,
            pluginId: $request->input('plugin_id') > 0 ? $request->input('plugin_id') : null,
            applianceId: $request->input('appliance_id')
        );
    }
}
