<?php

declare(strict_types=1);

namespace Modules\Process\Enums;

enum ProcessUseTypeEnum: string
{
    case Any = 'any';
    case Internal = 'internal';
    case External = 'external';

    public static function getArrayForExternalUse(): array
    {
        return [
            self::Any,
            self::External,
        ];
    }

    public static function labels(): array
    {
        return [
            self::Any->value => 'Any',
            self::Internal->value => 'internal',
            self::External->value => 'external',
        ];
    }

    public static function labelsArray(): array
    {
        return [
            [
                'text' => 'internal',
                'value' => self::Internal->value,
            ],
            [
                'text' => 'any',
                'value' => self::Any->value,
            ],
            [
                'text' => 'external',
                'value' => self::External->value,
            ],
        ];
    }
}
