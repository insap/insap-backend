<?php

declare(strict_types=1);

namespace Modules\Process\Enums;

enum ProcessFieldEnum: string
{
    case Number = 'number';
    case DateTime = 'dateTime';
    case Text = 'text';
    case File = 'file';
    case CheckBox = 'checkbox';

    case Select = 'select';
}
