<?php

declare(strict_types=1);

namespace Modules\Process\Enums;

enum ProcessOptionEnum: string
{
    case Name = 'name';
    case Description = 'description';
    case Version = 'version';
    case Date = 'date';

    case TransferDataOnMultiplyImport = 'transfer_data_on_multiply_import';
    case OverwriteDataOnMultiplyImport = 'overwrite_exists_data_on_multiply_import';

    case Fields = 'fields';

    //case Select = 'select';
}
