<?php

declare(strict_types=1);

namespace Modules\Process\Enums;

enum ProcessTypeEnum: int
{
    case Importer = 1;
    case Exporter = 2;

    public static function toSelect(): array
    {
        return [
            self::Importer->value => 'Импортер',
            self::Exporter->value => 'Экспортер',
        ];
    }

    public static function labels(): array
    {
        return [
            self::Importer->value => 'importer',
            self::Exporter->value => 'exporter',
        ];
    }

    public static function labelsArray(): array
    {
        return [
            [
                'text' => 'importer',
                'value' => self::Importer->value,
            ],
            [
                'text' => 'exporter',
                'value' => self::Exporter->value,
            ],
        ];
    }
}
