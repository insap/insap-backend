<?php

declare(strict_types=1);

namespace Modules\Process\Enums;

use Modules\Process\Models\Interpreter\InterpreterGo;
use Modules\Process\Models\Interpreter\InterpreterPhp;
use Modules\Process\Models\Interpreter\InterpreterPython;

enum ProcessInterpreterEnum: string
{
    case Php = InterpreterPhp::class;
    case Go = InterpreterGo::class;
    case Python = InterpreterPython::class;

    public static function labels(): array
    {
        return [
            self::Php->value => 'PHP',
            self::Python->value => 'Python',
            self::Go->value => 'Golang',
        ];
    }

    public static function labelsArray(): array
    {
        return [
            [
                'text' => 'PHP',
                'value' => self::Php->value,
            ],
            [
                'text' => 'Python',
                'value' => self::Python->value,
            ],
            [
                'text' => 'Golang',
                'value' => self::Go->value,
            ],
        ];
    }
}
