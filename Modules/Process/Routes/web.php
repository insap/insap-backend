<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Process\Http\Controllers\ProcessCompatibleController;
use Modules\Process\Http\Controllers\ProcessController;

Route::prefix('processes')->middleware('auth:sanctum')->name('processes.')->group(function () {
    Route::post('create', [ProcessController::class, 'createProcess'])->name('create');
    Route::post('update', [ProcessController::class, 'update'])->name('update');
    Route::post('update-app', [ProcessController::class, 'updateApp'])->name('update-app');

    Route::post('get-all', [ProcessController::class, 'all'])->name('all');
    Route::post('get-by-filters-paginated', [ProcessController::class, 'getByFiltersPaginated'])->name('get-by-filters-paginated');
    Route::post('get-types-list', [ProcessController::class, 'getTypesList'])->name('get-types-list');
    Route::post('get-use-types-list', [ProcessController::class, 'getUseTypesList'])->name('get-use-types-list');

    Route::post('get-interpreters-list', [ProcessController::class, 'getInterpretersList'])->name('get-interpreters-list');
    Route::post('get-fields-by-process', [ProcessController::class, 'getFieldsByProcess'])->name('get-fields-by-process');

    Route::prefix('compatible')->name('compatible.')->group(function () {
        Route::post('get-all-compatible-by-exporter', [ProcessCompatibleController::class, 'getAllCompatibleByExporter'])->name('get-compatible-by-process');
        Route::post('get-all-exporters-by-importer', [ProcessCompatibleController::class, 'getAllExportersByImporter'])->name('get-all-exporters-by-importer');
    });
});
