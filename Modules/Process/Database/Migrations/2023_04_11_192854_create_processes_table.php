<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Appliance\Models\Appliance;
use Modules\Plugin\Models\Plugin;
use Modules\Process\Models\Process;
use Modules\User\Entities\User;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Process::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('interpreter');
            $table->smallInteger('type');
            $table->json('options')->nullable();
            $table->bigInteger('appliance_id');
            $table->bigInteger('plugin_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('image_id')->nullable();
            $table->smallInteger('is_active');
            $table->bigInteger('archive_id')->nullable();

            $table->bigInteger('default_id')->nullable();
            $table->json('compatible_processes')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('appliance_id')
                ->references('id')
                ->on(Appliance::TABLE)
                ->onDelete('RESTRICT');

            $table->foreign('plugin_id')
                ->references('id')
                ->on(Plugin::TABLE)
                ->onDelete('SET NULL');

            $table->foreign('image_id')
                ->references('id')
                ->on(\Modules\File\Entities\File::TABLE)
                ->onDelete('SET NULL');

            $table->foreign('archive_id')
                ->references('id')
                ->on(\Modules\File\Entities\File::TABLE)
                ->onDelete('SET NULL');

            $table->foreign('user_id')
                ->references('id')
                ->on(User::TABLE)
                ->onDelete('SET NULL');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Process::TABLE);
    }
};
