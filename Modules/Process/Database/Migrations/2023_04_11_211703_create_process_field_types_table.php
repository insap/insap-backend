<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Process\Models\ProcessFieldType;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(ProcessFieldType::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string('field_type');
            $table->string('alias');
            $table->string('title');
            $table->boolean('required');
            $table->text('description')->nullable();
            $table->string('default_value')->nullable();
            $table->json('select_options')->nullable();
            $table->string('icon')->nullable();
            $table->integer('order');
            $table->smallInteger('is_active');
            $table->unsignedBigInteger('process_id')->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(ProcessFieldType::TABLE);
    }
};
