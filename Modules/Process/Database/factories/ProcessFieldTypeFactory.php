<?php

namespace Modules\Process\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Process\Models\ProcessFieldType;

class ProcessFieldTypeFactory extends Factory
{
    protected $model = ProcessFieldType::class;

    public function definition(): array
    {
        return [
            'field_type' => $this->faker->word(),
            'alias' => $this->faker->word(),
            'title' => $this->faker->word(),
            'order' => $this->faker->randomNumber(),
            'is_active' => $this->faker->randomNumber(),
            'process_id' => $this->faker->word(),
        ];
    }
}
