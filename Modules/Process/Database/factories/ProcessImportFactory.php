<?php

namespace Modules\Process\database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Modules\Record\Models\RecordImport;

class ProcessImportFactory extends Factory
{
    protected $model = RecordImport::class;

    public function definition(): array
    {
        return [
            'order' => $this->faker->randomNumber(),
            'files' => Carbon::now(),
            'params' => Carbon::now(),
            'status' => $this->faker->randomNumber(),
            'record_id' => $this->faker->randomNumber(),
            'process_id' => $this->faker->randomNumber(),
        ];
    }
}
