<?php

namespace Modules\Process\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Process\Models\Process;

class ProcessFactory extends Factory
{
    protected $model = Process::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'interpreter' => $this->faker->word(),
            'appliance_id' => $this->faker->randomNumber(),
            'is_active' => $this->faker->randomNumber(),
        ];
    }
}
