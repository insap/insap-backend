<?php

namespace Modules\Process\Database\Seeders;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\Plugin\Models\Plugin;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Enums\ProcessUseTypeEnum;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessCompatibleServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\User\Services\UserServiceInterface;
use Storage;

class ProcessExportersSeeder extends Seeder
{
    public function run(
        ProcessServiceInterface $processService,
        UserServiceInterface $userService,
        ApplianceServiceInterface $applianceService,
        ProcessCompatibleServiceInterface $processCompatibleService
    ): void {
        Model::unguard();

        $storage = Storage::disk('examples');
        $user = $userService->getUserByEmail(config('app.admin_default_email'));

        $exporters = [
            [
                'file' => 'exporter-php-adcp-sts.zip',
                'interpreter' => ProcessInterpreterEnum::Php,
                'name' => 'ADCP STS',
                'description' => 'Для работы с sts',
                'pluginId' => Plugin::whereSlug('adcp')->first()->id,
                'applianceId' => $applianceService->getBySlug('adcp')->id,
                'use_type' => ProcessUseTypeEnum::External,
                'for_importer_id' => Process::whereName('ADCP базовый')->first()->id,
            ],
            [
                'file' => 'exporter-php-ctd-sts.zip',
                'interpreter' => ProcessInterpreterEnum::Php,
                'name' => 'CTD STS',
                'description' => 'Для работы с sts',
                'pluginId' => Plugin::whereSlug('ctd')->first()->id,
                'applianceId' => $applianceService->getBySlug('ctd')->id,
                'use_type' => ProcessUseTypeEnum::External,
                'for_importer_id' => Process::whereName('CTD базовый')->first()->id,
            ],
            [
                'file' => 'exporter-php-driftery-sts.zip',
                'interpreter' => ProcessInterpreterEnum::Php,
                'name' => 'Дрифтеры STS',
                'description' => 'Для работы с sts',
                'pluginId' => Plugin::whereSlug('drifters')->first()->id,
                'applianceId' => $applianceService->getBySlug('driftery')->id,
                'use_type' => ProcessUseTypeEnum::External,
                'for_importer_id' => Process::whereName('Дрифтеры базовый')->first()->id,
            ],
            [
                'file' => 'exporter-php-meteo-sts.zip',
                'interpreter' => ProcessInterpreterEnum::Php,
                'name' => 'Meteo STS',
                'description' => 'Для работы с sts',
                'pluginId' => null,
                'applianceId' => $applianceService->getBySlug('meteo')->id,
                'use_type' => ProcessUseTypeEnum::External,
                'for_importer_id' => Process::whereName('Meteo базовый')->first()->id,
            ],
        ];

        foreach ($exporters as $exporter) {

            $uploadedFile = UploadedFile::fake()
                ->createWithContent(
                    name: $exporter['file'],
                    content: file_get_contents($storage->path($exporter['file']))
                );

            $exporterHandler = $processService->createWithApp(
                new ProcessDto(
                    processType: ProcessTypeEnum::Exporter,
                    processInterpreter: $exporter['interpreter'],
                    activeStatus: ActiveStatusEnum::Active,
                    name: $exporter['name'],
                    description: $exporter['description'],
                    userId: $user->id,
                    pluginId: $exporter['pluginId'],
                    applianceId: $exporter['applianceId']
                ),
                $uploadedFile
            );

            $processCompatibleService->sync(
                exporter: $exporterHandler,
                compatibleImporters: [$exporter['for_importer_id']]
            );

            $exporterHandler->use_type = ProcessUseTypeEnum::External;
            $exporterHandler->save();
        }
    }
}
