<?php

namespace Modules\Process\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class ProcessDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Model::unguard();

        $this->call([
            ProcessImportersSeeder::class,
            ProcessExportersSeeder::class,
        ]);
    }
}
