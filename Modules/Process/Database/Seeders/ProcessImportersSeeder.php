<?php

namespace Modules\Process\Database\Seeders;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\Plugin\Models\Plugin;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\User\Services\UserServiceInterface;
use Storage;

class ProcessImportersSeeder extends Seeder
{
    public function run(
        ProcessServiceInterface $processService,
        UserServiceInterface $userService,
        ApplianceServiceInterface $applianceService
    ): void {
        Model::unguard();

        $storage = Storage::disk('examples');
        $user = $userService->getUserByEmail(config('app.admin_default_email'));

        $importers = [
            [
                'file' => 'importer-python-adcp.zip',
                'interpreter' => ProcessInterpreterEnum::Python,
                'name' => 'ADCP базовый',
                'description' => 'Базовой импорт для работы в пределах insap',
                'pluginId' => Plugin::whereSlug('adcp')->first()->id,
                'applianceId' => $applianceService->getBySlug('adcp')->id,
            ],
            [
                'file' => 'importer-python-ctd.zip',
                'interpreter' => ProcessInterpreterEnum::Python,
                'name' => 'CTD базовый',
                'description' => 'Базовой импорт для работы в пределах insap',
                'pluginId' => Plugin::whereSlug('ctd')->first()->id,
                'applianceId' => $applianceService->getBySlug('ctd')->id,
            ],
            [
                'file' => 'importer-python-drifters.zip',
                'interpreter' => ProcessInterpreterEnum::Python,
                'name' => 'Дрифтеры базовый',
                'description' => 'Базовой импорт для работы в пределах insap',
                'pluginId' => Plugin::whereSlug('drifters')->first()->id,
                'applianceId' => $applianceService->getBySlug('driftery')->id,
            ],
            [
                'file' => 'importer-python-meteo.zip',
                'interpreter' => ProcessInterpreterEnum::Python,
                'name' => 'Meteo базовый',
                'description' => 'Базовой импорт для работы в пределах insap',
                'pluginId' => null,
                'applianceId' => $applianceService->getBySlug('meteo')->id,
            ],

            [
                'file' => 'importer-php-ctd.zip',
                'interpreter' => ProcessInterpreterEnum::Php,
                'name' => 'CTD для обработанных',
                'description' => 'Импортер для обработанных CTD файлов',
                'pluginId' => Plugin::whereSlug('ctd')->first()->id,
                'applianceId' => $applianceService->getBySlug('ctd')->id,
            ],
        ];

        foreach ($importers as $importer) {

            if (!file_exists($storage->path($importer['file']))  ||!is_readable($storage->path($importer['file']))  ||!is_file($storage->path($importer['file'])))
            {
                throw new \Exception("File {$importer['file']} not found or not readable");
            }

            $uploadedFile = UploadedFile::fake()
                ->createWithContent(
                    name: $importer['file'],
                    content: file_get_contents($storage->path($importer['file']))
                );

            $processService->createWithApp(
                new ProcessDto(
                    processType: ProcessTypeEnum::Importer,
                    processInterpreter: $importer['interpreter'],
                    activeStatus: ActiveStatusEnum::Active,
                    name: $importer['name'],
                    description: $importer['description'],
                    userId: $user->id,
                    pluginId: $importer['pluginId'],
                    applianceId: $importer['applianceId']
                ),
                $uploadedFile
            );
        }
    }
}
