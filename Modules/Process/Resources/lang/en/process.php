<?php

return [
    'importer' => 'Importer',
    'exporter' => 'Exporter',

    'use_type' => [
        'any' => 'Any',
        'internal' => 'For internal use only',
        'external' => 'For external use only',
    ],
];
