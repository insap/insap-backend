<?php

return [
    'importer' => 'Импортер',
    'exporter' => 'Экспортер',

    'use_type' => [
        'any' => 'Любой',
        'internal' => 'Для внутреннего использования',
        'external' => 'Для внешних сервисов',
    ],
];
