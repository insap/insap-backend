<?php

return [
    'check_importer' => 'Проверка на корректность обработчика...',
    'check_importer_fail' => '[Error] Папка обработчика не найдена в файловой системе',

    'pass_params' => 'Отправляем параметры импорта из системы в обработчик...',

    'execute_main_command' => 'Запускаем команду для обработчика [:command]...',
    'execute_main_command_fail' => 'Ошибка при выполнении команды...',

    'retrieve_information_from_importer' => 'Получаем обработанные данные с импортера...',
];
