<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Process\DTO\FieldRaw\FieldSelectItemDto;
use Modules\Process\DTO\ProcessFieldDto;
use Modules\Process\DTO\ProcessFileDto;
use Modules\Process\DTO\ProcessRequestFieldDto;
use Modules\Process\Enums\ProcessFieldEnum;
use Modules\Process\Enums\ProcessOptionEnum;
use Modules\Process\Models\Process;
use Modules\Process\Models\ProcessFieldType;
use Modules\Record\Models\Record;

class ProcessFieldService implements ProcessFieldServiceInterface
{
    public function __construct(
        private PluginServiceInterface $pluginService
    )
    {
    }

    public function installFieldsToProcess(Process $process): void
    {
        $fields = $process->getOptionsByKey(ProcessOptionEnum::Fields->value);

        $processFields = collect();
        foreach ($fields as $field) {
            $diff = collect(ProcessFieldType::REQUIRED_FIELDS)->diff(collect($field)->keys());

            throw_if(
                $diff->count(),
                new \RuntimeException('[Options][Fields] Require properties missing (' . $diff->implode(',') . ' ! Field: ' . collect($field)->keys()->implode(','))
            );

            $dto = ProcessFieldDto::createFromArray($field, $process);
            $processFields->push(ProcessFieldType::create($dto->toArray()));
        }

        $process->fields()->saveMany($processFields);
    }

    public function buildFieldsDtoFromRequest(array $requestArray): Collection
    {
        return collect($requestArray)->map(fn(string|array $field) => ProcessRequestFieldDto::makeFromArray(
            !is_array($field) ? json_decode($field, true, 512, JSON_THROW_ON_ERROR) : $field
        ));
    }

    /**
     * @param Collection<int, ProcessRequestFieldDto> $fields
     * @param Collection<string, string> $fileAliases
     */
    public function makeFileDtoFromFieldsDto(Collection $fields, Collection $fileAliases): Collection
    {
        return $fields->filter(fn(ProcessRequestFieldDto $dto) => $dto->getFieldType() === ProcessFieldEnum::File)
            ->map(fn(ProcessRequestFieldDto $dto) => ProcessFileDto::make($fileAliases->get('_file_alias_' . $dto->getAlias()),
                $dto->getAlias()));
    }

    public function filterFieldsDto(Collection $fields): Collection
    {
        return $fields->filter(fn(ProcessRequestFieldDto $dto) => $dto->isNotEmpty())
            ->mapWithKeys(fn(ProcessRequestFieldDto $dto) => [$dto->getAlias() => $dto->getValue()]);
    }

    public function getByProcess(Process $process): EloquentCollection
    {
        return ProcessFieldType::whereHas('process', function (Builder $query) use ($process) {
            $query->where('id', $process->id);
        })->active()->orderBy('order')->get();
    }

    public function getCalculatedValues(Record $record, Process $process): Collection
    {
        return $this->getByProcess($process)
            ->map(fn(ProcessFieldType $processFieldType) => array_merge($processFieldType->toArray(),
                ['calculated' => $this->calculateField($processFieldType, $record)]
            ));
    }

    private function calculateField(ProcessFieldType $processFieldType, Record $record): array
    {
        $fieldRaw = $processFieldType->makeFieldRawDto();

        if ($fieldRaw->fieldType !== ProcessFieldEnum::Select || !$fieldRaw->selectOptions) {
            return [];
        }

        return \Cache::remember($processFieldType->id . '_' . $record->id, CarbonInterval::week(), function () use ($fieldRaw, $processFieldType, $record) {
            $result = collect();
            $singleColumn = $fieldRaw->selectOptions->singleColumn;

            $result = $result->mergeRecursive(collect($fieldRaw->selectOptions->prepend ?? [])
                ->map(fn(FieldSelectItemDto $dto) => $dto->toArray()));

            if ($singleColumn->enabled) {
                $pluginService = $this->pluginService->getPluginService($processFieldType->process->plugin);
                $getColumnQuery = $pluginService->getSingleColumnFromRecord($record, $singleColumn->column);

                if ($singleColumn->json && $singleColumn->type === 'key') {
                    foreach ($getColumnQuery as $item) {
                        $decoded = !is_array($item) ? json_decode($item, true, 512, JSON_THROW_ON_ERROR) : $item;
                        $result->merge(collect($decoded)->map(fn($value, $key) => $singleColumn->round ? round((float)$key) : $key)->all());
                    }
                }

                $result = $result->merge($getColumnQuery->unique())->map(fn($value) => !is_array($value) ? [
                    'text' => (string)$value,
                    'value' => (string)$value,
                ] : $value);
            }

            if ($fieldRaw->selectOptions->options) {
                $result = $result->mergeRecursive(collect($fieldRaw->selectOptions->options)
                    ->map(fn(FieldSelectItemDto $dto) => $dto->toArray()));
            }

            return $result->all();
        });
    }
}
