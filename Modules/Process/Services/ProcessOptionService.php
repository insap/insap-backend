<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Support\Collection;
use Modules\Process\Enums\ProcessOptionEnum;
use Modules\Process\Models\Process;

class ProcessOptionService implements ProcessOptionServiceInterface
{
    public function getOptionFilePath(Process $process): string
    {
        return $process->getStoragePath().DIRECTORY_SEPARATOR.Process::OPTIONS_FILE_NAME;
    }

    public function isOptionsFileExists(Process $process): bool
    {
        return \File::exists($this->getOptionFilePath($process));
    }

    public function getAllOptions(Process $process): Collection
    {
        return collect(json_decode(file_get_contents($this->getOptionFilePath($process)), true, 512, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE));
    }

    public function getOptionsDiff(Process $process): Collection
    {
        $options = $this->getAllOptions($process);
        $requiredOptionsDiff = collect(array_column(ProcessOptionEnum::cases(), 'value'))->diff($options->keys());

        return $requiredOptionsDiff;
    }

    public function isAllRequiredOptionsExists(Process $process): bool
    {
        return (bool) $this->getOptionsDiff($process);
    }
}
