<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Contracts\Process\ProcessResult;
use Illuminate\Support\Collection;
use Modules\Process\DTO\Interpreter\InterpreterArgDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Exceptions\ProcessInterpreterExecuteException;
use Modules\Process\Models\Interpreter\InterpreterInterface;
use Modules\Process\Models\Process;

interface InterpreterServiceInterface
{
    public function make(ProcessInterpreterEnum $interpreterEnum): InterpreterInterface;

    public function makeFromProcess(Process $process): InterpreterInterface;

    /**
     * @param  InterpreterArgDto[]|Collection|null  $args
     *
     * @throws ProcessInterpreterExecuteException
     */
    public function executeHandlerCommand(
        InterpreterInterface $interpreter,
        ?Collection $args = null,
        ?string $dir = null,
    ): int;

    public function formatCommandForExecute(
        InterpreterInterface $interpreter,
        ?Collection $args = null,
    ): string;

    public function executeRaw(string $rawCommand, int $timeout = 60): ProcessResult;
}
