<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Modules\Process\Models\Process;
use Modules\Process\Models\ProcessFieldType;
use Modules\Record\Models\Record;

interface ProcessFieldServiceInterface
{
    public function buildFieldsDtoFromRequest(array $requestArray): Collection;

    public function makeFileDtoFromFieldsDto(Collection $fields, Collection $fileAliases): Collection;

    /**
     * Удаляем все данные с пустыми значениями(после того как выделили среди них файлы)
     */
    public function filterFieldsDto(Collection $fields): Collection;

    public function installFieldsToProcess(Process $process): void;

    /**
     * @return EloquentCollection<int, ProcessFieldType>
     */
    public function getByProcess(Process $process): EloquentCollection;

    public function getCalculatedValues(Record $record, Process $process): Collection;
}
