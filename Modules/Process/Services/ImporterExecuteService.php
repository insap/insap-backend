<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use DB;
use Modules\File\DTO\LocalArchiveFileDto;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Process\DTO\ProcessFileDto;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Enums\ProcessOptionEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Exceptions\ProcessException;
use Modules\Record\Enums\RecordImportStatusEnum;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordImport;
use Modules\Record\Services\RecordImportServiceInterface;
use Ramsey\Uuid\Uuid;

class ImporterExecuteService
{
    public function __construct(
        private readonly RecordImportServiceInterface $recordImportService,
        private readonly PluginServiceInterface $pluginService,
        private readonly ProcessExecuteServiceInterface $processExecuteService,
    ) {
    }

    /**
     * @throws ProcessException
     * @throws \Throwable
     */
    public function executeProcess(RecordImport $recordImport, array $params = [], array $files = []): bool
    {
        ini_set('memory_limit', '4G');
        $record = $recordImport->record;
        $process = $recordImport->process;

        //throw_if($record->import_status === ProcessImportStatusEnum::, new \RuntimeException("Can't execute process in FINAL state"));
        //throw_if($record->process === null, new \RuntimeException("Can't execute record without process"));

        try {
            DB::beginTransaction();
            $recordImport->log = '[Start]'.PHP_EOL;

            $pluginService = $this->pluginService->getPluginService($process->plugin);

            $processParamsDto = new ProcessParamsDto($params);
            $processParamsDto->setFilesFromUploaded($files);

            if ($record->imports->count() > 1 && $process->getOptionsByKey(ProcessOptionEnum::TransferDataOnMultiplyImport->value) === true) {
                $processParamsDto->setData($pluginService->getDataFromRecord($record));
            }

            // Processing data
            $processParamsDto = $this->processExecuteService->execute(ProcessTypeEnum::Importer, $process, $processParamsDto);

            // Add to DB
            $pluginService->addDataToDatabase($recordImport, $processParamsDto, $process);

            $filesToStore = [];
            foreach ($processParamsDto->getFiles() as $file) {
                $filesToStore[] = $this->storeFileWithMetadata($file, $record->id, $recordImport->id);
            }

            $recordImport->files = array_merge($record->files ?? [], $filesToStore);
            $recordImport->params = $processParamsDto->getParams()->all();
            $recordImport->status = RecordImportStatusEnum::Finished->value;
            $recordImport->log .= $this->processExecuteService->getLog()->getLog().' [End]'.PHP_EOL;
            $recordImport->save();

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();

            $recordImport->status = RecordImportStatusEnum::Error->value;
            $recordImport->log .= $this->processExecuteService->getLog()->getLog().' ...FAIL ';

            if ($exception instanceof ProcessException) {
                $recordImport->log .= $exception->getMessage().' Processing Error '.$exception->getProcessError();
            } else {
                $recordImport->log .= $exception->getMessage().' Trace: '.$exception->getTraceAsString();
            }

            $recordImport->save();
            $this->recordImportService->clearImport($recordImport, false);

            throw $exception;
        } finally {

            unset($processParamsDto);
        }

        return true;
    }

    private function storeFileWithMetadata(ProcessFileDto $fileDto, int $recordId, int $recordImportId): array
    {
        $uploadedFile = $fileDto->getUploadedFile();
        $path = (string) Uuid::uuid4().DIRECTORY_SEPARATOR.$uploadedFile->getClientOriginalName();

        if (\Storage::disk('minio')->put($path, $uploadedFile->getContent(), [
            'Metadata' => [
                Record::RECORD_ID_FIELD => $recordId,
                Record::RECORD_IMPORT_ID_FIELD => $recordImportId,
                'filename' => $uploadedFile->getClientOriginalName(),
                'extension' => $uploadedFile->getClientOriginalExtension(),
                'type' => $fileDto->getAlias(),
            ],
        ])) {
            return LocalArchiveFileDto::fromProcessFile($path, $fileDto)->toArray();
        }

        return [];
    }
}
