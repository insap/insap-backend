<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use App\Services\StaticLogger\StaticLogger;
use File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use JsonMachine\Exception\InvalidArgumentException;
use JsonMachine\Items;
use Modules\Process\DTO\ProcessFileDto;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Exceptions\ProcessException;
use Modules\Process\Exceptions\ProcessInterpreterExecuteException;
use Modules\Process\Models\Process;

class ProcessExecuteService implements ProcessExecuteServiceInterface
{
    public function __construct(
        public InterpreterServiceInterface $interpreterService,
        public StaticLogger $staticLogger
    ) {
    }

    public function execute(
        ProcessTypeEnum $processType,
        Process $process,
        ProcessParamsDto $paramsDto
    ): ProcessParamsDto {
        $this->staticLogger->reset();

        $this->staticLogger->add(__('process::execute.check_importer'));

        if (! Storage::disk('process')->exists((string) $process->id)) {
            $this->staticLogger->addError(__('process::execute.check_importer'));
            throw new \RuntimeException('[Execute] Process dir does not exist...');
        }

        $this->staticLogger->addSuccess();

        /**
         *  First, we need to pass some params to importer directory (copy files, data, params)
         */
        $this->passParamsToImporter($process, $paramsDto);

        /**
         *  Execute importer app
         */
        $interpreter = $this->interpreterService->makeFromProcess($process);

        $command = $this->interpreterService->formatCommandForExecute($interpreter);
        $this->staticLogger->add(__('process::execute.execute_main_command', compact('command')), trackTime: true);

        try {
            $this->interpreterService->executeHandlerCommand(
                interpreter: $interpreter,
                dir: $process->getStoragePath()
            );
        } catch (\Throwable|ProcessInterpreterExecuteException $exception) {
            $this->staticLogger->addError(__('process::execute.execute_main_command_fail'), trackTime: true);

            // Если есть ошибки с импортера берем их
            $importerErrors = $this->getErrorFromImporter($process);

            if (app()->environment('production')) {
                $this->clearAppDir($process);
            }

            if ($importerErrors) {
                throw new ProcessException('[ExecuteEvent] Error from process', 0, $exception, implode(';', $importerErrors));
            }

            throw $exception;
        }

        $importerErrors = $this->getErrorFromImporter($process);

        if ($importerErrors) {
            throw new ProcessException('Error from importer after script command', 0, null, implode(';', $importerErrors));
        }

        $this->staticLogger->addSuccess(trackTime: true);

        /**
         *  Retrieve updated files from importer
         */
        return $this->retrieveInformationFromImporter($process);
    }

    private function passParamsToImporter(Process $process, ProcessParamsDto $paramsDto): void
    {
        $this->staticLogger->add(__('process::execute.pass_params'), trackTime: true);

        $path = $process->getStoragePath();

        $dataPath = $path.DIRECTORY_SEPARATOR.'data';
        $importerFilesPath = $dataPath.DIRECTORY_SEPARATOR.'files';

        $paramsJsonFile = $dataPath.DIRECTORY_SEPARATOR.'params.json';
        $filesJsonFile = $dataPath.DIRECTORY_SEPARATOR.'files.json';
        $dataJsonFile = $dataPath.DIRECTORY_SEPARATOR.'data.json';

        $this->clearAppDir($process);

        $this->writeArrayToJsonFile($paramsDto->getParams()->all(), $paramsJsonFile);

        // Files
        $copyFiles = $this->copyFilesToPath($paramsDto->getFiles()->all(), $importerFilesPath);
        $this->writeArrayToJsonFile($copyFiles, $filesJsonFile);

        $data = $paramsDto->getData()->all();
        $this->writeArrayToJsonFile($data, $dataJsonFile);

        $this->staticLogger->addSuccess(trackTime: true);
    }

    private function getErrorFromImporter(Process $process): array
    {
        $path = $process->getStoragePath();

        $resultPath = $path.DIRECTORY_SEPARATOR.'result';
        $errorJsonFile = $resultPath.DIRECTORY_SEPARATOR.'errors.json';

        // Если импортер сгенерировал файл с ошибками, то выводим их
        if (file_exists($errorJsonFile)) {
            $errors = $this->decodeJsonToArray($errorJsonFile);

            if ($errors) {
                return $errors;
            }

        }

        return [];
    }

    private function clearAppDir(Process $process): void
    {
        $path = $process->getStoragePath();

        $importerDataPath = $path.DIRECTORY_SEPARATOR.'data';
        $importerFilesPath = $importerDataPath.DIRECTORY_SEPARATOR.'files';
        $importerResultPath = $path.DIRECTORY_SEPARATOR.'result';

        $this->clearAndCreateDir($importerDataPath);
        $this->clearAndCreateDir($importerFilesPath);
        $this->clearAndCreateDir($importerResultPath);
    }

    private function clearAndCreateDir(string $path, bool $onlyClear = false): void
    {
        if (File::exists($path)) {
            File::deleteDirectory($path);
        }

        if (! $onlyClear) {
            File::makeDirectory($path, 0755, true, true);
        }

        if (! File::exists($path)) {
            throw new \RuntimeException('Cannot create directory '.$path);
        }
    }

    private function writeArrayToJsonFile(array $array, string $jsonFile): void
    {
        File::put($jsonFile, json_encode($array, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE));
    }

    /**
     * @param  ProcessFileDto[]  $files
     */
    private function copyFilesToPath(array $files, string $path): array
    {
        $result = [];

        foreach ($files as $fileDto) {
            $file = $fileDto->getUploadedFile();

            $extension = $file->getClientOriginalExtension() ?: $file->getExtension();
            $fileName = $this->getFixedHashNameByExtension(
                hashName: $file->hashName(),
                originalExtension: $file->getClientOriginalExtension(),
                extension: $file->guessExtension()
            );

            $result[] = [
                'name' => $fileName,
                'original' => $file->getClientOriginalName(),
                'extension' => $extension,
                'mime' => $file->getMimeType(),
                'type' => $fileDto->getAlias(),
                'alias' => $fileDto->getAlias(),
            ];

            $fullPath = $path.DIRECTORY_SEPARATOR.$fileName;
            File::put($fullPath, $file->getContent());
        }

        return $result;
    }

    private function getFixedHashNameByExtension(string $hashName, string $originalExtension, string $extension): string
    {
        if ($originalExtension !== $extension) {
            $hashName = \Str::replace($extension, $originalExtension, $hashName);
        }

        return $hashName;
    }

    private function retrieveInformationFromImporter(Process $process): ProcessParamsDto
    {
        $this->staticLogger->add(__('process::execute.retrieve_information_from_importer'), trackTime: true);
        $path = $process->getStoragePath();

        $resultPath = $path.DIRECTORY_SEPARATOR.'result';
        $filesPath = $resultPath.DIRECTORY_SEPARATOR.'files';

        $paramsJsonFile = $resultPath.DIRECTORY_SEPARATOR.'params.json';
        $filesJsonFile = $resultPath.DIRECTORY_SEPARATOR.'files.json';
        $dataJsonFile = $resultPath.DIRECTORY_SEPARATOR.'data.json';

        $params = $this->decodeJsonToArray($paramsJsonFile, false);
        $files = $this->decodeJsonToArray($filesJsonFile);

        $processedData = [];
        if (file_exists($dataJsonFile)) {
            $processedData = $this->decodeJsonToArray($dataJsonFile);
        }

        $processParamsDto = new ProcessParamsDto($params, $processedData);

        foreach ($files as $file) {
            $path = $filesPath.DIRECTORY_SEPARATOR.$file['name'];
            $processParamsDto->addFileFromUploaded(new UploadedFile($path, $file['original'], $file['mime']), $file['type']);
        }

        $this->staticLogger->addSuccess(trackTime: true);

        return $processParamsDto;
    }

    /**
     * @throws InvalidArgumentException
     */
    private function decodeJsonToArray(string $path, bool $convertToArray = true): array
    {
        $result = [];
        foreach (Items::fromFile($path) as $key => $item) {
            $result[$key] = $convertToArray ? (array) $item : $item;
        }

        return $result;
    }

    public function getLog(): StaticLogger
    {
        return $this->staticLogger;
    }
}
