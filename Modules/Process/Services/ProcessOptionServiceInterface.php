<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Support\Collection;
use Modules\Process\Models\Process;

interface ProcessOptionServiceInterface
{
    public function getOptionFilePath(Process $process): string;

    public function isOptionsFileExists(Process $process): bool;

    public function getAllOptions(Process $process): Collection;

    public function getOptionsDiff(Process $process): Collection;

    public function isAllRequiredOptionsExists(Process $process): bool;
}
