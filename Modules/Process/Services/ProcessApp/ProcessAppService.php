<?php

declare(strict_types=1);

namespace Modules\Process\Services\ProcessApp;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use JsonMachine\Items;
use Log;
use Modules\File\Services\FileServiceInterface;
use Modules\Process\DTO\Interpreter\InterpreterArgDto;
use Modules\Process\Models\Process;
use Modules\Process\Services\InterpreterServiceInterface;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Process\Services\ProcessOptionServiceInterface;
use PhpZip\Exception\ZipException;
use PhpZip\ZipFile;

class ProcessAppService implements ProcessAppServiceInterface
{
    public function __construct(
        private readonly FileServiceInterface $fileService,
        private readonly InterpreterServiceInterface $interpreterService,
        private readonly ProcessOptionServiceInterface $optionsService,
        private readonly ProcessFieldServiceInterface $processFieldService
    ) {
    }

    public function installApp(Process $process, UploadedFile $archive): void
    {
        $storage = Storage::disk('process');
        $storage->makeDirectory((string) $process->id);

        $path = $process->getStoragePath();

        try {
            $zip = new ZipFile();
            $zip->openFile($archive->getRealPath())
                ->extractTo($path);
        } catch (ZipException $exception) {
            Log::error("Can't Open/Extract file '{$archive->getRealPath()}' to '$path'", [
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
                'uploadedFile' => $archive,
                'path' => $path,
                'process' => $process,
            ]);

            throw new \RuntimeException("Message: ". $exception->getMessage() .". Can't Open/Extract file '{$archive->getRealPath()}' to '$path'", 0, $exception);
        }

        $interpreter = $this->interpreterService->make($process->interpreter);

        $this->installRequirements($process);
        $this->installOptions($process);

        // Test Command
        $exitCode = $this->interpreterService->executeHandlerCommand(
            interpreter: $interpreter,
            args: collect([new InterpreterArgDto('test', '1')]),
            dir: $path
        );

        throw_if($exitCode > 0, new \RuntimeException('[Install] Test command exit code greater then 0, ['.$exitCode.']'));
    }

    public function updateApp(Process $process, UploadedFile $archive): Process
    {
        $this->deleteApp($process);

        $process->fields()->delete();

        cache()->flush();

        $this->installApp($process, $archive);

        return $process;
    }

    /**
     * @throws \Throwable
     */
    private function installRequirements(Process $process): void
    {
        $path = $process->getStoragePath();

        $requirementsJsonFile = $path.DIRECTORY_SEPARATOR.Process::REQUIREMENTS_FILE_NAME;

        throw_if(! \File::exists($requirementsJsonFile), new \RuntimeException("Can't find requirements file in [".$requirementsJsonFile.']'));

        $cdCommand = 'cd '.$path;
        $detectComposer = false;

        foreach (Items::fromFile($requirementsJsonFile, ['pointer' => '/commands']) as $command) {

            if (\Str::startsWith($command, 'composer')) {
                $detectComposer = true;
            }

            $finalCommand = $detectComposer ? "export COMPOSER_HOME=\"/tmp\";$cdCommand;$command" : "$cdCommand;$command";

            $this->interpreterService->executeRaw(
                rawCommand: $finalCommand,
                timeout: 600
            );
        }
    }

    private function installOptions(Process $process): void
    {
        throw_if(! $this->optionsService->isOptionsFileExists($process), new \RuntimeException("Can't find options file in [".$this->optionsService->getOptionFilePath($process).']'));
        throw_if(! $this->optionsService->isAllRequiredOptionsExists($process), new \RuntimeException('Please provide all required options, such as '.$this->optionsService->getOptionsDiff($process)->implode(',')));

        $process->fill(['options' => $this->optionsService->getAllOptions($process)->all()])->save();

        $this->processFieldService->installFieldsToProcess($process);
    }

    public function deleteApp(Process $process): void
    {
        Storage::disk('process')->deleteDirectory((string) $process->id);

        if ($process->archiveFile) {
            $this->fileService->delete($process->archiveFile);
        }
    }
}
