<?php

declare(strict_types=1);

namespace Modules\Process\Services\ProcessApp;

use Illuminate\Http\UploadedFile;
use Modules\Process\Models\Process;

interface ProcessAppServiceInterface
{
    public function installApp(Process $process, UploadedFile $archive): void;

    public function updateApp(Process $process, UploadedFile $archive): Process;

    public function deleteApp(Process $process): void;
}
