<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\File\Services\FileServiceInterface;
use Modules\Plugin\Models\Plugin;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\DTO\ProcessFiltersDto;
use Modules\Process\Enums\ProcessUseTypeEnum;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessApp\ProcessAppServiceInterface;
use Modules\Tags\Services\TagServiceInterface;
use Modules\User\Services\UserServiceInterface;
use Storage;
use Webmozart\Assert\Assert;

class ProcessService implements ProcessServiceInterface
{
    public function __construct(
        private readonly FileServiceInterface $fileService,
        private readonly UserServiceInterface $userService,
        private readonly ProcessAppServiceInterface $processAppService,
        private readonly TagServiceInterface $tagService
    ) {
    }

    public function getAll(): Collection
    {
        return Process::active()->get();
    }

    public function getById(int $id): Process
    {
        $model = Process::find($id);

        Assert::notNull($model, "Can't find process with id $id");

        return $model;
    }

    public function createWithApp(ProcessDto $dto, UploadedFile $archive): Process
    {
        \DB::beginTransaction();
        $process = Process::create($dto->toArray());

        try {
            $this->processAppService->installApp($process, $archive);

            $file = $this->fileService->createFromUploadedFile($archive, $dto->userId ? $this->userService->getUserById($dto->userId) : null);
            $process->archiveFile()->associate($file)->save();

            \DB::commit();
        } catch (\Throwable $throwable) {
            $this->delete($process);

            $process = null;
            \DB::rollBack();
            throw $throwable;
        }

        return $process;
    }

    public function delete(Process $process): ?bool
    {
        $this->processAppService->deleteApp($process);

        if ($process->imageFile) {
            $this->fileService->delete($process->imageFile);
        }

        return $process->delete();
    }

    private function deleteApp(Process $process): void
    {
        Storage::disk('process')->deleteDirectory((string) $process->id);

        if ($process->archiveFile) {
            $this->fileService->delete($process->archiveFile);
        }
    }

    public function updateName(Process $process, string $name): bool
    {
        return $process->fill(['name' => $name])->save();
    }

    public function updateDescription(Process $process, string $description): bool
    {
        return $process->fill(['description' => $description])->save();
    }

    public function updatePlugin(Process $process, ?Plugin $plugin): bool
    {
        if ($plugin && $plugin->id !== $process->plugin_id) {
            Assert::eq(0, $process->imports()->count(), trans('process.change_plugin_while_has_record'));
            $process->plugin()->associate($plugin);
        }

        if ($plugin === null && $process->plugin_id) {
            Assert::eq(0, $process->imports()->count(), trans('process.change_plugin_while_has_record'));

            $process->plugin()->delete();
            $process->plugin_id = null;
        }

        return $process->save();
    }

    public function getAllByFilters(ProcessFiltersDto $dto): Collection|LengthAwarePaginator
    {
        $data = Process::active()
            ->when($dto->appliances, function (Builder $builder) use ($dto) {
                $builder->whereIn('appliance_id', $dto->appliances);
            })
            ->when($dto->tags, function (Builder $builder) use ($dto) {
                $builder->whereHas('tags', fn (Builder $builder) => $builder->whereIn('name', $dto->tags));
            })
            ->when($dto->types, function (Builder $builder) use ($dto) {
                $builder->whereIn('type', $dto->types);
            })
            ->when($dto->langs, function (Builder $builder) use ($dto) {
                $builder->whereIn('interpreter', $dto->langs);
            })
            ->when($dto->useType, function (Builder $builder) use ($dto) {
                $builder->whereIn('use_type', $dto->useType);
            })
            ->when($dto->search && strlen($dto->search) > 1, function (Builder $builder) use ($dto) {
                $builder->where(fn (Builder $query) => $query
                    ->orWhere('name', 'ilike', '%'.$dto->search.'%')
                    ->orWhere('interpreter', 'ilike', '%'.$dto->search.'%')
                );
            });

        return $dto->page ? $data->paginate(
            perPage: $dto->perPage,
            page: $dto->page
        ) : $data->get();
    }

    public function updateUseType(Process $process, ProcessUseTypeEnum $useTypeEnum): bool
    {
        return $process->fill(['use_type' => $useTypeEnum])->save();
    }

    public function updateTags(Process $process, array $tags): bool
    {
        $this->tagService->sync($process->tags, $tags, $process->tags());

        return true;
    }
}
