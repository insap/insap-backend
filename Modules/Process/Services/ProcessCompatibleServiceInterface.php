<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Process\Models\Process;

interface ProcessCompatibleServiceInterface
{
    public function addImporterToCompatible(Process $exporter, Process $importer): bool;

    public function sync(Process $exporter, array $compatibleImporters = []): void;

    public function setDefaultImporter(Process $exporter, Process $importer): bool;

    public function getAllCompatibleByExporter(Process $exporter): Collection;

    public function getAllExportersByImporter(Process $importer): Collection;

    public function existsCompatibleWithExporterAndImporter(Process $exporter, Process $importer): bool;
}
