<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Models\Process;

readonly class ProcessCompatibleService implements ProcessCompatibleServiceInterface
{
    public function __construct(
        private ProcessServiceInterface $processService
    ) {
    }

    public function addImporterToCompatible(Process $exporter, Process $importer): bool
    {
        $processes = $exporter->compatible_processes;
        $processes[] = $importer->id;

        $exporter->compatible_processes = array_unique($processes);

        return $exporter->save();
    }

    public function setDefaultImporter(Process $exporter, Process $importer): bool
    {
        $exporter->default_id = $importer->id;

        return $this->addImporterToCompatible($exporter, $importer);
    }

    public function getAllCompatibleByExporter(Process $exporter): Collection
    {
        $processes = $this->processService->getAll();
        $processes->load(['appliance']);

        return $processes
            ->filter(fn (Process $processFilter) => $processFilter->type === ProcessTypeEnum::Importer &&
                $processFilter->id !== $exporter->id &&
                $exporter->appliance->id === $processFilter->appliance->id
            );
    }

    public function existsCompatibleWithExporterAndImporter(Process $exporter, Process $importer): bool
    {
        return in_array($importer->id, $exporter->compatible_processes, true);
    }

    public function sync(Process $exporter, array $compatibleImporters = []): void
    {
        foreach ($compatibleImporters as $importerId) {
            $importer = $this->processService->getById($importerId);
            $this->addImporterToCompatible($exporter, $importer);
        }
    }

    public function getAllExportersByImporter(Process $importer): Collection
    {
        $processes = $this->processService->getAll();
        $processes->load(['appliance']);

        return $processes
            ->filter(fn (Process $processFilter) => in_array($importer->id, $processFilter->compatible_processes ?? [], true)
            );
    }
}
