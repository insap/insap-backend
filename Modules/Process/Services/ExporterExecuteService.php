<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use App\Models\Common\Response;
use App\Services\ZipperServiceInterface;
use Illuminate\Support\Collection;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;
use Modules\Record\Services\RecordServiceInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Throwable;

class ExporterExecuteService
{
    public function __construct(
        public RecordServiceInterface $recordService,
        public ProcessExecuteServiceInterface $processExecuteService,
        public ZipperServiceInterface $zipperService,
        private ProcessCompatibleServiceInterface $processCompatibleService
    ) {
    }

    public function executeProcess(Process $process, Record $record, array $params = [], array $files = []): ProcessParamsDto
    {
        ini_set('memory_limit', '2G');

        $record->load('imports.process');

        foreach ($record->imports as $recordImport) {
            throw_if(
                ! $this->processCompatibleService->existsCompatibleWithExporterAndImporter($process, $recordImport->process),
                new RuntimeException("Can't execute without compatible exporter (".$process->id.') and importer ('.$recordImport->process->id.')')
            );
        }

        try {
            $processParamsDto = new ProcessParamsDto($params);
            $processParamsDto->setFilesFromUploaded($files);

            $processParamsDto->setData($this->recordService->getDataFromRecord($record));

            // Processing data
            $processParamsDto = $this->processExecuteService->execute(ProcessTypeEnum::Exporter, $process, $processParamsDto);

            return $processParamsDto;

        } catch (Throwable $exception) {
            throw new RuntimeException('[Exporter] export event failed <'.$exception->getMessage().'>', 0, $exception);
        }
    }

    /**
     * @param  Collection<int, Record>  $records
     */
    public function executeProcessMultiply(Process $process, Collection $records, array $params): ProcessParamsDto
    {
        ini_set('memory_limit', '2G');

        foreach ($records as $record) {
            foreach ($record->imports as $recordImport) {
                throw_if(
                    ! $this->processCompatibleService->existsCompatibleWithExporterAndImporter($process, $recordImport->process),
                    new RuntimeException("Can't execute without compatible exporter (".$process->id.') and importer ('.$recordImport->process->id.')')
                );
            }
        }

        try {
            $params['_multiply_export'] = true;
            $processParamsDto = new ProcessParamsDto($params);

            $data = collect();
            foreach ($records as $record) {
                $data->put($record->id, $this->recordService->getDataFromRecord($record));
            }

            $processParamsDto->setData($data);
            // Processing data
            $processParamsDto = $this->processExecuteService->execute(ProcessTypeEnum::Exporter, $process, $processParamsDto);

            return $processParamsDto;

        } catch (Throwable $exception) {
            throw $exception;
        }
    }

    public function formatResponse(ProcessParamsDto $processParamsDto, Record $record, Process $process): StreamedResponse|Response
    {
        $response = new Response();

        if ($processParamsDto->getFiles()->count() > 0) {
            $zipArchive = $this->zipperService->createAndDownloadZip($this->generateFileName($record, $process), $processParamsDto->getFiles());

            return $zipArchive->response();
        }

        return $response->withData($processParamsDto->toArray());
    }

    private function generateFileName(Record $record, Process $process): string
    {
        $name = \Str::transliterate($record->name.'_'.$process->name).'.zip';

        if ($record->date) {
            $name = $record->date->format('Y-m-d').'_'.$name;
        }

        return sanitize_filename($name);
    }
}
