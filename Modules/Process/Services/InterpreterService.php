<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Contracts\Process\ProcessResult;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Process;
use Modules\Process\DTO\Interpreter\InterpreterArgDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Exceptions\ProcessInterpreterExecuteException;
use Modules\Process\Models\Interpreter\InterpreterInterface;
use RuntimeException;

class InterpreterService implements InterpreterServiceInterface
{
    public function executeHandlerCommand(
        InterpreterInterface $interpreter,
        ?Collection $args = null,
        ?string $dir = null,
    ): int {
        $command = $this->formatCommandForExecute($interpreter, $args);

        $process = Process::command($command)
            ->path($dir ?: '')
            ->run();

        $exitCode = $process->exitCode();

        if (! $process->successful()) {
            throw new ProcessInterpreterExecuteException(
                processResult: $process,
                message: 'Error while execute '.$process->command()." with message [$exitCode]",
            );
        }

        return $exitCode;
    }

    public function make(ProcessInterpreterEnum $interpreterEnum): InterpreterInterface
    {
        return new ($interpreterEnum->value);
    }

    public function executeRaw(string $rawCommand, int $timeout = 60): ProcessResult
    {
        $process = Process::timeout($timeout)->run($rawCommand);
        $exitCode = $process->exitCode();

        if (! $process->successful()) {
            $error = $process->errorOutput();
            throw new RuntimeException("[Interpreter] Error while execute raw '$rawCommand' with message [$exitCode] : '$error'");
        }

        return $process;
    }

    public function makeFromProcess(\Modules\Process\Models\Process $process): InterpreterInterface
    {
        return $this->make($process->interpreter);
    }

    public function formatCommandForExecute(InterpreterInterface $interpreter, ?Collection $args = null): string
    {
        $command = $interpreter->getCommand('app');
        $argsString = $args ? $args->reduce(fn ($result, InterpreterArgDto $dto) => $result .= '--'.$dto->name.'='.$dto->value.' ', '') : '';

        return $command.' '.$argsString;
    }
}
