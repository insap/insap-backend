<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\Plugin\Models\Plugin;
use Modules\Process\DTO\ProcessDto;
use Modules\Process\DTO\ProcessFiltersDto;
use Modules\Process\Enums\ProcessUseTypeEnum;
use Modules\Process\Models\Process;

interface ProcessServiceInterface
{
    /**
     * @return Collection<int, Process>
     */
    public function getAll(): Collection;

    public function getAllByFilters(ProcessFiltersDto $dto): Collection|LengthAwarePaginator;

    public function getById(int $id): Process;

    public function createWithApp(ProcessDto $dto, UploadedFile $archive): Process;

    public function delete(Process $process): ?bool;

    public function updateName(Process $process, string $name): bool;

    public function updateDescription(Process $process, string $description): bool;

    public function updatePlugin(Process $process, ?Plugin $plugin): bool;

    public function updateUseType(Process $process, ProcessUseTypeEnum $useTypeEnum): bool;

    public function updateTags(Process $process, array $tags): bool;
}
