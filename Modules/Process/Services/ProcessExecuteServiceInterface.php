<?php

declare(strict_types=1);

namespace Modules\Process\Services;

use App\Services\StaticLogger\StaticLogger;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Exceptions\ProcessException;
use Modules\Process\Models\Process;

interface ProcessExecuteServiceInterface
{
    /**
     * @throws ProcessException
     * @throws \Throwable
     */
    public function execute(
        ProcessTypeEnum $processType,
        Process $process,
        ProcessParamsDto $paramsDto
    ): ProcessParamsDto;

    public function getLog(): StaticLogger;
}
