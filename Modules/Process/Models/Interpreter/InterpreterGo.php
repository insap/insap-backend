<?php

namespace Modules\Process\Models\Interpreter;

use Illuminate\Support\Collection;

class InterpreterGo implements InterpreterInterface
{
    protected string $launcher = 'go';

    protected string $extension = 'go';

    protected string $action = 'run';

    public function getCommand(string $app, ?Collection $options = null): string
    {
        return "go run $app.go";
    }
}
