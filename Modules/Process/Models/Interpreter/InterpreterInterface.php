<?php

namespace Modules\Process\Models\Interpreter;

use Illuminate\Support\Collection;

interface InterpreterInterface
{
    public function getCommand(string $app, ?Collection $options = null): string;
}
