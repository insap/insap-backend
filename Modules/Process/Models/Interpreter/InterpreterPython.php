<?php

namespace Modules\Process\Models\Interpreter;

use Illuminate\Support\Collection;

class InterpreterPython implements InterpreterInterface
{
    public function getCommand(string $app, ?Collection $options = null): string
    {
        return "python3 $app.py";
    }
}
