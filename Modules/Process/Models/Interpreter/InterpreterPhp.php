<?php

namespace Modules\Process\Models\Interpreter;

use Illuminate\Support\Collection;

class InterpreterPhp implements InterpreterInterface
{
    public function getCommand(string $app, ?Collection $options = null): string
    {
        $memory_limit = $options && $options->get('memory_limit', '1G') ?: '1G';

        return "php -d memory_limit=$memory_limit $app.php";
    }
}
