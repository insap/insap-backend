<?php

namespace Modules\Process\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\User\Entities\User;

/**
 * Modules\Process\Models\ProcessCompatible
 *
 * @property-read \Modules\Process\Models\Process|null $exporter
 * @property-read \Modules\Process\Models\Process|null $importer
 * @property-read User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessCompatible newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessCompatible newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessCompatible query()
 *
 * @mixin \Eloquent
 */
class ProcessCompatible extends Model
{
    use HasFactory;

    public const TABLE = 'process_compatibles';

    protected $table = self::TABLE;

    protected $fillable = [
        'importer_id',
        'exporter_id',
        'default_for_export',
        'user_id',
    ];

    public function importer(): BelongsTo
    {
        return $this->belongsTo(Process::class, 'importer_id', 'id');
    }

    public function exporter(): BelongsTo
    {
        return $this->belongsTo(Process::class, 'exporter_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
