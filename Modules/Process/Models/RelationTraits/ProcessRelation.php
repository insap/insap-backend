<?php

declare(strict_types=1);

namespace Modules\Process\Models\RelationTraits;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Modules\Appliance\Models\Appliance;
use Modules\File\Entities\File;
use Modules\Option\Models\Option;
use Modules\Plugin\Models\Plugin;
use Modules\Process\Models\ProcessFieldType;
use Modules\Record\Models\RecordImport;
use Modules\Tags\Models\Tag;
use Modules\User\Entities\User;

trait ProcessRelation
{
    public function appliance(): BelongsTo
    {
        return $this->belongsTo(Appliance::class);
    }

    public function plugin(): BelongsTo
    {
        return $this->belongsTo(Plugin::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function fields(): HasMany
    {
        return $this->hasMany(ProcessFieldType::class);
    }

    public function imports(): HasMany
    {
        return $this->hasMany(RecordImport::class);
    }

    public function imageFile(): BelongsTo
    {
        return $this->belongsTo(File::class, 'image_id');
    }

    public function archiveFile(): BelongsTo
    {
        return $this->belongsTo(File::class, 'archive_id');
    }

    public function tags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function features(): MorphToMany
    {
        return $this->morphToMany(Option::class, 'optional', Option::OPTIONAL_TABLE);
    }
}
