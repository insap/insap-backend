<?php

namespace Modules\Process\Models;

use App\Helpers\ImageHelper;
use App\Scopes\ActiveScope;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Modules\Appliance\Models\Appliance;
use Modules\File\Entities\File;
use Modules\Plugin\Models\Plugin;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Enums\ProcessUseTypeEnum;
use Modules\Process\Models\RelationTraits\ProcessRelation;
use Modules\Record\Models\RecordImport;
use Modules\Tags\Models\Tag;
use Modules\User\Entities\User;

/**
 * Modules\Process\Models\Process
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property ProcessInterpreterEnum $interpreter
 * @property ProcessTypeEnum $type
 * @property array|null $options
 * @property int $appliance_id
 * @property int|null $plugin_id
 * @property int|null $user_id
 * @property int|null $image_id
 * @property int $is_active
 * @property int|null $archive_id
 * @property int|null $default_id
 * @property array|null $compatible_processes
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property ProcessUseTypeEnum $use_type
 * @property-read Appliance $appliance
 * @property-read File|null $archiveFile
 * @property-read Collection<int, \Modules\Process\Models\ProcessFieldType> $fields
 * @property-read int|null $fields_count
 * @property-read string $image
 * @property-read File|null $imageFile
 * @property-read Collection<int, RecordImport> $imports
 * @property-read int|null $imports_count
 * @property-read Plugin|null $plugin
 * @property-read User|null $user
 *
 * @method static Builder|Process active()
 * @method static Builder|Process newModelQuery()
 * @method static Builder|Process newQuery()
 * @method static Builder|Process onlyTrashed()
 * @method static Builder|Process query()
 * @method static Builder|Process whereApplianceId($value)
 * @method static Builder|Process whereArchiveId($value)
 * @method static Builder|Process whereCompatibleProcesses($value)
 * @method static Builder|Process whereCreatedAt($value)
 * @method static Builder|Process whereDefaultId($value)
 * @method static Builder|Process whereDeletedAt($value)
 * @method static Builder|Process whereDescription($value)
 * @method static Builder|Process whereId($value)
 * @method static Builder|Process whereImageId($value)
 * @method static Builder|Process whereInterpreter($value)
 * @method static Builder|Process whereIsActive($value)
 * @method static Builder|Process whereName($value)
 * @method static Builder|Process whereOptions($value)
 * @method static Builder|Process wherePluginId($value)
 * @method static Builder|Process whereType($value)
 * @method static Builder|Process whereUpdatedAt($value)
 * @method static Builder|Process whereUseType($value)
 * @method static Builder|Process whereUserId($value)
 * @method static Builder|Process withTrashed()
 * @method static Builder|Process withoutTrashed()
 *
 * @property-read int|null $options_count
 * @property-read Collection<int, Tag> $tags
 * @property-read int|null $tags_count
 *
 * @mixin Eloquent
 */
class Process extends Model
{
    use ActiveScope, HasFactory, ProcessRelation, SoftDeletes;

    public const TABLE = 'processes';

    public const REQUIREMENTS_FILE_NAME = 'requirements.json';

    public const OPTIONS_FILE_NAME = 'options.json';

    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'description',
        'interpreter',
        'type',
        'options',
        'appliance_id',
        'plugin_id',
        'user_id',
        'image_id',
        'is_active',
        'archive_id',
        'default_id',
        'compatible_processes',
        'use_type',
    ];

    protected $casts = [
        'options' => 'array',
        'compatible_processes' => 'array',
        'use_type' => ProcessUseTypeEnum::class,
        'type' => ProcessTypeEnum::class,
        'interpreter' => ProcessInterpreterEnum::class,
    ];

    protected $attributes = [
        'use_type' => ProcessUseTypeEnum::Internal,
    ];

    protected function optionFields(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => json_decode($attributes['options'], true, 512, JSON_THROW_ON_ERROR)['fields'],
        );
    }

    public function getOptionsByKey(string $key)
    {
        return collect($this->options)->get($key);
    }

    public function getImageAttribute(): string
    {
        return $this->image_id ? File::find($this->image_id)->url : ImageHelper::getAvatarImage($this->name);
    }

    public function getStoragePath(): string
    {
        return Storage::disk('process')->path((string) $this->id);
    }
}
