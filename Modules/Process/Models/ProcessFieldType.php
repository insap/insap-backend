<?php

namespace Modules\Process\Models;

use App\Enums\ActiveStatusEnum;
use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Process\DTO\FieldRaw\FieldRawDto;

/**
 * Modules\Process\Models\ProcessFieldType
 *
 * @property int $id
 * @property string $field_type
 * @property string $alias
 * @property string $title
 * @property bool $required
 * @property string|null $description
 * @property string|null $default_value
 * @property array|null $select_options
 * @property string|null $icon
 * @property int $order
 * @property int $is_active
 * @property int $process_id
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Process\Models\Process|null $process
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType active()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereDefaultValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereFieldType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereSelectOptions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ProcessFieldType withoutTrashed()
 *
 * @mixin \Eloquent
 */
class ProcessFieldType extends Model
{
    use ActiveScope, HasFactory, SoftDeletes;

    public const TABLE = 'process_field_types';

    protected $table = self::TABLE;

    public const REQUIRED_FIELDS = [
        'field_type',
        'alias',
        'title',
        'order',
    ];

    protected $fillable = [
        'field_type',
        'alias',
        'title',
        'description',
        'default_value',
        'icon',
        'order',
        'is_active',
        'process_id',
        'required',
        'select_options',
    ];

    protected $casts = [
        'select_options' => 'array',
    ];

    protected $attributes = [
        'is_active' => ActiveStatusEnum::Active->value,
    ];

    public function process(): BelongsTo
    {
        return $this->belongsTo(Process::class, 'process_id');
    }

    public function makeFieldRawDto(): FieldRawDto
    {
        return FieldRawDto::fromModel($this);
    }
}
