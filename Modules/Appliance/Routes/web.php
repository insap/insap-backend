<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Appliance\Http\Controllers\ApplianceController;

Route::prefix('appliances')->middleware('auth:sanctum')->name('appliances.')->group(function () {
    Route::post('/', [ApplianceController::class, 'index'])->name('all');

    Route::post('create', [ApplianceController::class, 'create'])->name('create');
    Route::post('update', [ApplianceController::class, 'update'])->name('update');

    Route::post('change-image', [ApplianceController::class, 'changeImage'])->name('change-image');
});
