<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('appliances')->middleware('client_auth')->name('appliances.external.')->group(function () {
    Route::post('/', [\Modules\Appliance\Http\Controllers\ExternalApplianceController::class, 'index'])->name('all');
});
