<?php

declare(strict_types=1);

namespace Modules\Appliance\DTO;

class ApplianceFilterDto
{
    public function __construct(
        public ?array $slugs,
    ) {
    }
}
