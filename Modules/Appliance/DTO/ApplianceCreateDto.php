<?php

declare(strict_types=1);

namespace Modules\Appliance\DTO;

use App\Enums\ActiveStatusEnum;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Str;

readonly class ApplianceCreateDto implements Arrayable
{
    public function __construct(
        public string $name,
        public ?string $description,
        public ActiveStatusEnum $activeStatusEnum,
        public ?int $userId = null,
        public ?string $slug = null
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'is_active' => $this->activeStatusEnum->value,
            'user_id' => $this->userId,
            'slug' => $this->slug ?? Str::slug($this->name),
        ];
    }
}
