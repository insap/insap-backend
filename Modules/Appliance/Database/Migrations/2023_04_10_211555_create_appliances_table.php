<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Appliance\Models\Appliance;
use Modules\User\Entities\User;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Appliance::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->smallInteger('is_active');
            $table->bigInteger('user_id')->nullable();

            $table->bigInteger('image_id')->nullable();

            $table->string('slug')->unique();

            $table->foreign('image_id')
                ->references('id')
                ->on(\Modules\File\Entities\File::TABLE)
                ->onDelete('SET NULL');

            $table->foreign('user_id')
                ->references('id')
                ->on(User::TABLE)
                ->onDelete('SET NULL');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Appliance::TABLE);
    }
};
