<?php

namespace Modules\Appliance\Database\Seeders;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Appliance\DTO\ApplianceCreateDto;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\User\Services\UserServiceInterface;

class ApplianceDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(
        ApplianceServiceInterface $applianceService,
        UserServiceInterface $userService
    ) {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        $userId = $userService->findUserByEmail(config('app.admin_default_email'))->id;

        $applianceService->create(new ApplianceCreateDto('ADCP', 'adcp', ActiveStatusEnum::Active, $userId));
        $applianceService->create(new ApplianceCreateDto('CTD', 'ctd', ActiveStatusEnum::Active, $userId));
        $applianceService->create(new ApplianceCreateDto('Метео', 'meteo', ActiveStatusEnum::Active, $userId));
        $applianceService->create(new ApplianceCreateDto('Дрифтеры', 'дрифтеры', ActiveStatusEnum::Active, $userId));
    }
}
