<?php

namespace Modules\Appliance\Database\factories;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Appliance\Models\Appliance;

class ApplianceFactory extends Factory
{
    protected $model = Appliance::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'is_active' => ActiveStatusEnum::Active->value,
        ];
    }
}
