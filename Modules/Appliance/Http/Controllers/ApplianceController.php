<?php

namespace Modules\Appliance\Http\Controllers;

use App\Enums\ActiveStatusEnum;
use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Appliance\DTO\ApplianceCreateDto;
use Modules\Appliance\Http\Requests\ApplianceChangeImageRequest;
use Modules\Appliance\Http\Requests\ApplianceCreateRequest;
use Modules\Appliance\Http\Requests\ApplianceUpdateRequest;
use Modules\Appliance\Http\Resources\ApplianceResource;
use Modules\Appliance\Services\ApplianceServiceInterface;

class ApplianceController extends Controller
{
    public function __construct(private readonly ApplianceServiceInterface $applianceService)
    {
    }

    /**
     * @OA\Post (
     *     path="web/appliances",
     *     tags={"appliances", "web"},
     *     summary="Приборы",
     *     description="Получения списка всех приборов",
     *
     *     @OA\Response(
     *          response=200,
     *          description="Успех",
     *
     *          @OA\JsonContent(ref="#/components/schemas/ApplianceResource")
     *     ),
     *
     *     @OA\Response(
     *          response=403,
     *          description="Ошибка в правах"
     *     )
     *
     * )
     */
    public function index(): Response
    {
        $response = Response::make();
        $appliances = $this->applianceService->getAllAppliances();

        $appliances->load(['processes', 'user']);

        return $response->withData(ApplianceResource::collection($appliances));
    }

    public function create(ApplianceCreateRequest $request): Response
    {
        $response = Response::make();

        $appliance = $this->applianceService->create(new ApplianceCreateDto(
            name: $request->input('name'),
            description: $request->input('description'),
            activeStatusEnum: ActiveStatusEnum::Active,
            userId: $request->user()->id,
        ), $request->file('image'));

        return $response->withData(ApplianceResource::make($appliance));
    }

    public function update(ApplianceUpdateRequest $request): Response
    {
        $response = Response::make();

        $appliance = $this->applianceService->getById((int) $request->input('appliance_id'));

        if ($request->input('name')) {
            $this->applianceService->updateName($appliance, $request->input('name'));
        }

        if ($request->input('description')) {
            $this->applianceService->updateDescription($appliance, $request->input('description'));
        }

        if ($request->file('image')) {
            $this->applianceService->changeImage($appliance, $request->file('image'));
        }

        return $response->success();
    }

    public function changeImage(ApplianceChangeImageRequest $request): Response
    {
        $response = Response::make();

        $appliance = $this->applianceService->getById((int) $request->input('appliance_id'));
        $this->applianceService->changeImage($appliance, $request->file('image'));

        return $response->success();
    }
}
