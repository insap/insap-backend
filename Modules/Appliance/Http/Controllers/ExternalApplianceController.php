<?php

namespace Modules\Appliance\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Appliance\Http\Resources\ExternalApplianceResource;
use Modules\Appliance\Services\ApplianceServiceInterface;

class ExternalApplianceController extends Controller
{
    public function __construct(
        private readonly ApplianceServiceInterface $applianceService
    ) {
    }

    public function index(): Response
    {
        $response = Response::make();

        $data = $this->applianceService->getAllAppliances();

        return $response->withData(ExternalApplianceResource::collection($data));
    }
}
