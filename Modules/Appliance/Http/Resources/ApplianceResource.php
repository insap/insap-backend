<?php

namespace Modules\Appliance\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Appliance\Models\Appliance;
use Modules\Process\Http\Resources\ProcessResource;
use Modules\User\Http\Resources\UserResource;

/** @mixin Appliance */
class ApplianceResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at->format('d.m.Y H:i:s'),

            'image' => $this->image,

            'processes' => ProcessResource::collection($this->whenLoaded('processes')),
            'creator_user' => UserResource::make($this->whenLoaded('user')),
        ];
    }
}
