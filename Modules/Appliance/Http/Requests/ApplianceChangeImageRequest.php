<?php

namespace Modules\Appliance\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Appliance\Models\Appliance;

class ApplianceChangeImageRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'appliance_id' => 'required|int|exists:'.Appliance::TABLE.',id',
            'image' => 'required|file',
        ];
    }
}
