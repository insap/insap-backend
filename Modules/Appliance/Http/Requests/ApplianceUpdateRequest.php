<?php

namespace Modules\Appliance\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Appliance\Models\Appliance;

class ApplianceUpdateRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'appliance_id' => 'required|int|exists:'.Appliance::TABLE.',id',
            'name' => 'string|nullable',
            'description' => 'string|nullable',
            'image' => 'file|nullable',
        ];
    }
}
