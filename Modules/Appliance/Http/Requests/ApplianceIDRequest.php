<?php

namespace Modules\Appliance\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Appliance\Models\Appliance;

class ApplianceIDRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'appliance_id' => 'required|int|exists:'.Appliance::TABLE.', id',
        ];
    }
}
