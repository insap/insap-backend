<?php

namespace Modules\Appliance\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class ApplianceCreateRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['nullable', 'string'],
            'image' => 'nullable|file',
        ];
    }
}
