<?php

declare(strict_types=1);

namespace Modules\Appliance\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\DTO\ApplianceCreateDto;
use Modules\Appliance\DTO\ApplianceFilterDto;
use Modules\Appliance\Models\Appliance;
use Modules\File\Services\FileServiceInterface;

readonly class ApplianceService implements ApplianceServiceInterface
{
    public function __construct(
        private FileServiceInterface $fileService
    ) {
    }

    public function create(ApplianceCreateDto $dto, ?UploadedFile $uploadedFile = null): Appliance
    {
        $imageId = null;

        if ($uploadedFile) {
            $imageId = $this->fileService->createFromUploadedFile($uploadedFile)->id;
        }

        return Appliance::create(array_merge($dto->toArray(), ['image_id' => $imageId]));
    }

    public function getAllAppliances(): Collection
    {
        return Appliance::active()->orderBy('id')->get();
    }

    public function getByName(string $name): Appliance
    {
        return Appliance::whereName($name)->firstOrFail();
    }

    public function updateName(Appliance $appliance, string $newName): bool
    {
        return $appliance->fill(['name' => $newName])->save();
    }

    public function updateDescription(Appliance $appliance, string $description): bool
    {
        return $appliance->fill(['description' => $description])->save();
    }

    public function changeImage(Appliance $appliance, UploadedFile $image): bool
    {
        if ($appliance->imageFile) {
            $this->fileService->delete($appliance->imageFile);
        }

        $file = $this->fileService->createFromUploadedFile($image);

        return $appliance->imageFile()->associate($file)->save();
    }

    public function getById(int $id): Appliance
    {
        return Appliance::findOrFail($id);
    }

    public function getBySlug(string $slug): Appliance
    {
        return Appliance::whereSlug($slug)->firstOrFail();
    }

    public function getAllAppliancesFilter(ApplianceFilterDto $dto): Collection
    {
        return Appliance::active()
            // @phpstan-ignore-next-line
            ->when($dto->slugs, fn (Builder $builder) => $builder->whereIn('slug', $dto->slugs))
            ->get();
    }

    public function delete(Appliance $appliance): bool
    {
        if ($appliance->image_id) {
            $this->fileService->delete($appliance->imageFile);
        }

        return $appliance->delete();
    }
}
