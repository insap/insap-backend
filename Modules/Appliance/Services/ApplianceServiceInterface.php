<?php

declare(strict_types=1);

namespace Modules\Appliance\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\DTO\ApplianceCreateDto;
use Modules\Appliance\DTO\ApplianceFilterDto;
use Modules\Appliance\Models\Appliance;

interface ApplianceServiceInterface
{
    public function create(ApplianceCreateDto $dto, ?UploadedFile $uploadedFile = null): Appliance;

    public function delete(Appliance $appliance): bool;

    public function getByName(string $name): Appliance;

    public function getBySlug(string $slug): Appliance;

    public function getById(int $id): Appliance;

    /** @return Appliance[]|Collection */
    public function getAllAppliances(): Collection;

    public function getAllAppliancesFilter(ApplianceFilterDto $dto): Collection;

    public function updateName(Appliance $appliance, string $newName): bool;

    public function updateDescription(Appliance $appliance, string $description): bool;

    public function changeImage(Appliance $appliance, UploadedFile $image): bool;
}
