<?php

namespace Modules\Appliance\Tests\Unit;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\DTO\ApplianceCreateDto;
use Modules\Appliance\DTO\ApplianceFilterDto;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\Appliance\Tests\Fixture\ApplianceFixture;
use Modules\File\Services\FileServiceInterface;
use Tests\TestCase;

class ApplianceServiceTest extends TestCase
{
    private ?ApplianceServiceInterface $applianceService;

    private ?FileServiceInterface $fileService;

    private ?ApplianceFixture $applianceFixture;

    public function setUp(): void
    {
        parent::setUp();

        $this->applianceService = $this->app->make(ApplianceServiceInterface::class);
        $this->fileService = $this->app->make(FileServiceInterface::class);

        $this->applianceFixture = $this->app->make(ApplianceFixture::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->applianceService = null;
        $this->fileService = null;

        $this->applianceFixture = null;
    }

    public function testCreate(): void
    {
        $dto = new ApplianceCreateDto(
            name: $this->faker->word,
            description: $this->faker->words(10, true),
            activeStatusEnum: ActiveStatusEnum::Active
        );

        $appliance = $this->applianceService->create($dto);

        $this->assertSame($dto->name, $appliance->name);
        $this->assertSame($dto->description, $appliance->description);
        $this->assertSame($dto->activeStatusEnum->value, $appliance->is_active);
    }

    public function testCreateWithImage(): void
    {
        $dto = new ApplianceCreateDto(
            name: $this->faker->word,
            description: $this->faker->words(10, true),
            activeStatusEnum: ActiveStatusEnum::Active
        );

        $uploadedFile = UploadedFile::fake()->image('test.jpg');

        $appliance = $this->applianceService->create($dto, $uploadedFile);

        $this->assertSame($dto->name, $appliance->name);
        $this->assertSame($dto->description, $appliance->description);
        $this->assertSame($dto->activeStatusEnum->value, $appliance->is_active);
        $this->assertNotNull($appliance->image_id);

        $this->applianceService->delete($appliance);
    }

    public function testDelete(): void
    {
        $appliance = $this->applianceFixture->createWithImage();

        $fileId = $appliance->image_id;
        $applianceId = $appliance->id;

        $this->applianceService->delete($appliance);

        $this->expectException(ModelNotFoundException::class);
        $result = $this->applianceService->getById($applianceId);

        $this->expectException(ModelNotFoundException::class);
        $file = $this->fileService->getById($fileId);
    }

    public function testGetByName(): void
    {
        $appliance = $this->applianceFixture->create();

        $result = $this->applianceService->getByName($appliance->name);

        $this->assertNotNull($result);
    }

    public function testGetBySlug(): void
    {
        $appliance = $this->applianceFixture->create();

        $result = $this->applianceService->getBySlug($appliance->slug);

        $this->assertNotNull($result);
    }

    public function testGetById(): void
    {
        $appliance = $this->applianceFixture->create();

        $result = $this->applianceService->getById($appliance->id);

        $this->assertNotNull($result);
    }

    public function testGetAllActiveAppliances(): void
    {
        $this->applianceFixture->create();
        $this->applianceFixture->createInactive();

        $result = $this->applianceService->getAllAppliances();
        $this->assertGreaterThan(0, $result->count());
    }

    public function testGetAllActiveAppliancesByFilters(): void
    {
        $appliance = $this->applianceFixture->create();
        $this->applianceFixture->createInactive();

        $dto = new ApplianceFilterDto(slugs: [$appliance->slug]);
        $result = $this->applianceService->getAllAppliancesFilter($dto);

        $this->assertGreaterThan(0, $result->count());
    }

    public function testUpdateName(): void
    {
        $appliance = $this->applianceFixture->create();
        $newName = $this->faker->word;

        $this->applianceService->updateName($appliance, $newName);

        $appliance->refresh();

        $this->assertSame($newName, $appliance->name);
    }

    public function testUpdateDescription(): void
    {
        $appliance = $this->applianceFixture->create();
        $newDescription = $this->faker->words(3, true);

        $this->applianceService->updateDescription($appliance, $newDescription);

        $appliance->refresh();

        $this->assertSame($newDescription, $appliance->description);
    }

    public function testChangeImage(): void
    {
        $appliance = $this->applianceFixture->create();
        $uploadedFile = UploadedFile::fake()->image('test_image.png', 100, 100);

        $this->applianceService->changeImage($appliance, $uploadedFile);

        $appliance->refresh();

        $this->assertTrue($this->fileService->exists($appliance->imageFile->path));

        $this->applianceService->delete($appliance);
    }
}
