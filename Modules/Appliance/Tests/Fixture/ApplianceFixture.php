<?php

declare(strict_types=1);

namespace Modules\Appliance\Tests\Fixture;

use App\Enums\ActiveStatusEnum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\DTO\ApplianceCreateDto;
use Modules\Appliance\Models\Appliance;
use Modules\Appliance\Services\ApplianceServiceInterface;

class ApplianceFixture
{
    use WithFaker;

    public function __construct(
        private readonly ApplianceServiceInterface $applianceService
    ) {
        $this->setUpFaker();
    }

    public function create(
        ?string $name = null,
        ?ActiveStatusEnum $activeStatusEnum = ActiveStatusEnum::Active,
        ?UploadedFile $uploadedFile = null
    ): Appliance {
        $dto = new ApplianceCreateDto(
            name: $name ?? $this->faker->word,
            description: $this->faker->words(10, true),
            activeStatusEnum: $activeStatusEnum
        );

        return $this->applianceService->create($dto, $uploadedFile);
    }

    public function createWithImage(): Appliance
    {
        return $this->create(
            name: $this->faker->word,
            uploadedFile: UploadedFile::fake()->image('test.jpg')
        );
    }

    public function createInactive(?string $name = null): Appliance
    {
        return $this->create($name, ActiveStatusEnum::Inactive);
    }
}
