<?php

namespace Modules\Appliance\Models;

use App\Enums\ActiveStatusEnum;
use App\Helpers\ImageHelper;
use App\Scopes\ActiveScope;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\File\Entities\File;
use Modules\Option\Models\Option;
use Modules\Process\Models\Process;
use Modules\User\Entities\User;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Modules\Appliance\Models\Appliance
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int $is_active
 * @property int|null $user_id
 * @property int|null $image_id
 * @property string $slug
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read string $image
 * @property-read File|null $imageFile
 * @property-read Collection<int, Process> $processes
 * @property-read int|null $processes_count
 * @property-read User|null $user
 *
 * @method static Builder|Appliance active()
 * @method static Builder|Appliance newModelQuery()
 * @method static Builder|Appliance newQuery()
 * @method static Builder|Appliance onlyTrashed()
 * @method static Builder|Appliance query()
 * @method static Builder|Appliance whereCreatedAt($value)
 * @method static Builder|Appliance whereDeletedAt($value)
 * @method static Builder|Appliance whereDescription($value)
 * @method static Builder|Appliance whereId($value)
 * @method static Builder|Appliance whereImageId($value)
 * @method static Builder|Appliance whereIsActive($value)
 * @method static Builder|Appliance whereName($value)
 * @method static Builder|Appliance whereSlug($value)
 * @method static Builder|Appliance whereUpdatedAt($value)
 * @method static Builder|Appliance whereUserId($value)
 * @method static Builder|Appliance withTrashed()
 * @method static Builder|Appliance withoutTrashed()
 *
 * @mixin Eloquent
 */
class Appliance extends Model
{
    use ActiveScope, HasFactory, HasSlug, SoftDeletes;

    public const TABLE = 'appliances';

    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'description',
        'is_active',
        'user_id',
        'image_id',
        'slug',
    ];

    protected $attributes = [
        'is_active' => ActiveStatusEnum::Active->value,
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->slugsShouldBeNoLongerThan(config('app.max_slug_length'))
            ->doNotGenerateSlugsOnUpdate()
            ->generateSlugsFrom(['name'])
            ->saveSlugsTo('slug');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function processes(): HasMany
    {
        return $this->hasMany(Process::class);
    }

    public function imageFile(): BelongsTo
    {
        return $this->belongsTo(File::class, 'image_id');
    }

    public function features(): MorphToMany
    {
        return $this->morphToMany(Option::class, 'optional', Option::OPTIONAL_TABLE);
    }

    public function getImageAttribute(): string
    {
        return $this->image_id ? File::find($this->image_id)->url : ImageHelper::getAvatarImage($this->name);
    }
}
