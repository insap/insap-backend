<?php

declare(strict_types=1);

namespace Modules\Record\Enums;

enum RecordImportStatusEnum: int
{
    case Initial = 0;
    case Processing = 1;
    case Finished = 2;
    case Error = 3;

    public static function labels(): array
    {
        return [
            self::Initial->value => 'initial',
            self::Processing->value => 'processing',
            self::Finished->value => 'finished',
            self::Error->value => 'error',
        ];
    }
}
