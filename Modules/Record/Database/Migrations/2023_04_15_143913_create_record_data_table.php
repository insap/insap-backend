<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Record\Models\RecordData;

return new class extends Migration
{
    public function up(): void
    {
        Schema::connection('mongodb')->dropIfExists(RecordData::TABLE);

        Schema::connection('mongodb')->table(RecordData::TABLE, function (Blueprint $table) {

            $table->double('latitude');
            $table->double('longitude');

            $table->dateTime('date_time');

            $table->unsignedBigInteger('record_id');
            $table->unsignedBigInteger('_step_id');
            $table->unsignedBigInteger('_record_import_id');

            $table->index('record_id');
            $table->index('_record_import_id');
            $table->index('_step_id');
            $table->index('date_time');
        });
    }

    public function down(): void
    {
        Schema::connection('mongodb')->dropIfExists(RecordData::TABLE);
    }
};
