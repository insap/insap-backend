<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Appliance\Models\Appliance;
use Modules\Journal\Models\Journal;
use Modules\Record\Models\Record;
use Modules\User\Entities\User;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Record::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('order')->nullable();
            $table->smallInteger('is_active');
            $table->dateTime('date')->nullable();

            $table->bigInteger('journal_id');
            $table->bigInteger('image_id')->nullable();
            $table->bigInteger('user_id');
            $table->bigInteger('default_importer_id')->nullable();
            $table->bigInteger('default_exporter_id')->nullable();
            $table->bigInteger('plugin_id')->nullable();
            $table->bigInteger('appliance_id');

            $table->json('compatible_exporters')->nullable();

            $table->json('files');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('image_id')->references('id')->on(\Modules\File\Entities\File::TABLE)->onDelete('SET NULL');
            $table->foreign('journal_id')->references('id')->on(Journal::TABLE)->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on(User::TABLE)->onDelete('RESTRICT');

            $table->foreign('appliance_id')
                ->references('id')
                ->on(Appliance::TABLE)
                ->onDelete('RESTRICT');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Record::TABLE);
    }
};
