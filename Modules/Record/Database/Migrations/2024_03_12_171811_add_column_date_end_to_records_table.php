<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Record\Models\Record;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table(Record::TABLE, function (Blueprint $table) {
            $table->dateTime('date_end')->nullable();
        });
    }

    public function down(): void
    {
        Schema::table(Record::TABLE, function (Blueprint $table) {
            $table->dropColumn('date_end');
        });
    }
};
