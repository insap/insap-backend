<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordImport;
use Modules\User\Entities\User;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(RecordImport::TABLE, function (Blueprint $table) {
            $table->id();
            $table->integer('order');
            $table->json('files')->nullable();
            $table->json('params')->nullable();
            $table->smallInteger('status');
            $table->text('log')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('record_id');
            $table->unsignedBigInteger('process_id');

            $table->foreign('user_id')
                ->references('id')
                ->on(User::TABLE)
                ->onDelete('SET NULL');

            $table->foreign('record_id')
                ->references('id')
                ->on(Record::TABLE)
                ->onDelete('RESTRICT');

            $table->foreign('process_id')
                ->references('id')
                ->on(Process::TABLE)
                ->onDelete('RESTRICT');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(RecordImport::TABLE);
    }
};
