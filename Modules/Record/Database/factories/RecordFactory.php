<?php

namespace Modules\Record\Database\factories;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Modules\Record\Models\Record;

class RecordFactory extends Factory
{
    protected $model = Record::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->words(10, true),
            'is_active' => ActiveStatusEnum::Active->value,
            'user_id' => $this->faker->randomNumber(),
            'journal_id' => $this->faker->randomNumber(),
            'files' => Carbon::now(),
        ];
    }
}
