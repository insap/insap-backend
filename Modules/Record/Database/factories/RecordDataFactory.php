<?php

namespace Modules\Record\Database\factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Record\Models\RecordData;

class RecordDataFactory extends Factory
{
    protected $model = RecordData::class;

    public function definition(): array
    {
        return [
            'record_id' => $this->faker->randomNumber(),
        ];
    }
}
