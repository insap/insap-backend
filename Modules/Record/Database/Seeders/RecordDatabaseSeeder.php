<?php

namespace Modules\Record\Database\Seeders;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\Journal\Services\JournalServiceInterface;
use Modules\Record\DTO\RecordCreateDto;
use Modules\Record\Services\RecordServiceInterface;
use Modules\User\Services\UserServiceInterface;

class RecordDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(
        RecordServiceInterface $recordService,
        JournalServiceInterface $journalService,
        UserServiceInterface $userService,
        ApplianceServiceInterface $applianceService
    ) {
        Model::unguard();

        $recordService->create(new RecordCreateDto(
            name: 'Test',
            activeStatus: ActiveStatusEnum::Active,
            journalId: $journalService->findJournalById(1)->id,
            userId: $userService->findUserByEmail(config('app.admin_default_email'))->id,
            applianceId: $applianceService->getBySlug('adcp')->id,
            date: Carbon::now()
        ));
    }
}
