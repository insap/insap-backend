<?php

namespace Modules\Record\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Modules\Process\Models\Process;
use Modules\Record\Enums\RecordImportStatusEnum;
use Modules\User\Entities\User;

/**
 * Modules\Record\Models\RecordImport
 *
 * @property int $id
 * @property int $order
 * @property array $files
 * @property array $params
 * @property int $status
 * @property string|null $log
 * @property int|null $user_id
 * @property int $record_id
 * @property int $process_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Process $process
 * @property-read \Modules\Record\Models\Record $record
 * @property-read User|null $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport query()
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereFiles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereLog($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereProcessId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereRecordId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RecordImport whereUserId($value)
 *
 * @mixin \Eloquent
 */
class RecordImport extends Model
{
    use HasFactory;

    public const TABLE = 'record_imports';

    protected $table = self::TABLE;

    protected $fillable = [
        'order',
        'files',
        'params',
        'status',
        'log',
        'user_id',
        'record_id',
        'process_id',
    ];

    protected $attributes = [
        'status' => RecordImportStatusEnum::Initial->value,
    ];

    protected $casts = [
        'files' => 'array',
        'params' => 'array',
    ];

    public function process(): BelongsTo
    {
        return $this->belongsTo(Process::class);
    }

    public function record(): BelongsTo
    {
        return $this->belongsTo(Record::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
