<?php

namespace Modules\Record\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use MongoDB\Laravel\Eloquent\Model;

/**
 * @property-read int $record_id
 */
class RecordData extends Model
{
    use HasFactory;

    public const TABLE = 'record_data';

    protected $collection = self::TABLE;

    protected $connection = 'mongodb';

    protected $guarded = [];

    protected array $dates = ['created_at', 'updated_at', 'date', 'time', 'date_time', 'datetime'];

    public function getRecordAttribute(): ?Record
    {
        return Record::whereId($this->record_id)->first();
    }
}
