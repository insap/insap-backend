<?php

namespace Modules\Record\Models;

use App\Enums\ActiveStatusEnum;
use App\Helpers\ImageHelper;
use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Appliance\Models\Appliance;
use Modules\File\Entities\File;
use Modules\Journal\Models\Journal;
use Modules\Plugin\Models\Plugin;
use Modules\Tags\Models\Tag;
use Modules\User\Entities\User;

/**
 * Modules\Record\Models\Record
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int|null $order
 * @property int $is_active
 * @property \Illuminate\Support\Carbon|null $date
 * @property \Illuminate\Support\Carbon|null $date_end
 * @property int $journal_id
 * @property int|null $image_id
 * @property int $user_id
 * @property int|null $default_importer_id
 * @property int|null $default_exporter_id
 * @property int|null $plugin_id
 * @property int $appliance_id
 * @property mixed|null $compatible_exporters
 * @property array $files
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Appliance $appliance
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Modules\Record\Models\RecordImport> $exporters
 * @property-read int|null $exporters_count
 * @property-read string $image
 * @property-read File|null $imageFile
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Modules\Record\Models\RecordImport> $imports
 * @property-read int|null $imports_count
 * @property-read Journal $journal
 * @property-read Plugin|null $plugin
 * @property-read \Illuminate\Database\Eloquent\Collection<int, Tag> $tags
 * @property-read int|null $tags_count
 * @property-read User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Record newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Record newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Record onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Record query()
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereApplianceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereCompatibleExporters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereDefaultExporterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereDefaultImporterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereFiles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereJournalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record wherePluginId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Record withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Record withoutTrashed()
 *
 * @mixin \Eloquent
 */
class Record extends Model
{
    use ActiveScope, HasFactory, SoftDeletes;

    public const string TABLE = 'records';

    public const string RECORD_ID_FIELD = 'record_id';

    public const string RECORD_IMPORT_ID_FIELD = '_record_import_id';

    public const string RECORD_IMPORT_ID_FIELD_PLUGIN = 'record_import_id';

    public const string STEP_ID_FIELD = '_step_id';

    public const string STEP_ID_FIELD_PLUGIN = 'step_id';

    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'description',
        'order', // Порядок отображения
        'is_active',
        'date',
        'date_end',
        'image_id',
        'default_importer_id',
        'user_id',
        'journal_id',
        'files', // FileIds from MongoFS
        'default_exporter_id',
        'compatible_exporters',

        'appliance_id',
    ];

    protected $casts = [
        'date' => 'datetime',
        'date_end' => 'datetime',
        'files' => 'array',
    ];

    protected $attributes = [
        'is_active' => ActiveStatusEnum::Active->value,
    ];

    public function journal(): BelongsTo
    {
        return $this->belongsTo(Journal::class);
    }

    public function imports(): HasMany
    {
        return $this->hasMany(RecordImport::class, 'record_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function imageFile(): BelongsTo
    {
        return $this->belongsTo(File::class, 'image_id');
    }

    public function plugin(): BelongsTo
    {
        return $this->belongsTo(Plugin::class);
    }

    public function tags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function exporters(): HasMany
    {
        return $this->hasMany(RecordImport::class, 'record_id');
    }

    public function appliance(): BelongsTo
    {
        return $this->belongsTo(Appliance::class);
    }

    public function getImageAttribute(): string
    {
        return $this->image_id ? File::find($this->image_id)->url : ImageHelper::getAvatarImage($this->name);
    }
}
