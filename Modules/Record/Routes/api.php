<?php

use Modules\Record\Http\Controllers\ExternalRecordController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('records')->middleware('client_auth')->name('records.')->group(function () {
    Route::post('export', [ExternalRecordController::class, 'export'])->name('export');

    Route::post('export-multiply', [ExternalRecordController::class, 'exportMultiply'])->name('export-multiply');
});
