<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Record\Http\Controllers\RecordController;
use Modules\Record\Http\Controllers\RecordImportController;
use Modules\Record\Http\Controllers\RecordMapController;

Route::prefix('records')->middleware('auth:sanctum')->name('records.')->group(function () {
    Route::post('create', [RecordController::class, 'create'])->name('create');
    Route::post('update', [RecordController::class, 'update'])->name('update');

    Route::post('get-files', [RecordController::class, 'getAllFiles'])->name('get-files');
    Route::post('add-local-archive', [RecordController::class, 'addLocalArchive'])->name('add-local-archive');
    Route::post('remove-local-archive', [RecordController::class, 'removeLocalArchive'])->name('remove-local-archive');

    Route::get('{record}/download/{oid}', [RecordController::class, 'downloadLocalArchive'])->name('download');
    Route::post('clear', [RecordController::class, 'clearRecord'])->name('clear-record');

    Route::post('get-by-id', [RecordController::class, 'getById'])->name('get-by-id');
    Route::post('get-records-by-journal', [RecordController::class, 'getRecordsByJournal'])->name('get-records-by-journal');

    Route::post('get-fields-calculated-values', [RecordController::class, 'getFieldsCalculatedValues'])->name('get-fields-calculated-values');
    Route::post('get-processes-by-record', [RecordController::class, 'getProcessesByRecord'])->name('get-processes-by-record');

    Route::post('map', RecordMapController::class)->name('map');

    Route::prefix('record-imports')->name('record-imports.')->group(function () {
        Route::post('create', [RecordImportController::class, 'create'])->name('create');
        Route::post('clear', [RecordImportController::class, 'clear'])->name('clear');

        Route::post('delete', [RecordImportController::class, 'delete'])->name('delete');

        Route::post('get-by-record', [RecordImportController::class, 'getByRecord'])->name('get-by-record');

        Route::post('create-and-import', [RecordImportController::class, 'createAndImport'])->name('create-and-import');
        Route::post('export', [RecordImportController::class, 'export'])->name('export');

        Route::get('{recordImport}/download/{oid}', [RecordImportController::class, 'downloadImportArchive'])->name('download');
    });
});
