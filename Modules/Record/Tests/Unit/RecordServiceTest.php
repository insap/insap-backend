<?php

namespace Modules\Record\Tests\Unit;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\Tests\Fixture\ApplianceFixture;
use Modules\Journal\Tests\Fixture\JournalFixture;
use Modules\Record\DTO\RecordCreateDto;
use Modules\Record\Services\RecordServiceInterface;
use Modules\Record\Tests\Fixture\RecordFixture;
use Modules\User\Tests\Fixture\UserFixture;
use Tests\TestCase;

class RecordServiceTest extends TestCase
{
    private ?RecordServiceInterface $recordService;

    private ?JournalFixture $journalFixture;

    private ?UserFixture $userFixture;

    private ?RecordFixture $recordFixture;

    private ?ApplianceFixture $applianceFixture;

    public function setUp(): void
    {
        parent::setUp();

        $this->recordService = $this->app->make(RecordServiceInterface::class);

        $this->journalFixture = $this->app->make(JournalFixture::class);
        $this->userFixture = $this->app->make(UserFixture::class);
        $this->recordFixture = $this->app->make(RecordFixture::class);
        $this->applianceFixture = $this->app->make(ApplianceFixture::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->recordService = null;

        $this->journalFixture = null;
        $this->userFixture = null;
        $this->recordFixture = null;
        $this->applianceFixture = null;
    }

    public function testCreate(): void
    {
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();
        $appliance = $this->applianceFixture->create();

        $dto = new RecordCreateDto(
            name: $this->faker->word,
            activeStatus: ActiveStatusEnum::Active,
            journalId: $journal->id,
            userId: $user->id,
            applianceId: $appliance->id
        );

        $record = $this->recordService->create($dto);

        $this->assertSame($dto->name, $record->name);
        $this->assertSame($dto->activeStatus->value, $record->is_active);
    }

    public function testGetRecordById(): void
    {
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();
        $appliance = $this->applianceFixture->create();

        $record = $this->recordFixture->create($journal, $user, $appliance);

        $result = $this->recordService->getById($record->id);

        $this->assertNotNull($result);
    }

    public function testDeleteRecord(): void
    {
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();
        $appliance = $this->applianceFixture->create();

        $record = $this->recordFixture->create($journal, $user, $appliance);
        $this->recordService->delete($record);

        $this->expectException(ModelNotFoundException::class);

        $this->recordService->getById($record->id);
    }

    public function testAddFileToRecord(): void
    {
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();
        $appliance = $this->applianceFixture->create();

        $record = $this->recordFixture->create($journal, $user, $appliance);

        $uploadedFile = UploadedFile::fake()->image('test_image.png', 100, 100);

        $this->recordService->addFileToRecord($record, $uploadedFile);
        $record->refresh();

        $files = $this->recordService->getRecordsFiles($record);

        $this->assertCount(1, $record->files);
        $this->assertCount(1, $files);

        $this->recordService->removeFileFromRecord($record, $files->first());

        $record->refresh();
        $files = $this->recordService->getRecordsFiles($record);

        $this->assertCount(0, $record->files);
        $this->assertCount(0, $files);
    }
}
