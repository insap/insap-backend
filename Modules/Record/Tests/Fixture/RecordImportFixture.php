<?php

declare(strict_types=1);

namespace Modules\Record\Tests\Fixture;

use Illuminate\Foundation\Testing\WithFaker;
use Modules\Process\Models\Process;
use Modules\Record\DTO\RecordImportDto;
use Modules\Record\Enums\RecordImportStatusEnum;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordImport;
use Modules\Record\Services\RecordImportServiceInterface;

class RecordImportFixture
{
    use WithFaker;

    public function __construct(
        private readonly RecordImportServiceInterface $recordImportService)
    {
        $this->setUpFaker();
    }

    public function create(Record $record, Process $process): RecordImport
    {
        $dto = new RecordImportDto(
            order: 1,
            importStatus: RecordImportStatusEnum::Initial,
            processId: $process->id,
            recordId: $record->id
        );

        return $this->recordImportService->create($dto);
    }
}
