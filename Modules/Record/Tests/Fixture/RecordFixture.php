<?php

declare(strict_types=1);

namespace Modules\Record\Tests\Fixture;

use App\Enums\ActiveStatusEnum;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\Appliance\Models\Appliance;
use Modules\Journal\Models\Journal;
use Modules\Record\DTO\RecordCreateDto;
use Modules\Record\Models\Record;
use Modules\Record\Services\RecordServiceInterface;
use Modules\User\Entities\User;

class RecordFixture
{
    use WithFaker;

    public function __construct(
        private readonly RecordServiceInterface $recordService
    ) {
        $this->setUpFaker();
    }

    public function create(Journal $journal, User $user, Appliance $appliance): Record
    {
        $dto = new RecordCreateDto(
            name: $this->faker->word,
            activeStatus: ActiveStatusEnum::Active,
            journalId: $journal->id,
            userId: $user->id,
            applianceId: $appliance->id
        );

        return $this->recordService->create($dto);
    }
}
