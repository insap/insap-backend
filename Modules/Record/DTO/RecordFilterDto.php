<?php

namespace Modules\Record\DTO;

class RecordFilterDto
{
    public function __construct(
        public ?string $search = null,
        public ?array $ids = null,
        public ?int $page = null,
        public int $perPage = 15
    ) {
    }

    public static function fromRequest(\Request $request): self
    {
        return new self(
            search: $request->input('search'),
            page: $request->input('page'),
        );
    }
}
