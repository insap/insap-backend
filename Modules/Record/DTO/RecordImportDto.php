<?php

declare(strict_types=1);

namespace Modules\Record\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Record\Enums\RecordImportStatusEnum;

class RecordImportDto implements Arrayable
{
    public function __construct(
        public int $order,
        public RecordImportStatusEnum $importStatus,
        public int $processId,
        public int $recordId,
        public ?array $files = null,
        public ?array $params = null,
        public ?string $importLog = null,
        public ?int $userId = null,
    ) {

    }

    public function toArray(): array
    {
        return [
            'order' => $this->order,
            'status' => $this->importStatus->value,
            'files' => $this->files,
            'params' => $this->params,
            'log' => $this->importLog,
            'user_id' => $this->userId,
            'process_id' => $this->processId,
            'record_id' => $this->recordId,
        ];
    }
}
