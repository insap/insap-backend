<?php

declare(strict_types=1);

namespace Modules\Record\DTO;

use App\Enums\ActiveStatusEnum;
use App\Traits\Makeable;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;
use Modules\Record\Http\Requests\RecordCreateRequest;

class RecordCreateDto implements Arrayable
{
    use Makeable;

    public function __construct(
        public string $name,
        public ActiveStatusEnum $activeStatus,
        public int $journalId,
        public int $userId,
        public int $applianceId,
        public ?string $description = null,
        public ?Carbon $date = null,
        public ?Carbon $dateEnd = null,
        public ?array $files = [],
        public ?int $order = null,
        public ?int $imageId = null
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'is_active' => $this->activeStatus->value,
            'date' => $this->date,
            'date_end' => $this->dateEnd,
            'order' => $this->order,
            'files' => $this->files,
            'journal_id' => $this->journalId,
            'user_id' => $this->userId,
            'image_id' => $this->imageId,
            'appliance_id' => $this->applianceId,
        ];
    }

    public static function fromRequest(RecordCreateRequest $request): self
    {
        return new self(
            name: $request->input('name'),
            activeStatus: ActiveStatusEnum::Active,
            journalId: (int) $request->input('journal_id'),
            userId: $request->user()->id,
            applianceId: (int) $request->input('appliance_id'),
            description: $request->input('description'),
            date: $request->input('date') ? Carbon::parse($request->input('date')) : null,
            dateEnd: $request->input('date') ? Carbon::parse($request->input('date_end')) : null,
        );
    }

    public function setImageId(?int $imageId): RecordCreateDto
    {
        $this->imageId = $imageId;

        return $this;
    }
}
