<?php
declare(strict_types=1);

namespace Modules\Record\DTO;

use Carbon\Carbon;
use Modules\Record\Http\Requests\RecordUpdateRequest;

class RecordUpdateDto
{
    public function __construct(
        public ?string $name,
        public ?string $description,
    )
    {
    }

    public static function fromRequest(RecordUpdateRequest $request): self
    {
        return new self(
            name: $request->input('name'),
            description: $request->input('description'),
        );
    }
}
