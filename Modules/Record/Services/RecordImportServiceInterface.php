<?php

declare(strict_types=1);

namespace Modules\Record\Services;

use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Modules\Record\DTO\RecordImportDto;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordImport;

interface RecordImportServiceInterface
{
    public function findById(int $id): ?RecordImport;

    /** @return EloquentCollection<int, RecordImport> */
    public function getImportsByRecordId(int $recordId): EloquentCollection;

    public function create(RecordImportDto $dto): RecordImport;

    public function deleteAllByRecord(Record $record): void;

    public function delete(RecordImport $import): bool;

    public function clearImport(RecordImport $import, bool $clearStatus = true): void;

    public function getNextOrder(?Record $record = null): int;
}
