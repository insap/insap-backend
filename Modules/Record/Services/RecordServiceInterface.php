<?php

declare(strict_types=1);

namespace Modules\Record\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\UploadedFile;
use Modules\File\Entities\File;
use Modules\Journal\Models\Journal;
use Modules\Process\Models\Process;
use Modules\Record\DTO\RecordCreateDto;
use Modules\Record\DTO\RecordFilterDto;
use Modules\Record\DTO\RecordUpdateDto;
use Modules\Record\Models\Record;

interface RecordServiceInterface
{
    public function create(RecordCreateDto $dto, ?UploadedFile $image = null): Record;

    public function getById(int $id): Record;

    public function update(Record $record, RecordUpdateDto $dto): bool;

    public function updateStartDate(Record $record, ?Carbon $date): bool;

    public function updateEndDate(Record $record, ?Carbon $date): bool;

    public function delete(Record $record, bool $force = false): bool;

    public function clear(Record $record): void;

    public function getDataFromRecord(Record $record): EloquentCollection;

    public function getRecordsByJournal(Journal $journal): EloquentCollection;

    /** @return EloquentCollection<int, File> */
    public function getRecordsFiles(Record $record): EloquentCollection;

    public function addFileToRecord(Record $record, UploadedFile $file): bool;

    public function removeFileFromRecord(Record $record, File $file): bool;

    /**
     * @return EloquentCollection<int, Process>
     */
    public function getImportersByRecord(Record $record): EloquentCollection;

    /**
     * @return EloquentCollection<int, Process>
     */
    public function getAllByFilters(RecordFilterDto $dto): EloquentCollection;
}
