<?php

declare(strict_types=1);

namespace Modules\Record\Services;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\UploadedFile;
use Modules\File\Entities\File;
use Modules\File\Services\FileServiceInterface;
use Modules\File\Services\Minio\MinioService;
use Modules\Journal\Models\Journal;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Process\DTO\ProcessFiltersDto;
use Modules\Process\Enums\ProcessTypeEnum;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\Record\DTO\RecordCreateDto;
use Modules\Record\DTO\RecordFilterDto;
use Modules\Record\DTO\RecordUpdateDto;
use Modules\Record\Models\Record;
use function PHPUnit\Framework\assertTrue;

readonly class RecordService implements RecordServiceInterface
{
    public function __construct(
        private PluginServiceInterface $pluginService,
        private RecordImportServiceInterface $processImportService,
        private FileServiceInterface $fileService,
        private ProcessServiceInterface $processService,
    ) {
    }

    public function create(RecordCreateDto $dto, ?UploadedFile $image = null): Record
    {
        if ($image) {
            $dto->setImageId($this->fileService->createFromUploadedFile($image)->id);
        }

        return Record::create($dto->toArray());
    }

    public function getById(int $id): Record
    {
        return Record::findOrFail($id);
    }

    public function update(Record $record, RecordUpdateDto $dto): bool
    {
        $array = [
            'name' => $dto->name ?? $record->name,
            'description' => $dto->description ?? $record->description,
        ];

        return $record->fill($array)->save();
    }

    public function delete(Record $record, bool $force = false): bool
    {
        $this->clear($record);

        if ($record->imageFile) {
            $this->fileService->delete($record->imageFile);
        }

        return $force ? $record->forceDelete() : $record->delete();
    }

    public function clear(Record $record): void
    {
        MinioService::delete($record->files ?? []);

        $record->files = [];
        $record->save();

        $this->processImportService->deleteAllByRecord($record);
    }

    public function getDataFromRecord(Record $record): EloquentCollection
    {
        $recordImport = $record->imports->first();

        return $this->pluginService
            ->getPluginService($recordImport->process->plugin)
            ->getDataFromRecord($record);
    }

    public function getRecordsByJournal(Journal $journal): EloquentCollection
    {
        return Record::whereHas('journal', fn (Builder $builder) => $builder->where('id', $journal->id)
            ->where('is_active', ActiveStatusEnum::Active->value))
            ->where('is_active', ActiveStatusEnum::Active->value)->orderByDesc('date')->orderByDesc('order')->get();
    }

    public function addFileToRecord(Record $record, UploadedFile $file): bool
    {
        return $record->fill([
            'files' => collect($record->files)->push($this->fileService->createFromUploadedFile($file)->id)->unique()->all(),
        ])->save();
    }

    public function getRecordsFiles(Record $record): EloquentCollection
    {
        return EloquentCollection::make($record->files)
            ->map(fn (int $fileId) => $this->fileService->getById($fileId))->values();
    }

    public function removeFileFromRecord(Record $record, File $file): bool
    {
        return $record->fill([
            'files' => collect($record->files)
                ->filter(fn (int $fileId) => ($fileId !== $file->id) || ! $this->fileService->delete($file))
                ->all(),
        ])->save();
    }

    public function getImportersByRecord(Record $record): EloquentCollection
    {
        $processId = $record->imports->first()?->process_id;
        $processes = $this->processService->getAllByFilters(new ProcessFiltersDto(
            appliances: [$record->appliance->id],
            types: [ProcessTypeEnum::Importer->value]
        ));

        // @phpstan-ignore-next-line
        return $processId ? $processes->filter(fn (Process $process) => $process->id === $processId) : $processes;
    }

    public function getAllByFilters(RecordFilterDto $dto): EloquentCollection
    {
        $data = Record::active()
            ->when($dto->search && strlen($dto->search) > 1, function (Builder $builder) use ($dto) {
                $builder->where(fn (Builder $query) => $query
                    ->orWhere('name', 'ilike', '%'.$dto->search.'%')
                    ->orWhere('description', 'ilike', '%'.$dto->search.'%')
                );
            })
            ->when($dto->ids, function (Builder $builder) use ($dto) {
                $builder->whereIn('id', $dto->ids);
            });

        return $dto->page ? $data->paginate(
            perPage: $dto->perPage,
            page: $dto->page
        ) : $data->get();
    }

    public function updateStartDate(Record $record, ?Carbon $date): bool
    {
        return $record->fill(['date' => $date])->save();
    }

    public function updateEndDate(Record $record, ?Carbon $date): bool
    {
        if ($date !== null) {
            assert($date->gte($record->date), 'Date end must be after date');
        }

        return $record->fill(['date_end' => $date])->save();
    }
}
