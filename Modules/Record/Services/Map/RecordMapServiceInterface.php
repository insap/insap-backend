<?php

declare(strict_types=1);

namespace Modules\Record\Services\Map;

use Modules\Record\Models\Record;

interface RecordMapServiceInterface
{
    public const int MAX_MARKERS_COUNT = 40;

    public function getForMap(Record $record, string $latitudeColumnName = 'latitude', string $longitudeColumnName = 'longitude'): array;

    public function isAvailableMapForRecord(Record $record): bool;
}
