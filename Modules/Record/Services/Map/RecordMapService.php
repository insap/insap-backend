<?php

declare(strict_types=1);

namespace Modules\Record\Services\Map;

use Illuminate\Support\Collection;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Record\Models\Record;

class RecordMapService implements RecordMapServiceInterface
{
    public function __construct(private readonly PluginServiceInterface $pluginService)
    {
    }

    public function getForMap(Record $record, string $latitudeColumnName = 'latitude', string $longitudeColumnName = 'longitude'): array
    {
        $imports = $record->imports;

        $result = [];
        $import = $imports->first();

        $recordData = $this->pluginService->getPluginService($import->process->plugin)->getDataFromRecord($record);

        throw_if(! $this->isAvailableMapForRecord($record, $recordData->first()->toArray(), $latitudeColumnName), new \RuntimeException("Map is not available for record '{$record->id}'"));

        $stepIdField = $this->pluginService->getPluginService($import->process->plugin)->getStepIdField();
        $recordImportField = $this->pluginService->getPluginService($import->process->plugin)->getRecordImportIdField();

        $recordData = $recordData->groupBy($recordImportField);

        foreach ($imports as $import) {
            /** @var Collection $values */
            $values = $recordData->get($import->id);

            $steps = $this->getSteps($stepIdField, $values);

            $result[$import->id] = $values
                ->filter(fn (mixed $item) => in_array($item->{$stepIdField}, $steps, true))
                ->values()
                ->map(fn (mixed $item, $key) => collect($item)->only([$latitudeColumnName, $longitudeColumnName, $stepIdField])->merge([
                    'step_id' => $key + 1,
                ]))
                ->all();
        }

        return $result;
    }

    public function isAvailableMapForRecord(Record $record, array $recordDataArray = [], string $latitudeColumnName = 'latitude'): bool
    {
        return $record->imports->count() > 0 && isset($recordDataArray[$latitudeColumnName]);
    }

    private function getSteps(string $stepIdField, Collection $values): array
    {
        $result = [];
        $maxRange = $values->max($stepIdField);
        $minRange = $values->min($stepIdField);

        $step = ($maxRange - $minRange) / RecordMapServiceInterface::MAX_MARKERS_COUNT;

        for ($i = 0; $i <= $maxRange; $i += $step) {
            $result[] = (int) ($minRange + $i);
        }

        return $result;
    }
}
