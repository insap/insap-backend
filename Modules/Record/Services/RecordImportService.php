<?php

declare(strict_types=1);

namespace Modules\Record\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\File\Services\Minio\MinioService;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Record\DTO\RecordImportDto;
use Modules\Record\Enums\RecordImportStatusEnum;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordImport;

class RecordImportService implements RecordImportServiceInterface
{
    public function __construct(
        private readonly PluginServiceInterface $pluginService
    ) {
    }

    public function findById(int $id): ?RecordImport
    {
        return RecordImport::whereId($id)->first();
    }

    public function getImportsByRecordId(int $recordId): Collection
    {
        return RecordImport::whereRecordId($recordId)->get();
    }

    public function create(RecordImportDto $dto): RecordImport
    {
        /** @var \Modules\Record\Models\Record|null $lastModel */
        $lastModel = $this->getImportsByRecordId($dto->recordId)->last();

        $result = array_merge($dto->toArray(), [
            'order' => $lastModel ? $lastModel->order + 1 : 1,
        ]);

        return RecordImport::create($result);
    }

    public function deleteAllByRecord(Record $record): void
    {
        $record->imports()->each(fn (RecordImport $import) => $this->delete($import));
    }

    public function delete(RecordImport $import): bool
    {
        $this->clearImport($import);

        return $import->delete();
    }

    public function clearImport(RecordImport $import, bool $clearStatus = true): void
    {
        MinioService::delete($import->files ?? []);

        if ($clearStatus) {
            $import->status = RecordImportStatusEnum::Initial->value;
        }

        $import->files = [];
        $import->params = [];

        $record = $import->record;

        $pluginService = $this->pluginService->getPluginService($import->process->plugin);
        $pluginService->deleteDataByImportFromRecord($record, $import);
    }

    public function getNextOrder(?Record $record = null): int
    {
        $result = (RecordImport::query()->orderByDesc('id')->first()->id ?? 0) + 1;

        if ($record) {
            $result = $record->imports()->count() + 1;
        }

        return $result;
    }
}
