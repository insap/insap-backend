<?php

namespace Modules\Record\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Record\Services\Map\RecordMapService;
use Modules\Record\Services\Map\RecordMapServiceInterface;
use Modules\Record\Services\RecordImportService;
use Modules\Record\Services\RecordImportServiceInterface;
use Modules\Record\Services\RecordService;
use Modules\Record\Services\RecordServiceInterface;

class RecordServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    protected $moduleName = 'Record';

    /**
     * @var string
     */
    protected $moduleNameLower = 'record';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->bind(RecordServiceInterface::class, RecordService::class);
        $this->app->bind(RecordImportServiceInterface::class, RecordImportService::class);
        $this->app->bind(RecordMapServiceInterface::class, RecordMapService::class);
    }

    /**
     * Register translations.
     */
    public function registerTranslations(): void
    {
        $langPath = resource_path('lang/modules/'.$this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    public function registerViews(): void
    {
        $viewPath = resource_path('views/modules/'.$this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath,
        ], ['views', $this->moduleNameLower.'-module-views']);

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Get the services provided by the provider.
     */
    public function provides(): array
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path.'/modules/'.$this->moduleNameLower)) {
                $paths[] = $path.'/modules/'.$this->moduleNameLower;
            }
        }

        return $paths;
    }
}
