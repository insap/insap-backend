<?php

namespace Modules\Record\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Process\Enums\ProcessUseTypeEnum;
use Modules\Process\Http\Resources\ExternalProcessResource;
use Modules\Process\Models\Process;
use Modules\Process\Services\ProcessCompatibleServiceInterface;
use Modules\Record\Services\RecordServiceInterface;

/**
 * @mixin \Modules\Record\Models\Record
 */
class ExternalRecordResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        $processCompatibleService = app(ProcessCompatibleServiceInterface::class);
        $recordService = app(RecordServiceInterface::class);

        $exporters = [];
        $resource = $this->resource;

        if ($this->imports->count()) {
            $process = $recordService->getImportersByRecord($this->resource)->first();
            $exporters = $processCompatibleService->getAllExportersByImporter($process)
                // @phpstan-ignore-next-line
                ->filter(fn (Process $process) => in_array($process->use_type, ProcessUseTypeEnum::getArrayForExternalUse(), true))
                // @phpstan-ignore-next-line
                ->map(function (Process $process) use ($resource) {
                    // @phpstan-ignore-next-line
                    $process->extra_record = $resource;

                    return $process;
                });
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'date' => $this->date?->format('d.m.Y'),
            'date_end' => $this->date_end?->format('d.m.Y'),

            'image' => $this->image,
            'available_exporters' => ExternalProcessResource::collection($exporters),
        ];
    }
}
