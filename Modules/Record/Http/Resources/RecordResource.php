<?php

namespace Modules\Record\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Journal\Http\Resources\JournalResource;
use Modules\User\Http\Resources\UserResource;

/** @mixin \Modules\Record\Models\Record */
class RecordResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'order' => $this->order,
            'is_active' => $this->is_active,
            'date' => $this->date?->format('d.m.Y'),
            'date_end' => $this->date_end?->format('d.m.Y'),

            'image' => $this->image,

            'files' => $this->files,

            'user' => UserResource::make($this->whenLoaded('user')),
            'journal' => JournalResource::make($this->whenLoaded('journal')),

            'appliance' => UserResource::make($this->whenLoaded('appliance')),

            'imports' => RecordImportResource::collection($this->whenLoaded('imports')),

            'default_importer_id' => $this->default_importer_id,
            'default_exporter_id' => $this->default_exporter_id,

            'created_at' => $this->created_at->format('d.m.Y H:i:s'),
            'updated_at' => $this->created_at->format('d.m.Y H:i:s'),
        ];
    }
}
