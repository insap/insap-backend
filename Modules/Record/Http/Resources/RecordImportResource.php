<?php

namespace Modules\Record\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\File\Services\Minio\MinioService;
use Modules\Process\Http\Resources\ProcessResource;
use Modules\Record\Enums\RecordImportStatusEnum;
use Modules\User\Http\Resources\UserResource;

/** @mixin \Modules\Record\Models\RecordImport */
class RecordImportResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'order' => $this->order,
            'files' => MinioService::toJson($this->files ?? []),
            'params' => $this->params,
            'status' => $this->status,
            'status_text' => RecordImportStatusEnum::labels()[$this->status],
            'log' => $this->log,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'process_id' => $this->process_id,

            'process' => new ProcessResource($this->whenLoaded('process')),
            'record' => new RecordResource($this->whenLoaded('record')),
            'user' => new UserResource($this->whenLoaded('user')),
        ];
    }
}
