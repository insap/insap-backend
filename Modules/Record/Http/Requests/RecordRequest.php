<?php

namespace Modules\Record\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'description' => ['nullable'],
            'order' => ['nullable', 'integer'],
            'is_active' => ['required', 'integer'],
            'date' => ['nullable', 'date'],
            'image_id' => ['nullable', 'integer'],
            'default_importer_id' => ['nullable', 'integer'],
            'user_id' => ['required', 'integer'],
            'journal_id' => ['required'],
            'files' => ['required', 'date'],
            'default_exporter_id' => ['nullable'],
        ];
    }
}
