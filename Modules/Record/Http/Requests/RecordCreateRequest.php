<?php

namespace Modules\Record\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Appliance\Models\Appliance;

class RecordCreateRequest extends CustomFormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:64',
            'description' => 'nullable|string',
            'date' => 'nullable|date_format:Y-m-d',
            'date_end' => 'sometimes|date_format:Y-m-d|gte:date',
            'appliance_id' => 'required|int|exists:'.Appliance::TABLE.',id',

            'journal_id' => 'required|int|exists:journals,id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
