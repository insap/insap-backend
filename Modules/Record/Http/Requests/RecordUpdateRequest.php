<?php

declare(strict_types=1);

namespace Modules\Record\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class RecordUpdateRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'record_id' => 'required|integer|exists:records,id',

            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'date' => 'nullable|string|date_format:d.m.Y',
            'date_end' => 'sometimes|nullable|date_format:d.m.Y',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
