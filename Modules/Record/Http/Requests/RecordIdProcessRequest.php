<?php

declare(strict_types=1);

namespace Modules\Record\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;

class RecordIdProcessRequest extends CustomFormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'record_id' => 'required|integer|exists:'.Record::TABLE.',id',
            'process_id' => 'required|integer|exists:'.Process::TABLE.',id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
