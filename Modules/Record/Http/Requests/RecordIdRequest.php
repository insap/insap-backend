<?php

declare(strict_types=1);

namespace Modules\Record\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Record\Models\Record;

class RecordIdRequest extends CustomFormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'record_id' => 'required|integer|exists:'.Record::TABLE.',id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
