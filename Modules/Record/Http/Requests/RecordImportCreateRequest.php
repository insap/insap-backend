<?php

namespace Modules\Record\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecordImportCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'files' => ['required'],
            'params' => ['required'],
            'user_id' => ['nullable', 'exists:users'],
            'record_id' => ['required', 'exists:records'],
            'process_id' => ['required', 'exists:processes'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
