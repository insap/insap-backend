<?php

namespace Modules\Record\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;

class ExternalExportMultiplyRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'records' => 'required|array',
            'records.*' => 'int|exists:'.Record::TABLE.',id',
            'process_id' => ['required', 'int', 'exists:'.Process::TABLE.',id'],

            'fields' => 'required|array',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
