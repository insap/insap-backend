<?php

declare(strict_types=1);

namespace Modules\Record\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\File\Entities\File;
use Modules\Record\Models\Record;

class RecordIdFileRemoveRequest extends CustomFormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'record_id' => 'required|integer|exists:'.Record::TABLE.',id',
            'archive_id' => 'required|integer|exists:'.File::TABLE.',id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
