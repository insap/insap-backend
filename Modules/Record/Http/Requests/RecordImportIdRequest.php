<?php

declare(strict_types=1);

namespace Modules\Record\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Record\Models\RecordImport;

class RecordImportIdRequest extends CustomFormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'record_import_id' => 'required|integer|exists:'.RecordImport::TABLE.',id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
