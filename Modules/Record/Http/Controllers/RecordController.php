<?php

declare(strict_types=1);

namespace Modules\Record\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Carbon\Carbon;
use Modules\File\Http\Resources\FileResource;
use Modules\File\Services\FileServiceInterface;
use Modules\Journal\Http\Requests\JournalGetByIdRequest;
use Modules\Journal\Services\JournalServiceInterface;
use Modules\Process\Http\Resources\ProcessResource;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\Record\DTO\RecordCreateDto;
use Modules\Record\DTO\RecordUpdateDto;
use Modules\Record\Http\Requests\RecordCreateRequest;
use Modules\Record\Http\Requests\RecordIdFileRemoveRequest;
use Modules\Record\Http\Requests\RecordIdFileRequest;
use Modules\Record\Http\Requests\RecordIdProcessRequest;
use Modules\Record\Http\Requests\RecordIdRequest;
use Modules\Record\Http\Requests\RecordUpdateRequest;
use Modules\Record\Http\Resources\RecordResource;
use Modules\Record\Services\RecordServiceInterface;

class RecordController extends Controller
{
    public function __construct(
        private readonly JournalServiceInterface $journalService,
        private readonly RecordServiceInterface $recordService,
        private readonly FileServiceInterface $fileService,
        private readonly ProcessServiceInterface $processService,
        private readonly ProcessFieldServiceInterface $processFieldService
    ) {
    }

    public function getRecordsByJournal(JournalGetByIdRequest $request): Response
    {
        $response = Response::make();

        $journal = $this->journalService->findJournalById((int) $request->input('journal_id'));

        $records = $this->recordService->getRecordsByJournal($journal);
        $records->load(['user', 'appliance']);

        return $response->withData(RecordResource::collection($records));
    }

    public function create(RecordCreateRequest $request): Response
    {
        $response = Response::make();

        $dto = RecordCreateDto::fromRequest($request);
        $record = $this->recordService->create($dto);

        return $response->withData($record);
    }

    public function update(RecordUpdateRequest $request): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById($request->input('record_id'));
        try {
            $this->recordService->update(
                $record,
                RecordUpdateDto::fromRequest($request)
            );

            if ($request->has('date')) {
                $this->recordService->updateStartDate($record, $request->date('date'));
            }

            if ($request->has('date_end')) {
                $this->recordService->updateEndDate($record, $request->date('date_end'));
            }

        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function clearRecord(RecordIdRequest $request): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById($request->input('record_id'));

        try {
            $this->recordService->clear($record);
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function getById(RecordIdRequest $request): Response
    {
        $response = Response::make();

        $data = $this->recordService->getById((int) $request->input('record_id'));
        $data->load(['user', 'imports', 'journal']);

        return $response->withData(RecordResource::make($data));
    }

    public function getAllFiles(RecordIdRequest $request): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById((int) $request->input('record_id'));

        $files = $this->recordService->getRecordsFiles($record);
        $files->load('user');

        return $response->withData(FileResource::collection($files));
    }

    public function addLocalArchive(RecordIdFileRequest $request): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById((int) $request->input('record_id'));

        try {
            $this->recordService->addFileToRecord($record, $request->file('archive'));
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function removeLocalArchive(RecordIdFileRemoveRequest $request): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById((int) $request->input('record_id'));
        $file = $this->fileService->getById((int) $request->input('archive_id'));

        try {
            $this->recordService->removeFileFromRecord($record, $file);
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function getFieldsCalculatedValues(RecordIdProcessRequest $request): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById((int) $request->input('record_id'));
        $process = $this->processService->getById((int) $request->input('process_id'));

        $data = $this->processFieldService->getCalculatedValues($record, $process);

        return $response->withData($data);
    }

    public function getProcessesByRecord(RecordIdRequest $request): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById((int) $request->input('record_id'));

        $data = $this->recordService->getImportersByRecord($record);

        return $response->withData(ProcessResource::collection($data));
    }
}
