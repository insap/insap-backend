<?php

namespace Modules\Record\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Record\Http\Requests\RecordIdRequest;
use Modules\Record\Services\Map\RecordMapServiceInterface;
use Modules\Record\Services\RecordServiceInterface;

class RecordMapController extends Controller
{
    public function __construct(
        private readonly RecordServiceInterface $recordService,
        private readonly RecordMapServiceInterface $recordMapService
    ) {
    }

    public function __invoke(RecordIdRequest $request)
    {
        $response = Response::make();

        $recordId = (int) $request->input('record_id');
        $record = $this->recordService->getById($recordId);

        try {
            $data = $this->recordMapService->getForMap($record);
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->withData($data)->response();
    }
}
