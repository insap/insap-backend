<?php

namespace Modules\Record\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Process\Services\ExporterExecuteService;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\Record\DTO\RecordFilterDto;
use Modules\Record\Http\Requests\ExternalExportMultiplyRequest;
use Modules\Record\Http\Requests\ExternalExportRequest;
use Modules\Record\Services\RecordServiceInterface;

class ExternalRecordController extends Controller
{
    public function __construct(
        private readonly RecordServiceInterface $recordService,
        private readonly ProcessServiceInterface $processService,
        private readonly ProcessFieldServiceInterface $processFieldService,
        private readonly ExporterExecuteService $exporterExecuteService
    ) {
    }

    public function export(ExternalExportRequest $request)
    {
        $record = $this->recordService->getById((int) $request->input('record_id'));
        $process = $this->processService->getById((int) $request->input('process_id'));

        try {
            $fields = $this->processFieldService->filterFieldsDto(
                fields: $this->processFieldService->buildFieldsDtoFromRequest($request->input('fields'))
            );

            $dto = $this->exporterExecuteService->executeProcess($process, $record, $fields->all(), []);
        } catch (\Throwable $throwable) {
            return Response::make()->catch($throwable);
        }

        return $this->exporterExecuteService->formatResponse($dto, $record, $process);
    }

    public function exportMultiply(ExternalExportMultiplyRequest $request)
    {
        $records = $this->recordService->getAllByFilters(new RecordFilterDto(ids: $request->input('records')));
        $process = $this->processService->getById((int) $request->input('process_id'));

        try {
            $fields = $this->processFieldService->filterFieldsDto(
                fields: $this->processFieldService->buildFieldsDtoFromRequest($request->input('fields'))
            );

            $dto = $this->exporterExecuteService->executeProcessMultiply($process, $records, $fields->all());
        } catch (\Throwable $throwable) {
            return Response::make()->catch($throwable);
        }

        return $this->exporterExecuteService->formatResponse($dto, $records->first(), $process);
    }
}
