<?php

declare(strict_types=1);

namespace Modules\Record\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Process\Services\ExporterExecuteService;
use Modules\Process\Services\ImporterExecuteService;
use Modules\Process\Services\ProcessFieldServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\Record\DTO\RecordImportDto;
use Modules\Record\Enums\RecordImportStatusEnum;
use Modules\Record\Http\Requests\RecordIdRequest;
use Modules\Record\Http\Requests\RecordImportCreateRequest;
use Modules\Record\Http\Requests\RecordImportIdRequest;
use Modules\Record\Http\Requests\RecordImportRequest;
use Modules\Record\Http\Resources\RecordImportResource;
use Modules\Record\Services\RecordImportServiceInterface;
use Modules\Record\Services\RecordServiceInterface;

class RecordImportController extends Controller
{
    public function __construct(
        private readonly RecordServiceInterface $recordService,
        private readonly RecordImportServiceInterface $recordImportService,
        private readonly ProcessServiceInterface $processService,
        private readonly ProcessFieldServiceInterface $processFieldService,
    ) {
    }

    public function create(RecordImportCreateRequest $request): Response
    {
        $response = Response::make();

        try {
            $dto = new RecordImportDto(
                order: $this->recordImportService->getNextOrder(),
                importStatus: RecordImportStatusEnum::Initial,
                processId: $request->input('process_id'),
                recordId: $request->input('record_id'),
                importLog: '',
                userId: $request->user()->id
            );

            $recordImport = $this->recordImportService->create($dto);
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->withData($recordImport);
    }

    public function clear(RecordImportIdRequest $request): Response
    {
        $response = Response::make();

        $recordImport = $this->recordImportService->findById($request->input('record_import_id'));
        $this->recordImportService->clearImport($recordImport);

        return $response->success();
    }

    public function delete(RecordImportIdRequest $request): Response
    {
        $response = Response::make();

        $recordImport = $this->recordImportService->findById($request->input('record_import_id'));
        $this->recordImportService->delete($recordImport);

        cache()->flush();
        return $response->success();
    }

    public function getByRecord(RecordIdRequest $request): Response
    {
        $response = Response::make();

        $data = $this->recordImportService->getImportsByRecordId($request->input('record_id'));
        $data->load(['user']);

        return $response->withData(RecordImportResource::collection($data));
    }

    public function createAndImport(RecordImportRequest $request, ImporterExecuteService $importerService): Response
    {
        $response = Response::make();

        $record = $this->recordService->getById((int) $request->input('record_id'));
        $process = $this->processService->getById((int) $request->input('process_id'));

        try {
            $recordImport = $this->recordImportService->create(
                new RecordImportDto(
                    order: $this->recordImportService->getNextOrder($record),
                    importStatus: RecordImportStatusEnum::Initial,
                    processId: $process->id,
                    recordId: $record->id,
                    userId: (int) $request->user()->id
                )
            );

            $fields = $this->processFieldService->buildFieldsDtoFromRequest($request->input('fields'));
            $files = $this->processFieldService->makeFileDtoFromFieldsDto($fields, collect($request->allFiles()));
            // Обязательно после $files
            $fields = $this->processFieldService->filterFieldsDto($fields);

            $importerService->executeProcess($recordImport, $fields->all(), $files->all());
            cache()->flush();
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function export(RecordImportRequest $request, ExporterExecuteService $exporterExecuteService)
    {
        $record = $this->recordService->getById((int) $request->input('record_id'));
        $process = $this->processService->getById((int) $request->input('process_id'));

        try {
            $fields = $this->processFieldService->filterFieldsDto(
                fields: $this->processFieldService->buildFieldsDtoFromRequest($request->input('fields'))
            );

            $dto = $exporterExecuteService->executeProcess($process, $record, $fields->all(), []);
        } catch (\Throwable $throwable) {
            return Response::make()->catch($throwable);
        }

        return $exporterExecuteService->formatResponse($dto, $record, $process);
    }
}
