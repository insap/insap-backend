<?php

declare(strict_types=1);

namespace Modules\Option\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Modules\Option\Enums\OptionTypeEnum;

class OptionCreateDto implements Arrayable
{
    public function __construct(
        public string|array $name,
        public OptionTypeEnum $optionTypeEnum,
        public string $icon
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'type' => $this->optionTypeEnum->value,
            'icon' => $this->icon,
        ];
    }
}
