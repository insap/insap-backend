<?php

namespace Modules\Option\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Option\Enums\OptionTypeEnum;
use Modules\Option\Http\Requests\OptionTypeRequest;
use Modules\Option\Services\OptionServiceInterface;

class OptionController extends Controller
{
    public function __construct(
        private readonly OptionServiceInterface $optionService
    ) {
    }

    public function getAllByType(OptionTypeRequest $request): Response
    {
        $response = Response::make();

        $optionType = $request->enum($request->input('option_type'), OptionTypeEnum::class);
        $data = $this->optionService->getOptionsByType($optionType);

        return $response->withData($data);
    }
}
