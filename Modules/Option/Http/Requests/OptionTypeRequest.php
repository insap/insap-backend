<?php

namespace Modules\Option\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Modules\Option\Enums\OptionTypeEnum;

class OptionTypeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'option_type' => ['required', 'string', Rule::enum(OptionTypeEnum::class)],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
