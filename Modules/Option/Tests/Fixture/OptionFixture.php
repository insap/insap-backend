<?php

declare(strict_types=1);

namespace Modules\Option\Tests\Fixture;

use Illuminate\Foundation\Testing\WithFaker;
use Modules\Option\DTO\OptionCreateDto;
use Modules\Option\Enums\OptionTypeEnum;
use Modules\Option\Models\Option;
use Modules\Option\Services\OptionServiceInterface;

class OptionFixture
{
    use WithFaker;

    public function __construct(private readonly OptionServiceInterface $optionService)
    {
        $this->setUpFaker();
    }

    public function create(?OptionTypeEnum $optionTypeEnum = null): Option
    {
        $word = $this->faker->word;

        $dto = new OptionCreateDto(
            name: ['ru' => $word, ['en' => $word]],
            optionTypeEnum: $optionTypeEnum ?? OptionTypeEnum::Process,
            icon: $word
        );

        return $this->optionService->create($dto);
    }
}
