<?php

namespace Modules\Option\Tests\Unit;

use Modules\Appliance\Tests\Fixture\ApplianceFixture;
use Modules\Option\DTO\OptionCreateDto;
use Modules\Option\Enums\OptionTypeEnum;
use Modules\Option\Services\OptionServiceInterface;
use Modules\Option\Tests\Fixture\OptionFixture;
use Tests\TestCase;

class OptionServiceTest extends TestCase
{
    private ?OptionServiceInterface $optionService;

    private ?OptionFixture $optionFixture;

    private ?ApplianceFixture $applianceFixture;

    public function setUp(): void
    {
        parent::setUp();

        $this->optionService = app(OptionServiceInterface::class);

        $this->optionFixture = app(OptionFixture::class);
        $this->applianceFixture = app(ApplianceFixture::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->optionService = null;

        $this->optionFixture = null;
        $this->applianceFixture = null;
    }

    public function testCreate(): void
    {
        $dto = new OptionCreateDto(
            name: ['ru' => 'Содержит координаты', ['en' => 'Work with coordinates']],
            optionTypeEnum: OptionTypeEnum::Process,
            icon: 'mdi-menu'
        );

        $option = $this->optionService->create($dto);

        $this->assertSame($dto->name[app()->currentLocale()], $option->name);
        $this->assertSame($dto->optionTypeEnum, $option->type);
        $this->assertSame($dto->icon, $option->icon);
    }

    public function testGetOptionsByType(): void
    {
        $option = $this->optionFixture->create(OptionTypeEnum::Process);

        $this->optionFixture->create(OptionTypeEnum::Appliance);

        $options = $this->optionService->getOptionsByType(OptionTypeEnum::Process);

        $this->assertCount(1, $options);

        $this->assertSame($options->first()->id, $option->id);
    }

    public function testCreateOptionForProcess()
    {
        $option = $this->optionFixture->create(OptionTypeEnum::Process);
        $appliance = $this->applianceFixture->create();

        $appliance->features()->save($option);

        $this->assertCount(1, $appliance->features);

    }
}
