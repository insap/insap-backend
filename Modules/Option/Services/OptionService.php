<?php

declare(strict_types=1);

namespace Modules\Option\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Option\DTO\OptionCreateDto;
use Modules\Option\Enums\OptionTypeEnum;
use Modules\Option\Models\Option;

class OptionService implements OptionServiceInterface
{
    public function create(OptionCreateDto $dto): Option
    {
        return Option::create($dto->toArray());
    }

    public function getOptionsByType(OptionTypeEnum $optionTypeEnum): Collection
    {
        return Option::whereType($optionTypeEnum->value)->get();
    }
}
