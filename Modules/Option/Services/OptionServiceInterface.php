<?php

declare(strict_types=1);

namespace Modules\Option\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Option\DTO\OptionCreateDto;
use Modules\Option\Enums\OptionTypeEnum;
use Modules\Option\Models\Option;

interface OptionServiceInterface
{
    public function create(OptionCreateDto $dto): Option;

    /** @return Collection<int, Option> */
    public function getOptionsByType(OptionTypeEnum $optionTypeEnum): Collection;
}
