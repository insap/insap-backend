<?php

declare(strict_types=1);

namespace Modules\Option\Enums;

enum OptionTypeEnum: string
{
    case Process = 'process';
    case Appliance = 'appliance';
}
