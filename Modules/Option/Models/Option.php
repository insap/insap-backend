<?php

namespace Modules\Option\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Option\Enums\OptionTypeEnum;
use Spatie\Sluggable\HasTranslatableSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\Translatable\HasTranslations;

/**
 * Modules\Option\Models\Option
 *
 * @property OptionTypeEnum $type
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Option newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Option newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Option query()
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereLocale(string $column, string $locale)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereLocales(string $column, array $locales)
 *
 * @property int $id
 * @property array $name
 * @property string $icon
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Option whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class Option extends Model
{
    use HasTranslatableSlug, HasTranslations;

    public const TABLE = 'options';

    public const OPTIONAL_TABLE = 'optionals';

    protected $table = self::TABLE;

    public array $translatable = [
        'name',
        'slug',
    ];

    protected $fillable = [
        'name',
        'icon',
        'type',
        'slug',
    ];

    protected $casts = [
        'type' => OptionTypeEnum::class,
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->slugsShouldBeNoLongerThan(config('app.max_slug_length'))
            ->doNotGenerateSlugsOnUpdate()
            ->generateSlugsFrom(['name'])
            ->saveSlugsTo('slug');
    }
}
