<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Option\Http\Controllers\OptionController;

Route::prefix('options')->middleware('auth:sanctum')->name('options.')->group(function () {
    Route::post('get-all-by-type', [OptionController::class, 'getAllByType']);
    Route::post('get-all-by-process', [OptionController::class, 'getAllByProcess']);
});
