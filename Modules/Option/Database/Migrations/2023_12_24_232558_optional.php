<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Option\Models\Option;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Option::OPTIONAL_TABLE, function (Blueprint $table) {
            $table->foreignId('option_id')->index()->references('id')->on(Option::TABLE)->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->morphs('optional');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('');
    }
};
