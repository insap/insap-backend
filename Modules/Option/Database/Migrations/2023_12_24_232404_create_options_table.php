<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Option\Models\Option;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Option::TABLE, function (Blueprint $table) {
            $table->id();

            $table->json('name');
            $table->string('icon')->default('');

            $table->string('type')->index();

            $table->json('slug');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Option::TABLE);
    }
};
