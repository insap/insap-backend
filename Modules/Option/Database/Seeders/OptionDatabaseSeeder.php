<?php

namespace Modules\Option\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Option\DTO\OptionCreateDto;
use Modules\Option\Enums\OptionTypeEnum;
use Modules\Option\Services\OptionServiceInterface;

class OptionDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(OptionServiceInterface $optionService)
    {
        Model::unguard();

        $dto = new OptionCreateDto(
            name: ['ru' => 'Содержит координаты', ['en' => 'Work with coordinates']],
            optionTypeEnum: OptionTypeEnum::Process,
            icon: 'mdi-menu'
        );

        $optionService->create($dto);
    }
}
