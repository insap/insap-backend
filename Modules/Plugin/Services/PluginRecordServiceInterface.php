<?php

declare(strict_types=1);

namespace Modules\Plugin\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordImport;

interface PluginRecordServiceInterface
{
    public function getDataFromRecord(Record $record): Collection;

    public function getSingleColumnFromRecord(Record $record, string $column): \Illuminate\Support\Collection;

    public function addDataToDatabase(RecordImport $processImport, ProcessParamsDto $paramsDto, Process $process): void;

    public function deleteDataFromRecord(Record $record): int;

    public function deleteDataByImportFromRecord(Record $record, RecordImport $recordImport): int;

    public function getStepIdField(): string;

    public function getRecordImportIdField(): string;
}
