<?php

declare(strict_types=1);

namespace Modules\Plugin\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Plugin\Enums\PluginCreateDto;
use Modules\Plugin\Models\Plugin;

class PluginService implements PluginServiceInterface
{
    public function create(PluginCreateDto $dto): Plugin
    {
        return Plugin::create($dto->toArray());
    }

    public function all(): Collection
    {
        return Plugin::active()->get();
    }

    public function getPluginService(?Plugin $plugin = null): PluginRecordServiceInterface
    {
        $class = $plugin->service_class ?? DefaultPluginRecordService::class;
        $service = app($class);

        if (($service instanceof PluginRecordServiceInterface) === false) {
            throw new \RuntimeException('Plugin class '.$class.' must be instance of PluginRecordServiceInterface');
        }

        return $service;
    }

    public function findById(int $id): ?Plugin
    {
        return Plugin::whereId($id)->first();
    }
}
