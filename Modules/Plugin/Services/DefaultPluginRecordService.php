<?php

declare(strict_types=1);

namespace Modules\Plugin\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Enums\ProcessOptionEnum;
use Modules\Process\Models\Process;
use Modules\Record\Models\Record;
use Modules\Record\Models\RecordData;
use Modules\Record\Models\RecordImport;

class DefaultPluginRecordService implements PluginRecordServiceInterface
{
    public function getDataFromRecord(Record $record): Collection
    {
        return RecordData::where(Record::RECORD_ID_FIELD, $record->id)->orderBy(Record::STEP_ID_FIELD)->get();
    }

    public function addDataToDatabase(RecordImport $processImport, ProcessParamsDto $paramsDto, Process $process): void
    {
        $record = $processImport->record;

        if ($process->getOptionsByKey(ProcessOptionEnum::OverwriteDataOnMultiplyImport->value)) {
            $this->deleteDataFromRecord($record);
        }

        $data = $paramsDto->getData()->all();
        $chunk = [];

        $step = 1;
        foreach ($data as $array) {
            // Add record_id to each record
            $array[Record::RECORD_ID_FIELD] = $record->id;
            $array[Record::RECORD_IMPORT_ID_FIELD] = $processImport->id;
            $array[Record::STEP_ID_FIELD] = $step;

            $chunk[] = $array;
            if (count($chunk) === 1000) {
                RecordData::insert($chunk);
                $chunk = [];
            }

            $step++;
        }

        if (count($chunk)) {
            RecordData::insert($chunk);
        }
    }

    public function deleteDataFromRecord(Record $record): int
    {
        return (int) RecordData::where(Record::RECORD_ID_FIELD, $record->id)->delete();
    }

    public function deleteDataByImportFromRecord(Record $record, RecordImport $recordImport): int
    {
        return (int) RecordData::where(Record::RECORD_ID_FIELD, $record->id)->where(Record::RECORD_IMPORT_ID_FIELD, $recordImport->id)->delete();
    }

    public function getSingleColumnFromRecord(Record $record, string $column): \Illuminate\Support\Collection
    {
        return RecordData::distinct()->where(Record::RECORD_ID_FIELD, $record->id)->pluck($column, $column);
    }

    public function getStepIdField(): string
    {
        return Record::STEP_ID_FIELD;
    }

    public function getRecordImportIdField(): string
    {
        return Record::RECORD_IMPORT_ID_FIELD;
    }
}
