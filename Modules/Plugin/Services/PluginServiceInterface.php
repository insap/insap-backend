<?php

declare(strict_types=1);

namespace Modules\Plugin\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Plugin\Enums\PluginCreateDto;
use Modules\Plugin\Models\Plugin;

interface PluginServiceInterface
{
    public function create(PluginCreateDto $dto): Plugin;

    /** @return Collection<int, Plugin> */
    public function all(): Collection;

    public function findById(int $id): ?Plugin;

    public function getPluginService(?Plugin $plugin = null): PluginRecordServiceInterface;
}
