<?php

declare(strict_types=1);

namespace Modules\Plugin\Enums;

use App\Enums\ActiveStatusEnum;
use Illuminate\Contracts\Support\Arrayable;

class PluginCreateDto implements Arrayable
{
    public function __construct(
        public string $name,
        public string $slug,
        public array $settings,
        public string $serviceClass,
        public ActiveStatusEnum $activeStatusEnum
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'settings' => $this->settings,
            'service_class' => $this->serviceClass,
            'is_active' => $this->activeStatusEnum->value,
        ];
    }
}
