<?php

declare(strict_types=1);

namespace Modules\Plugin\Tests\Unit;

use App\Enums\ActiveStatusEnum;
use Modules\Plugin\Enums\PluginCreateDto;
use Modules\Plugin\Models\Plugin;
use Modules\Plugin\Services\DefaultPluginRecordService;
use Modules\Plugin\Services\PluginServiceInterface;
use Tests\TestCase;

class PluginServiceTest extends TestCase
{
    private ?PluginServiceInterface $pluginService;

    public function setUp(): void
    {
        parent::setUp();

        $this->pluginService = $this->app->make(PluginServiceInterface::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->pluginService = null;
    }

    public function testCreate(): void
    {
        $dto = new PluginCreateDto(
            name: $this->faker->word,
            slug: $this->faker->slug,
            settings: [],
            serviceClass: DefaultPluginRecordService::class,
            activeStatusEnum: ActiveStatusEnum::Active
        );

        $plugin = $this->pluginService->create($dto);

        $this->assertSame($dto->name, $plugin->name);
        $this->assertSame($dto->slug, $plugin->slug);
        $this->assertSame($dto->settings, $plugin->settings);
        $this->assertSame($dto->serviceClass, $plugin->service_class);
        $this->assertSame($dto->activeStatusEnum->value, $plugin->is_active);
    }

    public function testGetAll(): void
    {
        $dto = new PluginCreateDto(
            name: $this->faker->word,
            slug: $this->faker->slug,
            settings: [],
            serviceClass: DefaultPluginRecordService::class,
            activeStatusEnum: ActiveStatusEnum::Active
        );

        $plugin = $this->pluginService->create($dto);

        $result = $this->pluginService->all();

        $this->assertGreaterThan(0, $result->count());
        $this->assertNotNull($result->filter(fn (Plugin $pluginAll) => $plugin->id === $pluginAll->id));
    }

    public function testGetPluginService(): void
    {
        $dto = new PluginCreateDto(
            name: $this->faker->word,
            slug: $this->faker->slug,
            settings: [],
            serviceClass: DefaultPluginRecordService::class,
            activeStatusEnum: ActiveStatusEnum::Active
        );

        $plugin = $this->pluginService->create($dto);
        $result = $pluginService = $this->pluginService->getPluginService($plugin);

        $this->assertNotNull($result);
    }
}
