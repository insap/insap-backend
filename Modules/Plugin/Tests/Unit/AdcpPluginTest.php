<?php

declare(strict_types=1);

namespace Modules\Plugin\Tests\Unit;

use App\Enums\InsapServiceEnum;
use Carbon\Carbon;
use Modules\Appliance\Tests\Fixture\ApplianceFixture;
use Modules\Journal\Tests\Fixture\JournalFixture;
use Modules\Plugin\Services\PluginServiceInterface;
use Modules\Process\DTO\ProcessParamsDto;
use Modules\Process\Enums\ProcessInterpreterEnum;
use Modules\Process\Services\ExporterExecuteService;
use Modules\Process\Services\ImporterExecuteService;
use Modules\Process\Services\ProcessCompatibleServiceInterface;
use Modules\Process\Services\ProcessServiceInterface;
use Modules\Process\Tests\Fixture\ProcessFixture;
use Modules\Record\Models\RecordImport;
use Modules\Record\Services\RecordImportServiceInterface;
use Modules\Record\Services\RecordServiceInterface;
use Modules\Record\Tests\Fixture\RecordFixture;
use Modules\Record\Tests\Fixture\RecordImportFixture;
use Modules\User\Tests\Fixture\UserFixture;
use Storage;
use Tests\TestCase;

class AdcpPluginTest extends TestCase
{
    private ?ProcessServiceInterface $processService;

    private ?ProcessCompatibleServiceInterface $processCompatibleService;

    private ?ImporterExecuteService $importerExecuteService;

    private ?ExporterExecuteService $exporterExecuteService;

    private ?RecordServiceInterface $recordService;

    private ?RecordImportServiceInterface $recordImportService;

    private ?PluginServiceInterface $pluginService;

    private ?ProcessFixture $processFixture;

    private ?ApplianceFixture $applianceFixture;

    private ?RecordImportFixture $recordImportFixture;

    private ?UserFixture $userFixture;

    private ?JournalFixture $journalFixture;

    private ?RecordFixture $recordFixture;

    public function setUp(): void
    {
        parent::setUp();

        $this->processService = $this->app->make(ProcessServiceInterface::class);
        $this->processCompatibleService = $this->app->make(ProcessCompatibleServiceInterface::class);
        $this->importerExecuteService = $this->app->make(ImporterExecuteService::class);
        $this->exporterExecuteService = $this->app->make(ExporterExecuteService::class);
        $this->recordService = $this->app->make(RecordServiceInterface::class);
        $this->recordImportService = $this->app->make(RecordImportServiceInterface::class);
        $this->pluginService = $this->app->make(PluginServiceInterface::class);

        $this->processFixture = $this->app->make(ProcessFixture::class);
        $this->applianceFixture = $this->app->make(ApplianceFixture::class);
        $this->recordImportFixture = $this->app->make(RecordImportFixture::class);
        $this->journalFixture = $this->app->make(JournalFixture::class);
        $this->userFixture = $this->app->make(UserFixture::class);
        $this->recordFixture = $this->app->make(RecordFixture::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->processService = null;
        $this->processCompatibleService = null;

        $this->importerExecuteService = null;
        $this->exporterExecuteService = null;
        $this->pluginService = null;
        $this->recordImportService = null;

        $this->processFixture = null;
        $this->applianceFixture = null;
        $this->recordImportFixture = null;
        $this->journalFixture = null;
        $this->userFixture = null;
        $this->recordFixture = null;
    }

    public function testImportPython(): void
    {
        if (config('app.insap_service') !== InsapServiceEnum::Insap->value) {
            $this->markTestSkipped();
        }

        $storage = Storage::disk('examples');

        $appliance = $this->applianceFixture->create();
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();
        $record = $this->recordFixture->create($journal, $user, $appliance);
        $plugin = $this->pluginService->findById(1);

        $process = $this->processFixture->createWithInterpreter($appliance, ProcessInterpreterEnum::Python, 'importer-python-adcp.zip', $plugin);
        $recordImport = $this->recordImportFixture->create($record, $process);

        $dataFileDto = $this->processFixture->getProcessFileDto('data',
            $storage->path('adcp'.DIRECTORY_SEPARATOR.'1'.DIRECTORY_SEPARATOR.'1597842707_data.txt'),
            'text/plain');

        $refFileDto = $this->processFixture->getProcessFileDto('ref',
            $storage->path('adcp'.DIRECTORY_SEPARATOR.'1'.DIRECTORY_SEPARATOR.'1597842707_ref.txt'),
            'text/plain');

        $params = [
            'date_time' => Carbon::now()->format('Y-m-d H:i:s'),
            'expedition_number' => 1,
        ];

        $this->assertNotNull($process->plugin);

        $result = $this->importerExecuteService->executeProcess($recordImport, $params, [$dataFileDto, $refFileDto]);

        $this->assertTrue($result);
        $this->assertTrue($this->recordService->getDataFromRecord($record)->count() > 0);

        $this->recordService->clear($record);
        $this->processService->delete($process);
    }

    public function testExportPhp(): void
    {
        if (config('app.insap_service') !== InsapServiceEnum::Insap->value) {
            $this->markTestSkipped();
        }

        $storage = Storage::disk('examples');

        $appliance = $this->applianceFixture->create('ADCP');
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();
        $record = $this->recordFixture->create($journal, $user, $appliance);
        $plugin = $this->pluginService->findById(1);

        $process = $this->processFixture->createWithInterpreter($appliance, ProcessInterpreterEnum::Python, 'importer-python-adcp.zip', $plugin);
        $exportProcess = $this->processFixture->createWithInterpreter($appliance, ProcessInterpreterEnum::Php, 'exporter_php_adcp.zip', $plugin);

        $recordImport = $this->recordImportFixture->create($record, $process);

        $dataFileDto = $this->processFixture->getProcessFileDto('data',
            $storage->path('adcp'.DIRECTORY_SEPARATOR.'1'.DIRECTORY_SEPARATOR.'1597842707_data.txt'),
            'text/plain');

        $refFileDto = $this->processFixture->getProcessFileDto('ref',
            $storage->path('adcp'.DIRECTORY_SEPARATOR.'1'.DIRECTORY_SEPARATOR.'1597842707_ref.txt'),
            'text/plain');

        $params = [
            'date_time' => Carbon::now()->format('Y-m-d H:i:s'),
            'expedition_number' => 1,
        ];

        $this->importerExecuteService->executeProcess($recordImport, $params, [$dataFileDto, $refFileDto]);

        $params = [
            'speed' => 2,
            'average' => 50,
        ];

        $this->processCompatibleService->setDefaultImporter($exportProcess, $process);

        /** @var ProcessParamsDto $result */
        $result = $this->exporterExecuteService->executeProcess($exportProcess, $record, $params, []);

        $this->assertInstanceOf(ProcessParamsDto::class, $result);
        $this->assertSame($result->getFiles()->count(), 0);
        $this->assertTrue($result->getParams()->count() > 0);
        $this->assertTrue($result->getData()->count() > 0);

        $this->recordService->clear($record);
        $this->processService->delete($process);
        $this->processService->delete($exportProcess);
    }

    public function testMultiplyImportAdcp(): void
    {
        if (config('app.insap_service') !== InsapServiceEnum::Insap->value) {
            $this->markTestSkipped();
        }

        $storage = Storage::disk('examples');

        $appliance = $this->applianceFixture->create('ADCP');
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();
        $record = $this->recordFixture->create($journal, $user, $appliance);
        $plugin = $this->pluginService->findById(1);

        $process = $this->processFixture->createWithInterpreter($appliance, ProcessInterpreterEnum::Python, 'importer-python-adcp.zip', $plugin);
        $exportProcess = $this->processFixture->createWithInterpreter($appliance, ProcessInterpreterEnum::Php, 'exporter_php_adcp.zip', $plugin);

        $array = [
            [
                'data_file' => 'adcp'.DIRECTORY_SEPARATOR.'k2019_0708'.DIRECTORY_SEPARATOR.'1604059223_data.TXT',
                'ref_file' => 'adcp'.DIRECTORY_SEPARATOR.'k2019_0708'.DIRECTORY_SEPARATOR.'1604059223_ref.TXT',
                'date_time' => Carbon::now()->format('Y-m-d H:i:s'),
                'expedition_number' => 1,
            ],
            [
                'data_file' => 'adcp'.DIRECTORY_SEPARATOR.'k2019_0708'.DIRECTORY_SEPARATOR.'1604059281_data.TXT',
                'ref_file' => 'adcp'.DIRECTORY_SEPARATOR.'k2019_0708'.DIRECTORY_SEPARATOR.'1604059281_ref.TXT',
                'date_time' => Carbon::now()->copy()->addDay()->format('Y-m-d H:i:s'),
                'expedition_number' => 2,
            ],
        ];

        foreach ($array as $item) {
            $dataFileDto = $this->processFixture->getProcessFileDto('data',
                $storage->path($item['data_file']),
                'text/plain');

            $refFileDto = $this->processFixture->getProcessFileDto('ref',
                $storage->path($item['ref_file']),
                'text/plain');

            $params = [
                'date_time' => $item['date_time'],
                'expedition_number' => $item['expedition_number'],
            ];

            $recordImport = $this->recordImportFixture->create($record, $process);
            $result = $this->importerExecuteService->executeProcess($recordImport, $params, [$dataFileDto, $refFileDto]);

            $this->assertTrue($result);
            $this->assertTrue($this->recordService->getDataFromRecord($record)->count() > 0);
            $this->assertNotNull($this->recordImportService->getImportsByRecordId($record->id)->first(fn (RecordImport $import) => $import->id === $recordImport->id));
        }

        $paramsArray = [
            [
                'speed' => 2,
                'average' => 50,
                'expedition_number' => 1,
            ],
            [
                'speed' => 2,
                'average' => 50,
                'expedition_number' => 2,
            ],
            [
                'speed' => 2,
                'average' => 50,
                'expedition_number' => 3,
            ],

        ];

        $this->processCompatibleService->setDefaultImporter($exportProcess, $process);

        foreach ($paramsArray as $params) {
            $result = $this->exporterExecuteService->executeProcess($exportProcess, $record, $params, []);

            $this->assertInstanceOf(ProcessParamsDto::class, $result);
            $this->assertSame($result->getFiles()->count(), 0);
            $this->assertTrue($result->getParams()->count() > 0);
            $this->assertTrue($result->getData()->count() > 0);

            foreach ($params as $key => $value) {
                $this->assertNotNull($result->getParams()->first(fn (int $param, string $paramKey) => $key === $paramKey && $value === $param));
            }

            /*
                        foreach ($result->getData()->get('data') as $data) {
                            $this->assertSame($data->expedition_number, $params['expedition_number']);
                        }
            */
        }

        $this->recordService->clear($record);
        $this->processService->delete($process);
        $this->processService->delete($exportProcess);
    }
}
