<?php

declare(strict_types=1);

namespace Modules\Plugin\Tests\Fixture;

use App\Enums\ActiveStatusEnum;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\Plugin\Enums\PluginCreateDto;
use Modules\Plugin\Models\Plugin;
use Modules\Plugin\Services\DefaultPluginRecordService;
use Modules\Plugin\Services\PluginServiceInterface;

class PluginFixture
{
    use WithFaker;

    public function __construct(
        private readonly PluginServiceInterface $pluginService
    ) {
        $this->setUpFaker();
    }

    public function create(string $class = DefaultPluginRecordService::class): Plugin
    {
        $pluginDto = new PluginCreateDto(
            name: $this->faker->word,
            slug: $this->faker->slug,
            settings: [],
            serviceClass: $class,
            activeStatusEnum: ActiveStatusEnum::Active
        );

        return $this->pluginService->create($pluginDto);
    }
}
