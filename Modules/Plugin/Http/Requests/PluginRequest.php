<?php

namespace Modules\Plugin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PluginRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'slug' => ['required'],
            'settings' => ['required', 'date'],
            'service_class' => ['required'],
            'is_active' => ['required', 'integer'],
        ];
    }
}
