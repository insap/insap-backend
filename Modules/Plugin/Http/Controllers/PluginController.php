<?php

namespace Modules\Plugin\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Plugin\Http\Resources\PluginResource;
use Modules\Plugin\Services\PluginServiceInterface;

class PluginController extends Controller
{
    public function __construct(private readonly PluginServiceInterface $pluginService)
    {
    }

    public function index(): Response
    {
        $response = Response::make();
        $plugins = $this->pluginService->all();

        return $response->withData(PluginResource::collection($plugins));
    }
}
