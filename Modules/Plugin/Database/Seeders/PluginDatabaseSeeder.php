<?php

namespace Modules\Plugin\Database\Seeders;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Plugin\Enums\PluginCreateDto;
use Modules\Plugin\Services\PluginServiceInterface;
use Plugins\adcp\Services\AdcpPluginService;
use Plugins\ctd\Services\CtdPluginService;
use Plugins\drifters\Services\DriftersPluginService;

class PluginDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(PluginServiceInterface $pluginService): void
    {
        Model::unguard();

        $adcp = new PluginCreateDto(
            'ADCP',
            'adcp',
            [],
            AdcpPluginService::class,
            ActiveStatusEnum::Active
        );

        $ctd = new PluginCreateDto(
            'CTD',
            'ctd',
            [],
            CtdPluginService::class,
            ActiveStatusEnum::Active
        );

        $drifters = new PluginCreateDto(
            'Drifters',
            'drifters',
            [],
            DriftersPluginService::class,
            ActiveStatusEnum::Active
        );

        $pluginService->create($adcp);
        $pluginService->create($ctd);
        $pluginService->create($drifters);
    }
}
