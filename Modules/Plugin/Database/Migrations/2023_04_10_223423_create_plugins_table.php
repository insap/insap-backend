<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Plugin\Models\Plugin;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Plugin::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->json('settings');
            $table->string('service_class');
            $table->smallInteger('is_active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Plugin::TABLE);
    }
};
