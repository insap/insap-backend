<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Plugin\Http\Controllers\PluginController;

Route::prefix('plugins')->middleware('auth:sanctum')->name('plugin.')->group(function () {
    Route::post('/', [PluginController::class, 'index'])->name('index');
});
