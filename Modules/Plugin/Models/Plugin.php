<?php

namespace Modules\Plugin\Models;

use App\Enums\ActiveStatusEnum;
use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Plugin\Models\Plugin
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property array $settings
 * @property string $service_class
 * @property int $is_active
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin active()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereServiceClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereSettings($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin withTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Plugin withoutTrashed()
 *
 * @mixin \Eloquent
 */
class Plugin extends Model
{
    use ActiveScope, HasFactory, SoftDeletes;

    public const TABLE = 'plugins';

    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'slug',
        'settings',
        'service_class',
        'is_active',
    ];

    protected $casts = [
        'settings' => 'array',
    ];

    protected $attributes = [
        'is_active' => ActiveStatusEnum::Active->value,
    ];
}
