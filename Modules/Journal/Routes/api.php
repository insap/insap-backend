<?php

use Modules\Journal\Http\Controllers\ExternalJournalController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('journals')->middleware('client_auth')->name('journals.external.')->group(function () {
    Route::post('/', [ExternalJournalController::class, 'index'])->name('index');
});
