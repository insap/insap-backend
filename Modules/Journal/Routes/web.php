<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\Journal\Http\Controllers\JournalController;

Route::prefix('journals')->middleware('auth:sanctum')->name('journals.')->group(function () {
    Route::post('/', [JournalController::class, 'index'])->name('index');

    Route::post('create', [JournalController::class, 'create'])->name('create');
    Route::post('update', [JournalController::class, 'update'])->name('update');
    Route::post('get-by-id', [JournalController::class, 'getById'])->name('get-by-id');

    Route::post('get-local-archive-files', [JournalController::class, 'getLocalArchiveFiles'])->name('get-local-archive-files');
    Route::post('add-local-archive', [JournalController::class, 'addLocalArchive'])->name('add-local-archive');
    Route::post('remove-local-archive', [JournalController::class, 'removeLocalArchive'])->name('remove-local-archive');
});
