<?php

declare(strict_types=1);

namespace Modules\Journal\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\UploadedFile;
use Modules\File\Entities\File;
use Modules\Journal\DTO\JournalCreateDto;
use Modules\Journal\DTO\JournalFilterDto;
use Modules\Journal\Models\Journal;
use Modules\User\Entities\User;

interface JournalServiceInterface
{
    public function create(JournalCreateDto $dto, ?UploadedFile $image = null): Journal;

    public function delete(Journal $journal, bool $force = false): bool;

    public function all(): EloquentCollection;

    /** @return EloquentCollection<int, Journal> */
    public function getAllByFilters(JournalFilterDto $dto): EloquentCollection;

    public function changeImage(Journal $journal, UploadedFile $image, User $user): Journal;

    public function findJournalById(int $id): ?Journal;

    public function getBySlug(string $slug): Journal;

    public function getById(int $journalId): Journal;

    public function getLocalArchiveFiles(Journal $journal): EloquentCollection;

    public function addLocalArchive(Journal $journal, UploadedFile $file): bool;

    public function removeLocalArchive(Journal $journal, File $file): bool;

    public function updateName(Journal $journal, string $name): bool;

    public function updateDescription(Journal $journal, string $description): bool;

    public function updateDate(Journal $journal, Carbon $dateStart, ?Carbon $dateEnd): bool;

    public function updateTags(Journal $journal, array $tags): bool;
}
