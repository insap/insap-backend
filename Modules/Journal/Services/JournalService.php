<?php

declare(strict_types=1);

namespace Modules\Journal\Services;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Http\UploadedFile;
use Modules\Appliance\DTO\ApplianceFilterDto;
use Modules\Appliance\Services\ApplianceServiceInterface;
use Modules\File\Entities\File;
use Modules\File\Services\FileServiceInterface;
use Modules\Journal\DTO\JournalCreateDto;
use Modules\Journal\DTO\JournalFilterDto;
use Modules\Journal\Models\Journal;
use Modules\Record\Services\RecordServiceInterface;
use Modules\Tags\Services\TagServiceInterface;
use Modules\User\Entities\User;

class JournalService implements JournalServiceInterface
{
    public function __construct(
        private readonly FileServiceInterface $fileService,
        private readonly ApplianceServiceInterface $applianceService,
        private readonly RecordServiceInterface $recordService,
        private readonly TagServiceInterface $tagService
    ) {
    }

    public function create(JournalCreateDto $dto, ?UploadedFile $image = null): Journal
    {
        if ($image) {
            $dto->setImageId($this->fileService->createFromUploadedFile($image)->id);
        }

        return Journal::create($dto->toArray());
    }

    public function changeImage(Journal $journal, UploadedFile $image, User $user): Journal
    {
        $this->deleteImage($journal);
        $file = $this->fileService->createFromUploadedFile($image, $user);

        return $journal->fill(['image_id' => $file->id]);
    }

    private function deleteImage(Journal $journal): ?bool
    {
        if ($journal->image_id) {
            return $this->fileService->delete($journal->imageFile);
        }

        return null;
    }

    public function findJournalById(int $id): ?Journal
    {
        return Journal::whereId($id)->first();
    }

    public function all(): EloquentCollection
    {
        return Journal::active()->orderByDesc('date_end')->orderBy('id')->get();
    }

    public function getLocalArchiveFiles(Journal $journal): EloquentCollection
    {
        $result = EloquentCollection::make();

        foreach ($journal->files as $fileId) {
            $result->push($this->fileService->getById($fileId));
        }

        return $result;
    }

    public function addLocalArchive(Journal $journal, UploadedFile $file): bool
    {
        $files = $journal->files;

        $files[] = $this->fileService->createFromUploadedFile($file)->id;
        $files = array_unique($files);

        $journal->files = $files;

        return $journal->save();
    }

    public function removeLocalArchive(Journal $journal, File $file): bool
    {
        $files = collect($journal->files);

        $candidateToRemove = (int) $files->first(fn (int $fileId) => $file->id === $fileId);

        if ($candidateToRemove) {
            $this->fileService->delete($this->fileService->getById($candidateToRemove));
        }

        $files = $files->filter(fn (int $fileId) => $file->id !== $fileId);
        $journal->files = $files->all();

        return $journal->save();
    }

    public function updateName(Journal $journal, string $name): bool
    {
        return $journal->fill(['name' => $name])->save();
    }

    public function updateDescription(Journal $journal, string $description): bool
    {
        return $journal->fill(['description' => $description])->save();
    }

    public function updateDate(Journal $journal, Carbon $dateStart, ?Carbon $dateEnd): bool
    {
        return $journal->fill(['date_start' => $dateStart])->fill(['date_end' => $dateEnd])->save();
    }

    public function update(Journal $journal, ?string $name, ?string $description, ?Carbon $dateStart, ?Carbon $dateEnd): bool
    {
        if ($name && $journal->name !== $name) {
            $journal->fill(['name' => $name]);
        }

        if ($description && $journal->description !== $description) {
            $journal->fill(['description' => $description]);
        }

        if ($dateStart && $journal->date_start !== $dateStart) {
            $journal->fill(['date_start' => $dateStart]);
        }

        if ($dateEnd && $journal->date_end !== $dateEnd) {
            $journal->fill(['date_end' => $dateEnd]);
        }

        return $journal->save();
    }

    public function getBySlug(string $slug): Journal
    {
        return Journal::whereSlug($slug)->firstOrFail();
    }

    public function getAllByFilters(JournalFilterDto $dto): EloquentCollection
    {
        return Journal::active()
            ->when($dto->appliances, function (Builder $builder) use ($dto) {
                $appliances = $this->applianceService->getAllAppliancesFilter(new ApplianceFilterDto(slugs: $dto->appliances))->pluck('id');

                $builder->withWhereHas('records', function ($builder) use ($appliances) {
                    $builder->whereIn('appliance_id', $appliances);
                    $builder->orderByDesc('date');

                })->whereHas('records.imports');
            })
            ->orderByDesc('date_start')
            ->orderByDesc('id')
            ->get();
    }

    public function delete(Journal $journal, bool $force = false): bool
    {
        $records = $force ? $journal->records()->withTrashed()->get() : $journal->records;

        foreach ($records as $record) {
            $this->recordService->delete($record, $force);
        }

        if ($journal->imageFile) {
            $this->fileService->delete($journal->imageFile);
        }

        return $force ? $journal->forceDelete() : $journal->delete();
    }

    public function updateTags(Journal $journal, array $tags): bool
    {
        $this->tagService->sync($journal->tags, $tags, $journal->tags());

        return true;
    }

    public function getById(int $journalId): Journal
    {
        return Journal::findOrFail($journalId);
    }
}
