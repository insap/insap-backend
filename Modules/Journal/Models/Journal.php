<?php

namespace Modules\Journal\Models;

use App\Helpers\ImageHelper;
use App\Scopes\ActiveScope;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Modules\File\Entities\File;
use Modules\Record\Models\Record;
use Modules\Tags\Models\Tag;
use Modules\User\Entities\User;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Modules\Journal\Models\Journal
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property int|null $order
 * @property int $is_active
 * @property Carbon|null $date_start
 * @property Carbon|null $date_end
 * @property int|null $image_id
 * @property int|null $creator_user_id
 * @property array $files
 * @property string $slug
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read string $image
 * @property-read File|null $imageFile
 * @property-read Collection<int, Record> $records
 * @property-read int|null $records_count
 * @property-read Collection<int, Tag> $tags
 * @property-read int|null $tags_count
 * @property-read User|null $user
 *
 * @method static Builder|Journal active()
 * @method static Builder|Journal newModelQuery()
 * @method static Builder|Journal newQuery()
 * @method static Builder|Journal onlyTrashed()
 * @method static Builder|Journal query()
 * @method static Builder|Journal whereCreatedAt($value)
 * @method static Builder|Journal whereCreatorUserId($value)
 * @method static Builder|Journal whereDateEnd($value)
 * @method static Builder|Journal whereDateStart($value)
 * @method static Builder|Journal whereDeletedAt($value)
 * @method static Builder|Journal whereDescription($value)
 * @method static Builder|Journal whereFiles($value)
 * @method static Builder|Journal whereId($value)
 * @method static Builder|Journal whereImageId($value)
 * @method static Builder|Journal whereIsActive($value)
 * @method static Builder|Journal whereName($value)
 * @method static Builder|Journal whereOrder($value)
 * @method static Builder|Journal whereSlug($value)
 * @method static Builder|Journal whereUpdatedAt($value)
 * @method static Builder|Journal withTrashed()
 * @method static Builder|Journal withoutTrashed()
 *
 * @mixin Eloquent
 */
class Journal extends Model
{
    use ActiveScope, HasFactory, HasSlug, SoftDeletes;

    public const TABLE = 'journals';

    protected $table = self::TABLE;

    protected $fillable = [
        'name',
        'description',
        'order',
        'is_active',
        'date_start',
        'date_end',
        'image_id',
        'creator_user_id',
        'files', // FileIds from MongoFS
        'slug',
    ];

    protected $casts = [
        'date_start' => 'datetime',
        'date_end' => 'datetime',
        'files' => 'array',
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->slugsShouldBeNoLongerThan(config('app.max_slug_length'))
            ->doNotGenerateSlugsOnUpdate()
            ->generateSlugsFrom(['name'])
            ->saveSlugsTo('slug');
    }

    public function imageFile(): BelongsTo
    {
        return $this->belongsTo(File::class, 'image_id');
    }

    public function records(): HasMany
    {
        return $this->hasMany(Record::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_user_id');
    }

    public function tags(): MorphToMany
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function getImageAttribute(): string
    {
        return $this->image_id ? File::find($this->image_id)->url : ImageHelper::getAvatarImage($this->name);
    }
}
