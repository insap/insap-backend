<?php

declare(strict_types=1);

namespace Modules\Journal\DTO;

use Modules\Journal\Http\Requests\ExternalJournalIndexRequest;

class JournalFilterDto
{
    public function __construct(
        public ?array $appliances = null,
    ) {
    }

    public static function fromRequest(ExternalJournalIndexRequest $request): self
    {
        return new self($request->input('appliances'));
    }
}
