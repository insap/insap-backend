<?php

declare(strict_types=1);

namespace Modules\Journal\DTO;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

class JournalCreateDto implements Arrayable
{
    public function __construct(
        public string $name,
        public ActiveStatusEnum $activeStatusEnum,
        public ?string $description = null,
        public ?int $order = null,
        public ?Carbon $dateStart = null,
        public ?Carbon $dateEnd = null,
        public ?int $imageId = null,
        public ?int $creatorUserId = null,
        public array $files = []
    ) {
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'is_active' => $this->activeStatusEnum->value,
            'description' => $this->description,
            'order' => $this->order,
            'date_start' => $this->dateStart,
            'date_end' => $this->dateEnd,
            'image_id' => $this->imageId,
            'creator_user_id' => $this->creatorUserId,
            'files' => $this->files,
        ];
    }

    public function setImageId(?int $imageId): JournalCreateDto
    {
        $this->imageId = $imageId;

        return $this;
    }
}
