<?php

declare(strict_types=1);

namespace Modules\Journal\Tests\Fixture;

use App\Enums\ActiveStatusEnum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Modules\Journal\DTO\JournalCreateDto;
use Modules\Journal\Models\Journal;
use Modules\Journal\Services\JournalServiceInterface;

class JournalFixture
{
    use WithFaker;

    public function __construct(
        private readonly JournalServiceInterface $journalService
    ) {
        $this->setUpFaker();
    }

    public function create(?UploadedFile $uploadedFile = null): Journal
    {
        $dto = new JournalCreateDto(
            name: $this->faker->words(2, true),
            activeStatusEnum: ActiveStatusEnum::Active
        );

        return $this->journalService->create($dto, $uploadedFile);
    }

    public function createWithImage(): Journal
    {
        return $this->create(
            uploadedFile: UploadedFile::fake()->image('test.jpg')
        );
    }
}
