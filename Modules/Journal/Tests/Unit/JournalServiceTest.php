<?php

namespace Modules\Journal\Tests\Unit;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Modules\File\Services\FileServiceInterface;
use Modules\Journal\DTO\JournalCreateDto;
use Modules\Journal\Services\JournalServiceInterface;
use Modules\Journal\Tests\Fixture\JournalFixture;
use Modules\User\Tests\Fixture\UserFixture;
use Tests\TestCase;

class JournalServiceTest extends TestCase
{
    private ?JournalServiceInterface $journalService;

    private ?FileServiceInterface $fileService = null;

    private ?JournalFixture $journalFixture;

    private ?UserFixture $userFixture;

    public function setUp(): void
    {
        parent::setUp();

        $this->journalService = $this->app->make(JournalServiceInterface::class);
        $this->fileService = $this->app->make(FileServiceInterface::class);

        $this->journalFixture = $this->app->make(JournalFixture::class);
        $this->userFixture = $this->app->make(UserFixture::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->journalService = null;
        $this->fileService = null;

        $this->journalFixture = null;
        $this->userFixture = null;
    }

    public function testCreate(): void
    {
        $dto = new JournalCreateDto(
            name: $this->faker->words(2, true),
            activeStatusEnum: ActiveStatusEnum::Active
        );

        $journal = $this->journalService->create($dto);

        $this->assertSame($dto->name, $journal->name);
        $this->assertSame($dto->activeStatusEnum->value, $journal->is_active);
    }

    public function testDelete(): void
    {
        $journal = $this->journalFixture->createWithImage();

        $fileId = $journal->image_id;
        $modelId = $journal->id;

        $this->journalService->delete($journal);

        $this->expectException(ModelNotFoundException::class);
        $result = $this->journalService->getById($modelId);

        $this->expectException(ModelNotFoundException::class);
        $file = $this->fileService->getById($fileId);
    }

    public function testFindJournalById(): void
    {
        $journal = $this->journalFixture->create();
        $result = $this->journalService->findJournalById($journal->id);

        $this->assertNotNull($result);
    }

    public function testGetBySlug(): void
    {
        $journal = $this->journalFixture->create();

        $result = $this->journalService->getBySlug($journal->slug);

        $this->assertNotNull($result);
    }

    public function testChangeImage(): void
    {
        $user = $this->userFixture->create();
        $journal = $this->journalFixture->create();

        $uploadedFile = UploadedFile::fake()->image('test_image.png', 100, 100);

        $this->journalService->changeImage($journal, $uploadedFile, $user);

        $this->assertIsNumeric($journal->image_id);
        $this->assertTrue($this->fileService->exists($journal->imageFile->path));

        $this->fileService->delete($journal->imageFile);
    }
}
