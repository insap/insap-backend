<?php

namespace Modules\Journal\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class JournalRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required'],
            'description' => ['nullable'],
            'order' => ['nullable', 'integer'],
            'is_active' => ['required', 'integer'],
            'date_start' => ['nullable', 'date'],
            'date_end' => ['nullable', 'date'],
            'image_id' => ['nullable', 'integer'],
        ];
    }
}
