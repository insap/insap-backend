<?php

namespace Modules\Journal\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\File\Entities\File;
use Modules\Journal\Models\Journal;

class JournalIdFileRemoveRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'journal_id' => 'required|int|exists:'.Journal::TABLE.',id',
            'archive_id' => 'required|integer|exists:'.File::TABLE.',id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
