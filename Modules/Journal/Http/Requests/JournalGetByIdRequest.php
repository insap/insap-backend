<?php

namespace Modules\Journal\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Journal\Models\Journal;

class JournalGetByIdRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'journal_id' => 'required|int|exists:'.Journal::TABLE.',id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
