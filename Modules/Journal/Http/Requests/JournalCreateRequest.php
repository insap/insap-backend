<?php

declare(strict_types=1);

namespace Modules\Journal\Http\Requests;

use App\Http\Requests\CustomFormRequest;

class JournalCreateRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'description' => 'nullable|string',
            'date_start' => 'nullable|date_format:Y-m-d',
            'date_end' => 'nullable|date_format:Y-m-d|after_or_equal:date_start',
            'image' => 'nullable|file',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
