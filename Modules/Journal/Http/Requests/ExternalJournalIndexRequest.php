<?php

namespace Modules\Journal\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Appliance\Models\Appliance;

class ExternalJournalIndexRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'appliances' => 'sometimes|array',
            'appliances.*' => 'required|string|exists:'.Appliance::TABLE.',slug',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
