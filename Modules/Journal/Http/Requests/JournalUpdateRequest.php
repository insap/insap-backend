<?php

namespace Modules\Journal\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Journal\Models\Journal;

class JournalUpdateRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'journal_id' => 'required|integer|exists:'.Journal::TABLE.',id',

            'tags' => 'nullable|array',

            'name' => 'nullable|string',
            'description' => 'nullable|string',
            'date_start' => 'nullable|string|date_format:d.m.Y',
            'date_end' => 'nullable|string|date_format:d.m.Y|after_or_equal:date_start',
        ];
    }
}
