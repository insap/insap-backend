<?php

namespace Modules\Journal\Http\Requests;

use App\Http\Requests\CustomFormRequest;
use Modules\Journal\Models\Journal;

class JournalIdFileRequest extends CustomFormRequest
{
    public function rules(): array
    {
        return [
            'journal_id' => 'required|int|exists:'.Journal::TABLE.',id',
            'archive' => 'required|file',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
