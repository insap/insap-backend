<?php

namespace Modules\Journal\Http\Controllers;

use App\Enums\ActiveStatusEnum;
use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Carbon\Carbon;
use Modules\File\Http\Resources\FileResource;
use Modules\File\Services\FileServiceInterface;
use Modules\Journal\DTO\JournalCreateDto;
use Modules\Journal\Http\Requests\JournalCreateRequest;
use Modules\Journal\Http\Requests\JournalGetByIdRequest;
use Modules\Journal\Http\Requests\JournalIdFileRemoveRequest;
use Modules\Journal\Http\Requests\JournalIdFileRequest;
use Modules\Journal\Http\Requests\JournalUpdateRequest;
use Modules\Journal\Http\Resources\JournalResource;
use Modules\Journal\Services\JournalServiceInterface;

class JournalController extends Controller
{
    public function __construct(
        private readonly JournalServiceInterface $journalService,
        private readonly FileServiceInterface $fileService,
    ) {
    }

    public function index(): Response
    {
        $response = Response::make();

        $data = $this->journalService->all();
        $data->load(['user']);

        return $response->withData(JournalResource::collection($data));
    }

    public function create(JournalCreateRequest $request): Response
    {
        $response = Response::make();

        try {
            $dto = new JournalCreateDto(
                name: $request->input('name'),
                activeStatusEnum: ActiveStatusEnum::Active,
                description: $request->input('description'),
                order: 0,
                dateStart: $request->input('date_start') ? Carbon::parse($request->input('date_start')) : null,
                dateEnd: $request->input('date_end') ? Carbon::parse($request->input('date_end')) : null,
                creatorUserId: request()->user()->id
            );

            $journal = $this->journalService->create($dto, $request->file('image'));
        } catch (\Throwable $e) {
            return $response->catch($e);
        }

        return $response->withData(JournalResource::make($journal));
    }

    public function update(JournalUpdateRequest $request): Response
    {
        $response = Response::make();

        $journal = $this->journalService->findJournalById((int) $request->input('journal_id'));

        try {

            if ($request->input('name')) {
                $this->journalService->updateName($journal, $request->input('name'));
            }

            if ($request->input('description')) {
                $this->journalService->updateDescription($journal, $request->input('description'));
            }

            if ($request->input('date_start')) {
                $this->journalService->updateDate($journal,
                    Carbon::parse($request->input('date_start')),
                    $request->input('date_end') ? Carbon::parse($request->input('date_end')) : null
                );
            }

            if ($request->input('tags')) {
                $this->journalService->updateTags($journal, $request->input('tags'));
            }

        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function getById(JournalGetByIdRequest $request): Response
    {
        $response = Response::make();

        $recordData = $this->journalService->findJournalById((int) $request->input('journal_id'));
        $recordData->load(['user', 'tags']);

        return $response->withData(JournalResource::make($recordData));
    }

    public function getLocalArchiveFiles(JournalGetByIdRequest $request): Response
    {
        $response = Response::make();

        $journal = $this->journalService->findJournalById((int) $request->input('journal_id'));

        $files = $this->journalService->getLocalArchiveFiles($journal);
        $files->load('user');

        return $response->withData(FileResource::collection($files));
    }

    public function addLocalArchive(JournalIdFileRequest $request): Response
    {
        $response = Response::make();

        $journal = $this->journalService->findJournalById((int) $request->input('journal_id'));

        try {
            $this->journalService->addLocalArchive($journal, $request->file('archive'));
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }

    public function removeLocalArchive(JournalIdFileRemoveRequest $request): Response
    {
        $response = Response::make();

        $journal = $this->journalService->findJournalById((int) $request->input('journal_id'));
        $file = $this->fileService->getById((int) $request->input('archive_id'));

        try {
            $this->journalService->removeLocalArchive($journal, $file);
        } catch (\Throwable $throwable) {
            return $response->catch($throwable);
        }

        return $response->success();
    }
}
