<?php

namespace Modules\Journal\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Common\Response;
use Modules\Journal\DTO\JournalFilterDto;
use Modules\Journal\Http\Requests\ExternalJournalIndexRequest;
use Modules\Journal\Http\Resources\ExternalJournalResource;
use Modules\Journal\Services\JournalServiceInterface;

class ExternalJournalController extends Controller
{
    public function __construct(
        private readonly JournalServiceInterface $journalService
    ) {
    }

    public function index(ExternalJournalIndexRequest $request): Response
    {
        $response = Response::make();

        $dto = JournalFilterDto::fromRequest($request);
        $data = $this->journalService->getAllByFilters($dto);
        $data->loadMissing(['records.imports']);

        return $response->withData(ExternalJournalResource::collection($data));
    }
}
