<?php

namespace Modules\Journal\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Record\Http\Resources\ExternalRecordResource;

/** @mixin \Modules\Journal\Models\Journal */
class ExternalJournalResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'date_start' => $this->date_start?->format('d.m.Y H:i:s'),
            'date_end' => $this->date_end?->format('d.m.Y H:i:s'),
            'image' => $this->image,

            'records' => ExternalRecordResource::collection($this->records),
        ];
    }
}
