<?php

namespace Modules\Journal\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Record\Http\Resources\RecordResource;
use Modules\Tags\Http\Resources\TagResource;
use Modules\User\Http\Resources\UserResource;

/** @mixin \Modules\Journal\Models\Journal */
class JournalResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'order' => $this->order,
            'is_active' => $this->is_active,
            'date_start' => $this->date_start?->format('d.m.Y H:i:s'),
            'date_end' => $this->date_end?->format('d.m.Y H:i:s'),
            'image' => $this->image,

            'tags' => TagResource::collection($this->whenLoaded('tags')),

            'creator_user' => UserResource::make($this->whenLoaded('user')),
            'records' => RecordResource::collection($this->whenLoaded('records')),

            'created_at' => $this->created_at->format('d.m.Y H:i:s'),
            'updated_at' => $this->updated_at->format('d.m.Y H:i:s'),
        ];
    }
}
