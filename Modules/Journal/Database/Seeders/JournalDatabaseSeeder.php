<?php

namespace Modules\Journal\Database\Seeders;

use App\Enums\ActiveStatusEnum;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Journal\DTO\JournalCreateDto;
use Modules\Journal\Services\JournalServiceInterface;
use Modules\User\Services\UserServiceInterface;

class JournalDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(
        JournalServiceInterface $journalService,
        UserServiceInterface $userService
    ) {
        Model::unguard();

        $userId = $userService->findUserByEmail(config('app.admin_default_email'))->id;

        $dto = new JournalCreateDto(
            name: 'Тестовая экспедиция',
            activeStatusEnum: ActiveStatusEnum::Active,
            description: 'тест',
            order: 0,
            dateStart: Carbon::yesterday(),
            dateEnd: Carbon::tomorrow(),
            creatorUserId: $userId
        );

        $journalService->create($dto);
    }
}
