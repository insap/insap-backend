<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Journal\Models\Journal;
use Modules\User\Entities\User;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create(Journal::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->integer('order')->nullable();
            $table->smallInteger('is_active');
            $table->dateTime('date_start')->nullable();
            $table->dateTime('date_end')->nullable();
            $table->bigInteger('image_id')->nullable();
            $table->bigInteger('creator_user_id')->nullable();

            $table->json('files');

            $table->string('slug')->unique();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('image_id')->references('id')->on(\Modules\File\Entities\File::TABLE)->onDelete('SET NULL');
            $table->foreign('creator_user_id')->references('id')->on(User::TABLE)->onDelete('SET NULL');

        });
    }

    public function down(): void
    {
        Schema::dropIfExists(Journal::TABLE);
    }
};
