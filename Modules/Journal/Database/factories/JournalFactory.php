<?php

namespace Modules\Journal\Database\factories;

use App\Enums\ActiveStatusEnum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\Journal\Models\Journal;

class JournalFactory extends Factory
{
    protected $model = Journal::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'is_active' => ActiveStatusEnum::Active->value,
        ];
    }
}
